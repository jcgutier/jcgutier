import java.util.Arrays;  

public class ArrayExample5 { 
    public static void main(String[] args) { 
        //declaration and initialization of an array
        String[] array = {"apple","orange","banana","grapes","mango"};
 
        // Print array elements
        System.out.println(Arrays.asList(array));
    }
}
