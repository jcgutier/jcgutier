import java.util.Arrays;
 
public class ArrayExample2 {
 
    public static void main(String[] args) {
        //declaration and initialization of a multidimensional array  
        int[][] array = {{1,2,3}, {4, 5}, {6, 7, 8}};
 
        // Print array elements
        System.out.println(Arrays.deepToString(array));
    }
}
