public class ArrayExample4 {
 
    public static void main(String[] args) {
        //declaration and initialization of an array
        String[] array = {"apple","orange","banana","grapes","mango"};
 
        // Print array elements
        for (String element: array) {
            System.out.println(element);
        }
    }
}
