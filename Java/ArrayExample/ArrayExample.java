import java.util.Arrays;
 
public class ArrayExample {
 
    public static void main(String[] args) {
        // Initialize an array
        String[] array = {"apple","orange","banana","grapes","mango"};
 
        // Print array elements
        System.out.println(Arrays.toString(array));
    }
}
