public class ArrayExample3 {
 
    public static void main(String[] args) {
        //declaration and initialization of an array
        String[] array = {"apple","orange","banana","grapes","mango"};
 
        // Print array elements
        for(int i=0;i<array.length;i++)
            System.out.println(array[i]);  
    }
}
