/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author jcgutier
 */
public class Polimorfismo {

    public static void main(String[] args) {
        Rectangulo rectangulo = new Rectangulo(2, 3, "Rectangulo");
        Circulo circulo = new Circulo(5, "Circulo");
        Cuadrado cuadrado = new Cuadrado(3);
        IFigura[] figuras = new IFigura[3];
        figuras[0] = rectangulo;
        figuras[1] = circulo;
        figuras[2] = cuadrado;

        for (int i = 0; i < figuras.length; i++) {
            System.out.println("Perimetro de " + figuras[i] + " = " + figuras[i].perimetro());
        }
    }
}
