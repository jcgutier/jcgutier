/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author jcgutier
 */
public class Rectangulo extends Poligono implements IFigura {
    double ladoA;
    double ladoB;
    
    public Rectangulo (double A, double B, String nombre){
        super (4,nombre);
        ladoA = A;
        ladoB = B;
    }
    
    @Override
    public double area(){
        return ladoA*ladoB;
    }

    @Override
    public double perimetro() {
        return 2*ladoA+2*ladoB;
    }
    @Override
    public double semiPerimetro(){
        return perimetro()/2;
    }
    @Override
    public boolean esEquilatero(){
        if(ladoA == ladoB){
            return true;
        }
        else {
            return false;
        }
    }
}
