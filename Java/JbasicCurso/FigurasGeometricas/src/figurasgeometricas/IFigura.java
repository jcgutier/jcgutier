/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author jcgutier
 */
public interface IFigura {
    
    public double area();
    public double perimetro();
}
