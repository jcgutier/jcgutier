/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author jcgutier
 */
public class FigurasGeometricas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Rectangulo rectangulo  = new Rectangulo (2,3,"Rectangulo");
        Circulo circulo = new Circulo(5,"Circulo");
        Cuadrado cuadrado = new Cuadrado(3);
        System.out.print(rectangulo+" Area= ");
        System.out.println(rectangulo.area());
        System.out.print(circulo+" Area = ");
        System.out.println(circulo.area());
        System.out.print(cuadrado+" Area= ");
        System.out.println(cuadrado.area());
        // TODO code application logic here
    }
}
