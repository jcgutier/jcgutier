/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author jcgutier
 */
public class Circulo extends Poligono implements IFigura {
    double radio;
    public Circulo(double radio, String nombre){
        super(0,nombre);
        this.radio=radio;
    }
    
    @Override
    public double area() {
        return Math.PI*radio*radio;
    }
    
    @Override
    public double perimetro(){
        return 2*Math.PI*radio;
    }
    
    @Override
    public double semiPerimetro(){
        return perimetro() / 2;
    }
    
    @Override
    public boolean esEquilatero(){
        return true;
    }
}
