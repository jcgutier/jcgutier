/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author jcgutier
 */
public abstract class Poligono {
    String nombre;
    int numLados;
    
    public Poligono (int lados, String n){
        numLados = lados;
        nombre = n;
    }
    
    @Override
    public String toString(){
        return nombre;
    }
    
    public abstract double semiPerimetro();
    public abstract boolean esEquilatero();
}
