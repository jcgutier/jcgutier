/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej26 {
    
    public static void main(String[] args) {
        
        System.out.print("Ingresa el año para analizar: ");
        Scanner en = new Scanner (System.in);
        int año = en.nextInt();
        int A= año%19;
        int B = año %4;
        int C = año %7;
        int D = (19*A+24)%30;
        int E = (2*B+4*C+6*D+5)%7;
        int N = (22+D+E);
        System.out.println();
        if (N<=31){
            System.out.println("El domingo de Pascua es el dia "+N+" de Marzo");
        }
        else {
            N-=31;
            System.out.println("El domingo de Pascua es el dia "+N+" de Abril");
        }
    }
    
}
