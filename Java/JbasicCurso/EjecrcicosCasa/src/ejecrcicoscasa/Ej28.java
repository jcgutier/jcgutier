/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej28 {
    
    public static void main(String[] args) {
        
        Scanner es = new Scanner(System.in);
        System.out.println("Introduce 2 numeros enteros");
        System.out.print(">>");
        int n=es.nextInt();
        es.nextLine();
        System.out.print(">>");
        int m=es.nextInt();
        es.nextLine();
        System.out.println();
        System.out.println("  "+n+"     "+n);
        System.out.println("x "+m+"     "+m);
        System.out.println("-----   ----");
        System.out.println(" "+(n*m)+"     "+(n/m)+"\tResiduo= "+(n%m));
    }
}
