/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej25 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce un numero de mes: ");
        int mes = entrada.nextInt();
        entrada.nextLine();
        System.out.println();
        if (mes % 2 == 0) {
            if (mes == 2) {
                System.out.println("Este mes tiene 28 dias");
            } 
            else if(mes==8||mes==10||mes==12){
                System.out.println("Este mes tiene 31 dias");
            }
            else {
                System.out.println("Este mes tiene 30 dias");
            }
        } 
        else {
            if(mes==9||mes==11){
                System.out.println("Este mes tiene 30 dias");
            }
            System.out.println("Este mes tiene 31 dias");
        }
    }
}
