/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej46 {
    
    public static void mayor_menor(){
        System.out.println();
        System.out.println("Este programa calcula si el numero introducido es mayor o menor");
        System.out.print("Introduce un numero => ");
        Scanner en = new Scanner(System.in);
        int num = en.nextInt();
        System.out.println();
        if(num<0){
            System.out.println("El numero es negativo");
        }
        if(num==0){
            System.out.println("El numero es cero");
        }
        if(num>0){
            System.out.println("El numero es positivo");
        }
    }
    
}
