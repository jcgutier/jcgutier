/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej38 {
    
    public static void main(String[] args) {
        
        Scanner en = new Scanner(System.in);
        double c;
        c = 2.997925*Math.pow(10, 10);
        System.out.println("Este programa calcula la energia producida por una determinada masa");
        System.out.print("Introduce la masa: ");
        double m=en.nextDouble();
        en.nextLine();
        double E = m*c*c;
        System.out.println("La energia es de "+E);        
    }
    
}
