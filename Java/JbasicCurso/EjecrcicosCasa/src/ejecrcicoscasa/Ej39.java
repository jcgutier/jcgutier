/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej39 {
    
    public static void main(String[] args) {
        
        Scanner en = new Scanner(System.in);
        System.out.println("Este programa calcula la hipotemusa de cualquier triangulo, dados los catetos");
        System.out.println("Introduce los catetos");
        System.out.print("C1: ");
        double c1=en.nextDouble();
        en.nextLine();
        System.out.print("C2: ");
        double c2=en.nextDouble();
        en.nextLine();
        double H = Math.sqrt(Math.pow(c1, 2)+Math.pow(c2, 2));
        System.out.println("La hipotemusa es: "+H);
    }
    
}
