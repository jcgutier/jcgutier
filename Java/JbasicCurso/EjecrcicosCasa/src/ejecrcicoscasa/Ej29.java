/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej29 {
    
    public static void main(String[] args) {
        
        System.out.println("Este programa conviente los °C a °F");
        Scanner es = new Scanner(System.in);
        System.out.print("°C = ");
        double cen=es.nextDouble();
        es.nextLine();
        double f=(cen*9/5)+32;
        System.out.println();
        System.out.println("°F = "+f);
    }
    
}
