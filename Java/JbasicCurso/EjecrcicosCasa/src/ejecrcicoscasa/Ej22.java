/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej22 {
    
    public static void main(String[] args) {

        Scanner es = new Scanner(System.in);
        int A=3,B=4,c;
        System.out.println("Las variables son A=3 y B=4");
        System.out.println("¿Listo para la acción?");
        es.nextLine();
        c=B;
        B=A;
        A=c;
        System.out.println();
        System.out.println("Ahora los valores son A="+A+" y B="+B);
    }
    
}
