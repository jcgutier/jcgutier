/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Gatos {
    public static void main(String[] args) {
        double cuota;
        Scanner es = new Scanner(System.in);
        System.out.print("Introduce el numero de litros gastados este mes: ");
        double litros=es.nextInt();
        es.nextLine();
        System.out.println();
        if(litros<=50) {
            System.out.println("Este mes pagaras 6 euros");
        }
        else if(litros>50&&litros<=200){
            litros=litros-50;
            litros=litros*0.1;
            cuota=litros+6;
            System.out.println("El gato de este mes es de "+cuota+" euros");
        }
        else if(litros>200){
            litros=litros-200;
            litros=litros*0.3;
            cuota=litros+21;
            System.out.println("El gato de este mes es de "+cuota+" euros");
        }
    }
}
