/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

/**
 *
 * @author jcgutier
 */
public class Ej18 {

    public static void main(String[] args) {

        System.out.println("Numero\t\tCuadrado\tCubo");
        for (int i = 0; i <= 9; i++) {
            System.out.println(i + "\t\t" + i * i + "\t\t" + i * i * i);
        }
    }
}
