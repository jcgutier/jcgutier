/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej43 {
    
    public static void main(String[] args) {
        
        System.out.println("Este programa calcula la edad a la que murio una persona dado"
                + "la fecha de nacimiento y la fecha de defuncion");
        System.out.print("Introduce la fecha de nacimiento (dd/mm/aaaa): ");
        Scanner en = new Scanner(System.in);
        String Nac = en.nextLine();
        System.out.print("Introduce la fecha de defuncion (dd/mm/aaaa): ");
        String Def = en.nextLine();
        System.out.println();
        calc(An(Nac.substring(6)),An(Def.substring(6)),Me(Nac.substring(3, 5)),Me(Def.substring(3, 5)));
        System.out.println();
        
    }
    
    public static int An(String año){
        char c[]= new char[5];
        for(int i=0;i<año.length();i++){
            c[i]=año.charAt(i);
        }
        int A=(int)c[0]-48;
        int B=(int)c[1]-48;
        int C=(int)c[2]-48;
        int D=(int)c[3]-48;
        A*=1000;
        B*=100;
        C*=10;
        int T=A+B+C+D;
        return T;
    }
    
    public static int Me(String mes){
        char c[]= new char[5];
        for(int i=0;i<mes.length();i++){
            c[i]=mes.charAt(i);
        }
        int A=(int)c[0]-48;
        int B=(int)c[1]-48;
        A*=10;
        int T=A+B;
        return T;
    }
    
    public static int Di(String dia){
        char c[]= new char[5];
        for(int i=0;i<dia.length();i++){
            c[i]=dia.charAt(i);
        }
        int A=(int)c[0]-48;
        int B=(int)c[1]-48;
        A*=10;
        int T=A+B;
        return T;
    }
    
    public static void calc(int anN, int anD, int meN, int meD){
        int ao;
        if(meN>meD){
            ao=anD-anN-1;
        }
        else{
            ao=anD-anN;
        }
        System.out.println("La persona murio de "+ao+" años");
    }
}
