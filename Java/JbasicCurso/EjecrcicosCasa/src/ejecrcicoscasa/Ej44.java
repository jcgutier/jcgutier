/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej44 {
    public static void main(String[] args) {
        System.out.println("Este programa calcula si un año es biciesto o no");
        System.out.print("Introduce el año > ");
        Scanner en = new Scanner(System.in);
        int an=en.nextInt();
        en.nextLine();
        if(an%4==0){
            if(an%100==0){
                if(an%400==0){
                    System.out.println("El año "+an+" es biciesto");
                }
                else{
                    System.out.println("El año "+an+" no es biciesto");
                }
            }
            else{
                System.out.println("El año "+an+" es biciesto");
            }
        }
        else{
            System.out.println("El año "+an+" no es biciesto");
        }
    }
}
