/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class MayorMenor {

    public static void main(String[] args) {
        System.out.println("\nEste Programa analisa de 2 numeros introducidos , cual es el mayor\n");
        int a = MayorMenor.captura();
        System.out.println();
        int b = MayorMenor.captura();
        System.out.println();
        MayorMenor.analisa(a, b);
    }

    public static int captura() {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce numero:");
        int num = entrada.nextInt();
        entrada.nextLine();
        return num;
    }

    public static void analisa(int a, int b) {
        if (a > b) {
            System.out.println("El numero " + a + " es mayor que " + b + "\n");
        } else if (a < b) {
            System.out.println("El numero " + b + " es mayor que " + a + "\n");
        } else if (a == b) {
            System.out.println("Los numeros introducidos son iguales\n");
        }
    }
}
