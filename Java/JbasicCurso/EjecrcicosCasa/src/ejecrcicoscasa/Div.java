/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Div {
    public static void main(String[] args) {
        System.out.println("Este programa calcula si el numero dado es divisible entre 2 o 5\n");
        int a=Comp.intr();
        System.out.println();
        Div.op(a);
        System.out.println();
    }
    
    public static int intr(){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Intoduce un numero: ");
        int nu= entrada.nextInt();
        entrada.nextLine();
        return nu;
    }
    
    public static void op(int a){
        if(a%2==0){
            System.out.println("El numero "+a+" es divisible entre 2");
            if(a%5==0)
                System.out.println("Y tambien es divisible entre 5");
        }
        else if(a%5==0){
            System.out.println("El numero "+a+" es divisible entre 5");
        }
        else
            System.out.println("El numero no es divisible entre 2 ni entre 5");
    }
}
