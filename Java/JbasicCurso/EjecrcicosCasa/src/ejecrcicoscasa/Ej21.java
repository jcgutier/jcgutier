/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej21 {

    public static void main(String[] args) {

        Scanner es = new Scanner(System.in);
        System.out.println("Este programa calcula el perimetro y area del rombo");
        System.out.print("Introduce el valor de un lado del rombo: ");
        int lado = es.nextInt();
        es.nextLine();
        System.out.println();
        System.out.println("El area es: " + lado * lado);
        System.out.println("El perimetro es: " + (lado + lado + lado + lado));
    }
}
