/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej37 {
    
    public static void main(String[] args) {
        
        Scanner en = new Scanner(System.in);
        System.out.println("Este programa calcula la fuerza gravitacional entre dos masas dado el radio que la separa");
        System.out.print("Introduce la masa 1: ");
        double G= Math.pow(6.673, -8);
        double m1 = en.nextDouble();
        en.nextLine();
        System.out.print("Introduce la masa 2: ");
        double m2= en.nextDouble();
        en.nextLine();
        System.out.print("Introduce el radio: ");
        double r=en.nextDouble();
        en.nextLine();
        double F= (G*m1*m2)/r*r;
        System.out.println();
        System.out.println("La Fuerza gravitacional es: "+F+" Newtons");
    }
    
}
