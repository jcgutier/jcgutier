/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej27 {
    
    public static void main(String[] args) {
        
        Scanner es = new Scanner(System.in);
        System.out.println("Este programa analiza el caracter introducido");
        System.out.print("Introduce 1 caracter: ");
        String ca = es.nextLine();
        char c = ca.charAt(0);
        int valor = (int)c;
        System.out.println();
        if(valor>64&&valor<123){
            if(valor>90&&valor<97){
                System.out.println("Introduciste un simbolo");
            }
            else {
                System.out.println("Introduciste una letra");
            }
        }
        else if(valor>47&&valor<58){
            System.out.println("Introdciste un numero");
        }
        else {
            System.out.println("Introduciste un simbolo");
        }
    }
    
}
