/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej35 {
    
    public static void main(String[] args) {
        
        Scanner en = new Scanner(System.in);
        System.out.println("Este programa calcula el area de un triangulo");
        System.out.println("Introduce los lados del triangulo");
        System.out.print("a=");
        int a= en.nextInt();
        en.nextLine();
        System.out.print("b=");
        int b= en.nextInt();
        en.nextLine();
        System.out.print("c=");
        int c= en.nextInt();
        en.nextLine();
        int p = (a+b+c)/2;
        double A= Math.sqrt(p*(p-a*((p-b)*(p-c))));
        System.out.println("El Area es de: "+A);
        
    }
    
}
