/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej31 {

    public static void main(String[] args) {
        Ej31 nu = new Ej31();
        nu.tri();
        System.out.println();
        nu.cuad();
    }

    public void cuad() {

        System.out.print("Introduce una palabra de 6 letras: ");
        Scanner en = new Scanner(System.in);
        String pal = en.nextLine();
        char l;
        System.err.println();
        for (int i = 0; i < 4; i++) {
            for (int g = 0; g <= 7; g++) {
                if (i == 0 || i == 3) {
                    System.out.print("*");
                } else {
                    if (g == 0 || g == 7) {
                        System.out.print("*");
                    } else {
                        if (i == 2 && g == 1) {
                            for (int d = 0; d < 6; d++) {
                                l = pal.charAt(d);
                                System.out.print(l);
                            }
                        } if(i==1) {
                            System.out.print(" ");
                        }
                    }
                }
            }
            System.out.println();
        }
        // TODO code application logic here
    }

    public static void tri() {

        int a = 0;
        for (int i = 0; i < 4; i++) {
            for (int g = 0; g <= 6; g++) {
                if (i == 3) {
                    if (a < 4) {
                        System.out.print("*");
                    }
                    a++;
                }
                if (g == 3 && i == 0) {
                    System.out.print("*");
                } else {
                    if ((g == 2 || g == 4) && i == 1) {
                        System.out.print("*");
                    } else {
                        if ((g == 1 || g == 5) && i == 2) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                }
            }
            System.out.println();
        }
    }
}
