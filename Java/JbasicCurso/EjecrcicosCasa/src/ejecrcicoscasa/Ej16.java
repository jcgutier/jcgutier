/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej16 {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce el numero de veces que quieres que se imprima: ");
        int num = entrada.nextInt();
        System.out.println();
        for(int i=0;i<num;i++){
            for(int j=0;j<8;j++){
                if(j%2==0){
                    System.out.print("*");
                }
                else{
                    System.out.print(" ");
                }
            }
            if(i%2==0){
                System.out.println();
                System.out.print(" ");
            }
            else{
                System.out.println();
            }
        }
    }
    
}
