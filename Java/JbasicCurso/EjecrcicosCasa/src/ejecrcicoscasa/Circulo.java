/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Circulo {
    public static void main(String[] args) {
        double pi=3.14159;
        System.out.println("Este Programa calcula el Diametro, Perimetro y Area de un Circulo\n");
        double r=intro();
        double d=Circulo.diam(r);
        double p=Circulo.per(d, pi);
        Circulo.area(r, pi);
    }
    
    public static double intro(){
        Scanner entrada=new Scanner(System.in);
        System.out.print("Introduce el valor del radio: ");        
        double radio=entrada.nextInt();
        entrada.nextLine();
        return radio;
    }
    
    public static double diam(double r){
        double d=r*2;
        System.out.println("El diamtro es: "+d);
        return d;
    }
    
    public static double per(double d, double pi){
        double p=d*pi;
        System.out.println("El Perimetro es: "+p);
        return p;
    }
    
    public static void area(double r, double pi){
        double a=pi*r*r;
        System.out.println("El Area es: "+a);
    }
}
