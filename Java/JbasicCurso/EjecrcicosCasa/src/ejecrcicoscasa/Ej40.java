/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej40 {
    
    public static void main(String[] args) {
        Scanner en = new Scanner(System.in);
        
        System.out.println("Este programa pasa del formato de 24Hrs a 12Hrs");
        System.out.println();
        System.out.print("Introduce las Horas: ");
        int h = en.nextInt();
        en.nextLine();
        System.out.print("Introduce los Minutos: ");
        int m = en.nextInt();
        en.nextLine();
        System.out.println();
        if(h<=12){
            System.out.println(h+":"+m+" am");
        }
        else{
            System.out.println((h-12)+":"+m+" pm");
        }
    }
    
}
