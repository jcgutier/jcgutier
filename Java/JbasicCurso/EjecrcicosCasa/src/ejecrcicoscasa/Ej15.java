/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej15 {
    
    public static void main(String[] args) {
        
        System.out.println("Introduce 2 numeros enteros\n");
        int a=intro();
        int b=intro();
        System.out.println();
        an(a,b);
    }
    
    public static int intro(){
        
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce un numero: ");
        int num = entrada.nextInt();
        entrada.nextLine();
        return num;
    }
    
    public static void an(int a, int b){
        
        if(a%b==0)
            System.out.println("El numero "+a+" es multiplo de "+b);
        else
            System.out.println("El numero no es multiplo");
    }
}
