/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej45 {

    public static void calcula() {
        System.out.println("Este programa calcula el numero menor de billetes y monedas para cieta cantidad");
        System.out.print("Introduce la cantidad deseada => ");
        Scanner en = new Scanner(System.in);
        float can = en.nextFloat();
        en.nextLine();
        int mil = 0, quinientos = 0, doscientos = 0, cien = 0, cincuenta = 0, veinte = 0, diez = 0, cinco = 0, dos = 0, uno = 0, cinc = 0;
        do {
            if (can >= 1000) {
                mil += 1;
                can -= 1000;
            } else if (can >= 500) {
                quinientos += 1;
                can -= 500;
            } else if (can >= 200) {
                doscientos += 1;
                can -= 200;
            } else if (can >= 100) {
                cien += 1;
                can -= 100;
            } else if (can >= 50) {
                cincuenta += 1;
                can -= 50;
            } else if (can >= 20) {
                veinte += 1;
                can -= 20;
            } else if (can >= 10) {
                diez += 1;
                can -= 10;
            } else if (can >= 5) {
                cinco += 1;
                can -= 5;
            } else if (can >= 2) {
                dos += 1;
                can -= 2;
            } else if (can >= 1) {
                uno += 1;
                can -= 1;
            } else if (can >= 0.5) {
                cinc += 1;
                can -= 0.5;
            }
        } while (can != 0);
        System.out.println();
        int billetes=mil+quinientos+doscientos+cien+cincuenta+veinte;
        int monedad=diez+cinco+dos+uno+cinc;
        System.out.println("De billetes se necesitan: "+billetes);
        System.out.println("De monedas se necesitan: "+monedad);
        System.out.println();
        if (mil > 0) {
            System.out.println("Mil .................... [" + mil + "]");
        }
        if (quinientos > 0) {
            System.out.println("Quinientos  ............ [" + quinientos + "]");
        }
        if (doscientos > 0) {
            System.out.println("Doscientos  ............ [" + doscientos + "]");
        }
        if (cien > 0) {
            System.out.println("Cien    ................ [" + cien + "]");
        }
        if (cincuenta > 0) {
            System.out.println("Cincuenta   ............ [" + cincuenta + "]");
        }
        if (veinte > 0) {
            System.out.println("Veinte  ................ [" + veinte + "]");
        }
        if (diez > 0) {
            System.out.println("Diez    ................ [" + diez + "]");
        }
        if (cinco > 0) {
            System.out.println("Cinco   ................ [" + cinco + "]");
        }
        if (dos > 0) {
            System.out.println("Dos .................... [" + dos + "]");
        }
        if (uno > 0) {
            System.out.println("Uno .................... [" + uno + "]");
        }
        if (cinc > 0) {
            System.out.println("Cincuenta Centavos  .... [" + cinc + "]");
        }
        System.out.println();
    }
}
