/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Comp {
    public static void main(String[] args) {
        System.out.println("Introduce 3 numeros\n");
        int a=Comp.intr();
        int b=Comp.intr();
        int c=Comp.intr();
        System.out.println();
        Comp.calc(a, b, c);
        System.out.println();
    }
    
    public static int intr(){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Intoduce un numero: ");
        int nu= entrada.nextInt();
        entrada.nextLine();
        return nu;
    }
    
    public static void calc(int a, int b, int c){
        if((a+b)==c)
            System.out.print(a+"+"+b+"="+c);
        else if((a+c)==b)
            System.out.print(a+"+"+c+"="+b);
        else if((b+c)==a)
            System.out.print(b+"+"+c+"="+a);
        else
            System.out.println("Ninguna de las sumas es igual a ningun numero introducido");
    }
    
}
