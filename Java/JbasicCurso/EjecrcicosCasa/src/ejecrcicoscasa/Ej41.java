/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej41 {

    public static void main(String[] args) {

        System.out.print("Introduce un numero de tres digitos: ");
        Scanner en = new Scanner(System.in);
        int n = en.nextInt();
        en.nextLine();
        String d = Integer.toString(n);
        String sub = d.substring(1);
        if(sub.compareTo("00")==0){
            System.out.println(cent(d));
        }
        else {
            System.out.println(cent(d) + " " + dece(d)+uni(d));
        }
    }

    public static String cent(String d) {
        String cent = null;
        if (d.startsWith("1")) {
            if (d.compareTo("100") == 0) {
                cent = "Cien";
            } else {
                cent = "Ciento";
            }
        } else if (d.startsWith("2")) {
            cent = "Doscientos";
        } else if (d.startsWith("3")) {
            cent = "Trescientos";
        } else if (d.startsWith("4")) {
            cent = "Cuatrocientos";
        } else if (d.startsWith("5")) {
            cent = "Quinientos";
        } else if (d.startsWith("6")) {
            cent = "Seiscientos";
        } else if (d.startsWith("7")) {
            cent = "Setecientos";
        } else if (d.startsWith("8")) {
            cent = "Ochocientos";
        } else if (d.startsWith("9")) {
            cent = "Novecientos";
        }
        return cent;
    }

    public static String dece(String d) {
        String de = null;
        int c = (int) d.charAt(1);
        int o = (int) d.charAt(2);
        if (c == 49 && o == 48) {
            de = "Diez";
            return de;
        } else if (c == 49 && o == 49) {
            de = "Once";
            return de;
        } else if (c == 49 && o == 50) {
            de = "Doce";
            return de;
        } else if (c == 49 && o == 51) {
            de = "Trece";
            return de;
        } else if (c == 49 && o == 52) {
            de = "Catorce";
            return de;
        } else if (c == 49 && o == 53) {
            de = "Quince";
            return de;
        } else if (c == 49 && o > 53) {
            de = "Dieci";
            return de;
        } else if (c == 50 && o == 48) {
            de = "Veinte";
            return de;
        } else if (c == 50 && o > 48) {
            de = "Veinti";
            return de;
        } else if (c == 51 && o == 48) {
            de = "Treinta";
            return de;
        } else if (c == 51 && o > 48) {
            de = "Treinta y ";
            return de;
        } else if (c == 52 && o == 48) {
            de = "Cuarenta";
            return de;
        } else if (c == 52 && o > 48) {
            de = "Cuarenta y ";
            return de;
        } else if (c == 53 && o == 48) {
            de = "Cincuenta";
            return de;
        } else if (c == 53 && o > 48) {
            de = "Cincuenta y ";
            return de;
        } else if (c == 54 && o == 48) {
            de = "Sesenta";
            return de;
        } else if (c == 54 && o > 48) {
            de = "Sesenta y ";
            return de;
        } else if (c == 55 && o == 48) {
            de = "Setenta";
            return de;
        } else if (c == 55 && o > 48) {
            de = "Setenta y ";
            return de;
        } else if (c == 56 && o == 48) {
            de = "Ochenta";
            return de;
        } else if (c == 56 && o > 48) {
            de = "Ochenta y ";
            return de;
        } else if (c == 57 && o == 48) {
            de = "Noventa";
            return de;
        } else if (c == 57 && o > 48) {
            de = "Noventa y ";
            return de;
        } else if (c == 48 || o == 48) {
            return de = "";
        }
        return de;
    }

    public static String uni(String d) {

        String un = "";
        int p = (int) d.charAt(2);
        int s = (int) d.charAt(1);
        if (s != 49) {
            if (p == 49) {
                un = "uno";
            }
            if (p == 50) {
                un = "dos";
            }
            if (p == 51) {
                un = "tres";
            }
            if (p == 52) {
                un = "cuatro";
            }
            if (p == 53) {
                un = "cinco";
            }
            if (p == 54) {
                un = "seis";
            }
            if (p == 55) {
                un = "siete";
            }
            if (p == 56) {
                un = "ocho";
            }
            if (p == 57) {
                un = "nueve";
            }
        } else {
            if (p == 49) {
                un = "";
            }
            if (p == 50) {
                un = "";
            }
            if (p == 51) {
                un = "";
            }
            if (p == 52) {
                un = "";
            }
            if (p == 53) {
                un = "";
            }
            if (p == 54) {
                un = "seis";
            }
            if (p == 55) {
                un = "siete";
            }
            if (p == 56) {
                un = "0cho";
            }
            if (p == 57) {
                un = "nueve";
            }
        }
        return un;
    }
}
