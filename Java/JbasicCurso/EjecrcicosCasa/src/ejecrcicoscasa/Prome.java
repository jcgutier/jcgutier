/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Prome {

    public static void main(String[] args) {
        System.out.println("Este Programa calcula el promedio de 3 numeros\n");
        double a = Prome.introduce();
        double b = Prome.introduce();
        double c = Prome.introduce();
        Prome.pro(a, b, c);
        Prome.mayor(a, b, c);
    }

    public static double introduce() {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce un numero: ");
        double num = entrada.nextInt();
        entrada.nextLine();
        return num;
    }

    public static void pro(double a, double b, double c) {
        double p;
        p = (a + b + c) / 3;
        System.out.println("El promedio de los 3 numeros es: " + p);
    }

    public static void mayor(double a, double b, double c) {
        if (a > b && a > c && b < c) {
            System.out.println("El numero mayor es " + a + " y el numero menor es " + b + "\n");
        } else if (a > b && a > c && c < b) {
            System.out.println("El numero mayor es " + a + " y el numero menor es " + c + "\n");
        } else if (b > a && b > c && a < c) {
            System.out.println("El numero mayor es " + b + " y el numero menor es " + a + "\n");
        } else if (b > a && b > c && c < a) {
            System.out.println("El numero mayor es " + b + " y el numero menor es " + c + "\n");
        } else if (c > b && c > b && a < b) {
            System.out.println("El numero mayor es " + c + " y el numero menor es " + a + "\n");
        } else if (c > b && c > b && a > b) {
            System.out.println("El numero mayor es " + c + " y el numero menor es " + b + "\n");
        } else if (a == b && b == c) {
            System.out.println("Los numeros son iguales\n");
        }
    }
}
