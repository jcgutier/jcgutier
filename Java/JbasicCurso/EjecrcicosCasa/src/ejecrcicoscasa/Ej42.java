/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej42 {

    public static void main(String[] args) {
        System.out.println("Este prgrama pasa de numeros arabigos a numeros romanos");
        System.out.print("Introduce el numero menor a 4000: ");
        Scanner en = new Scanner(System.in);
        int an = en.nextInt();
        en.nextLine();
        System.out.println();
        if(an>=4000){
            System.out.println("Error solo se ha implementado hasta el numero 3999");
        }
        else{
            System.out.println(mili(an)+centi(an)+deci(an)+uni(an));
        }
        System.out.println();
    }

    public static String uni(int an) {
        String pro=Integer.toString(an);
        char cesa[]= new char[10];
        for(int a=0;a<pro.length();a++){
            cesa[(pro.length())-a]=pro.charAt(a);
        }
        int u=(int)cesa[1];
        u-=48;
        String ro[] = new String[10];
        ro[1] = "I";
        ro[2] = "II";
        ro[3] = "III";
        ro[4] = "IV";
        ro[5] = "V";
        ro[6] = "VI";
        ro[7] = "VII";
        ro[8] = "VIII";
        ro[9] = "IX";
        String un="";
        for (int j = 1; j <= u; j++) {
            //System.out.println(ro[j]);
            un=ro[j];
        }
        return un;
    }
    
    public static String deci(int an) {
        String pro=Integer.toString(an);
        char cesa[]= new char[10];
        for(int a=0;a<pro.length();a++){
            cesa[(pro.length())-a]=pro.charAt(a);
        }
        int d=(int)cesa[2];
        d-=48;
        String ro[] = new String[100];
        ro[1] = "X";
        ro[2]="XX";
        ro[3]="XXX";
        ro[4]="XL";
        ro[5]="L";
        ro[6]="LX";
        ro[7]="LXX";
        ro[8]="LXXX";
        ro[9]="XC";
        String un="";
        for (int i = 1; i <= d; i++) {
            //System.out.println(ro[i]);
            un=ro[i];
        }
        return un;
    }
    
    public static String centi(int an) {
        String pro=Integer.toString(an);
        char cesa[]= new char[10];
        for(int a=0;a<pro.length();a++){
            cesa[(pro.length())-a]=pro.charAt(a);
        }
        int c=(int)cesa[3];
        c-=48;
        String ro[] = new String[10];
        ro[1] = "C";
        ro[2] = "CC";
        ro[3] = "CCC";
        ro[4] = "CD";
        ro[5] = "D";
        ro[6] = "DC";
        ro[7] = "DCC";
        ro[8] = "DCCC";
        ro[9] = "CM";
        String un="";
        for (int j = 1; j <= c; j++) {
            //System.out.println(ro[j]);
            un=ro[j];
        }
        return un;
    }
    
    public static String mili(int an) {
        String pro=Integer.toString(an);
        char cesa[]= new char[10];
        for(int a=0;a<pro.length();a++){
            cesa[(pro.length())-a]=pro.charAt(a);
        }
        int m=(int)cesa[4];
        m-=48;
        String ro[] = new String[10];
        ro[1] = "M";
        ro[2] = "MM";
        ro[3] = "MMM";
        String un="";
        if(m>3){
            return un;
        }
        else {
            for (int j = 1; j <= m; j++) {
            //System.out.println(ro[j]);
            un=ro[j];
        }
        return un;
        }
    }
}
