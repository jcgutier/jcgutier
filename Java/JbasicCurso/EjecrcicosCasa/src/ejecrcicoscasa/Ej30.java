/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej30 {
    
    public static void main(String[] args) {
        
        Scanner es = new Scanner(System.in);
        System.out.println("Este programa resuelve un sistema de ecuaciones de la siguiente manera\n");
        System.out.println("ax+by=c\ndx+ey=f\n");
        System.out.println("Ingresa los coeficientes: ");
        System.out.print("a=");
        double a = es.nextDouble();
        es.nextLine();
        System.out.print("b=");
        double b = es.nextDouble();
        es.nextLine();
        System.out.print("c=");
        double c = es.nextDouble();
        es.nextLine();
        System.out.print("d=");
        double d = es.nextDouble();
        es.nextLine();
        System.out.print("e=");
        double e = es.nextDouble();
        es.nextLine();
        System.out.print("f=");
        double f = es.nextDouble();
        es.nextLine();
        System.out.println();
        double x= (c*e-b*f)/(a*e-b*d);
        double y= (a*f-c*d)/(a*e-b*d);
        System.out.println("x = "+x);
        System.out.println("y = "+y);
    }
    
}
