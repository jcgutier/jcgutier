/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ej47 {

    public static void calculadora() {
        char separa[] = new char[100];
        double primer;
        double segundo;
        double ax;
        double ax1;
        int c;
        int mul = 0;
        double total;
        System.out.println();
        System.out.println("Este programa es una calculadora simple");
        System.out.println("En este momento solo realiza operaciones entre numeros enteros");
        System.out.println("Para salir presiona q");
        System.out.println("Introduce tu operacion:");
        System.out.println();
        Scanner en = new Scanner(System.in);
        String datos;
        do {
            double primero[] = new double[100];
            double segund[] = new double[100];
            total = 0;
            c = 0;
            ax = 10;
            ax1 = 10;
            primer = 0;
            segundo = 0;
            System.out.print(">> ");
            datos = en.nextLine();
            for (int i = 0; i < datos.length(); i++) {
                separa[i] = datos.charAt(i);
                if ((separa[i] > 47 && separa[i] < 58) || separa[i] == 46) {
                    primero[i] = ((int) separa[i] - 48);
                }
                if ((separa[i] > 47 && separa[i] < 58) || separa[i] == 46) {
                    segund[i] = ((int) separa[i] - 48);
                }
                if (primero[i] == 0) {
                    c = i;
                }
            }
            if (separa[0] == 105) {
                Instrucciones();
            }
            ax = Math.pow(ax, (c - 1));
            for (int i = 0; i < c; i++) {
                primero[i] = primero[i] * ax;
                ax = ax / 10;
                primer = primer + primero[i];
            }
            ax1 = Math.pow(ax1, (datos.length() - c - 1));
            for (int i = c; i < datos.length(); i++) {
                segund[i] = segund[i] * ax1;
                ax1 = ax1 / 10;
                segundo = segundo + segund[i];
            }
            //System.out.println("*************" + primer);
            //System.out.println("*************" + segundo);
            if (separa[c] == 43) {
                total = primer + segundo;
            }
            if (separa[c] == 42) {
                total = primer * segundo;
            }
            if (separa[c] == 45) {
                total = primer - segundo;
            }
            if (separa[c] == 47) {
                total = primer / segundo;
            }
            System.out.println();
            if (separa[0] == 133 || separa[0] == 105) {
                System.out.println();
            } else {
                System.out.println("R=" + total + "\n");
            }
            System.out.println();
        } while (separa[0] != 113);
        System.out.println("Aplicacion Finalizada ...");
        System.out.println();
    }

    public static void Instrucciones() {
        System.out.println("\n\n\n++++++++++Instructivo++++++++++\n\n\n");
        System.out.println("Esta es una calculadora simple solo realiza operaciones de suma(+), resta(-), multiplicacion(*) y division(/)");
        System.out.println("El simbolo \">>\" muestra donde se debe teclear las operaciones");
        System.out.println("Se debe de ingresar las operaciones sin espaciones");
        System.out.println("Ej.   >> 56+34");
        System.out.println("Si desea salir de la aplicacion es necesario presionar la tecla \"q\" ");
        System.out.println("Presiona ENTER para regresar a la aplicacion ...");
        Scanner ne = new Scanner(System.in);
        ne.nextLine();
    }
}
