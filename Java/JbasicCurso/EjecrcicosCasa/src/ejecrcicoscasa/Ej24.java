/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author jcgutier
 */
public class Ej24 {
    
    public static void main(String[] args) {
        
        System.out.println("Introduce 3 numeros: ");
        System.out.print(">>");
        int a=into();
        System.out.print(">>");
        int b=into();
        System.out.print(">>");
        int c=into();   
        an(a,b,c);
    }
    
    public static int into(){
        
        Scanner es = new Scanner(System.in);
        int num=es.nextInt();
        es.nextLine();
        return num;
    }
    
    public static void an(int a, int b, int c){
        
        if(a<b&&b<c)
            System.out.println("Los numeros introducidos estan en orden ascendente");
        else
            System.out.println("Los numeros no estan en orden");
    }
    
}
