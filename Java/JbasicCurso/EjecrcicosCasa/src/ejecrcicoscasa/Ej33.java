/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;
/**
 *
 * @author user
 */
public class Ej33 {
    
    public static void main(String[] args) {
        
        Scanner en = new Scanner(System.in);
        System.out.println("Este programa convierte la medida de pies a "
                + "yardas, pulgadas, centimetros y metros\n");
        System.out.print("Ingrese la medida en pies: ");
        double med = en.nextDouble();
        double ya = med/3;
        double pul = med*12;
        double cm = pul * 2.54;
        double m = cm/100;
        System.out.println();
        System.out.println("Yardas = "+ya);
        System.out.println("Pulgadas = "+pul);
        System.out.println("Centimetros = "+cm);
        System.out.println("Metros = "+m);
    }
    
}
