/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecrcicoscasa;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class Ej34 {
    
    public static void main(String[] args) {
        
        Scanner en = new Scanner(System.in);
        System.out.println("Este programa calcula el volumen de un cilindor"
                + "asi como su area lateral\n");
        System.out.print("Introduce el radio: ");
        double ra=en.nextDouble();
        en.nextLine();
        System.out.print("Introduce la altura: ");
        double h = en.nextDouble();
        en.nextLine();
        double pe=per(ra*2);
        System.out.println();
        System.out.println("El area lateral es: "+(pe*h));
        double ar=area(ra*2);
        System.out.println("El volumen es: "+(ar*h));
    }
    
    public static double per(double d){
        double p=d*Math.PI;
        return p;
    }
    
    public static double area(double r){
        double a=Math.PI*r*r;
        return a;
    }
}
