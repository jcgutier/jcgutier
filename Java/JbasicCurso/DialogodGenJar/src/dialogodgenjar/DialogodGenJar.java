/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogodgenjar;

import javax.swing.JOptionPane;

/**
 *
 * @author jcgutier
 */
public class DialogodGenJar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            JOptionPane.showMessageDialog(null, "Programa iniciado.");
            String entrada1 = JOptionPane.showInputDialog("Ingresa un numero");
            System.out.println("Numero ingresado: " + entrada1);
            String entrada2 = JOptionPane.showInputDialog("Ingresa otro numero");
            System.out.println("Numero ingresado: " + entrada2);
            int num1 = new Integer(entrada1);
            int num2 = new Integer(entrada2);
            JOptionPane.showMessageDialog(null, "El resultado de la suma es : " + (num1 + num2), "Resultado", JOptionPane.WARNING_MESSAGE);
            System.out.println("Fin del programa");
        } catch (Exception ex) {
            System.out.println();
            System.out.println("Ha habido un error " + ex);
            System.out.println("No se ha introducido un numero ");
            JOptionPane.showMessageDialog(null, "ERROR NO INTRODUCISTE UN NUMERO");
        }
        // TODO code application logic here
    }
}
