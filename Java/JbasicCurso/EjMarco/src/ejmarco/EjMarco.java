/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejmarco;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 *
 * @author jcgutier
 */
public class EjMarco {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new EjMarco();
    }
    public EjMarco()
    {
        addWindowListener(this);
        setTitle("Marco");
        setsize(200,100);
        setvisible(true);
    }
    public void paint(Graphics g)
    {
        Font letrero = new Font("SansSerif",Font.ITALIC,14);
        g.setFont(letrero);
        g.drawString("Bienvenido al AWT ",42,60);
    }

class Cierre extends WindowAdapter
{
    @Override
    public void windowClosing(WindowEvent e)
    {
        System.exit(0);
    }
}