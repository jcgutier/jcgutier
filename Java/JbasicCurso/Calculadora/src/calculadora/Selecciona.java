/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Selecciona extends Calculo {

    public static void Opcion() {
        int op;
        do {
            System.out.println("\n++++++++++Menu++++++++++\n");
            System.out.println("\t 1.- Instructivo");
            System.out.println("\t 2.- Calculo de Gastos");
            System.out.println("\t 3.- No implementado");
            System.out.println("\t 4.- No Implementado");
            System.out.println("\t 9.- Regresar a la calculadora");
            System.out.println();
            System.out.print("Introduce tu opcion deseada: ");
            Scanner en = new Scanner(System.in);
            Calculo ob = new Calculo();
            op = en.nextInt();
            switch (op) {
                case 1:
                    Calculo.Instrucciones();
                    break;
                case 2:
                    Calculo.Calc();
                    break;
                case 9:
                    break;
            }
        } while (op != 9);
    }
}
