/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Calculo {

    public static void Calc() {

        int gas = 227 * 3;
        int tel = 200;
        int casa = 300;
        double carro = 2783.31;

        System.out.print(
                "Introduce el pago de tu quincena => ");
        Scanner en = new Scanner(System.in);
        double quincena = en.nextDouble();

        System.out.print(
                "Introduce el pago para no generar intereses de la tarjeta =>");
        double tarjeta = en.nextDouble();

        System.out.print(
                "Introduce los gatos por conceptos adicionales =>");
        double adicional = en.nextDouble();
        double gastosq = tel + casa + (carro / 2) + gas + (tarjeta / 2) + adicional+150;
        double restante = quincena - gastosq;

        System.out.println();

        System.out.println(
                "Tus gatos esta quincena son de " + gastosq);
        System.out.println(
                "Por lo que te resta de tu quincena " + restante);
        System.out.println();
        System.out.println("Presiona ENTER para regresar al menu ...");
        Scanner ne = new Scanner(System.in);
        ne.nextLine();
    }

    public static void Instrucciones() {
        System.out.println("\n\n\n++++++++++Instructivo++++++++++\n\n\n");
        System.out.println("Esta es una calculadora simple solo realiza operaciones de suma(+), resta(-), multiplicacion(*) y division(/)");
        System.out.println("El simbolo \">>\" muestra donde se debe teclear las operaciones");
        System.out.println("Se debe de ingresar las operaciones sin espaciones");
        System.out.println("Ej.   >> 56+34");
        System.out.println("Si desea salir de la aplicacion es necesario presionar la tecla \"q\" ");
        System.out.println("Presiona ENTER para regresar al menu ...");
        Scanner ne = new Scanner(System.in);
        ne.nextLine();
    }
}
