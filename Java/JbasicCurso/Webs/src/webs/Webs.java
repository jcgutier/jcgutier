/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webs;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import javax.swing.JOptionPane;

/**
 *
 * @author jcgutier
 */
public class Webs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //JOptionPane.showMessageDialog(null,"Bienvenido a Java!");
        //System.exit(0);
        Frame MiMarco = new Frame();
        Panel MiPanel = new Panel();
        Button BotonArea = new Button("Calcular área");
        Button BotonPerimetro = new Button("Calcularperímetro");        
        Label Titulo = new Label("Notas matematicas");
        
        MiMarco.add(MiPanel);
        MiPanel.add(BotonArea);
        MiPanel.add(BotonPerimetro);
        MiMarco.add(Titulo);
        MiMarco.setSize(400,200);
        MiMarco.setTitle("Ventana con botones");
        MiMarco.setVisible(true);
        
    }
}
