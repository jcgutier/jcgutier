/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animales;

/**
 *
 * @author jcgutier
 */
public class Animales {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /*Perro manchas = new Perro();
        manchas.hablar();
        
        Perro firulais = new PerroDomesticado();
        firulais.hablar();
        ((PerroDomesticado)firulais).truco();*/
        
        Domesticado [] animales = new Domesticado[2];
        
        animales[0] = new GatoDomesticado();
        animales[1] = new PerroDomesticado();
        
        for(Domesticado animal: animales){
            System.out.print(animal+" -- Mi truco es: ");
            animal.truco();
        }
    }
}
