/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animales;

/**
 *
 * @author jcgutier
 */
public class Perro extends Animal {
    
    public Perro(){
        super("Perro");
    }
    
    @Override
    public void hablar(){
        System.out.println("Guau Guau !!");
    }
}
