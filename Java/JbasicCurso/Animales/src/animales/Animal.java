/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animales;

/**
 *
 * @author jcgutier
 */
public abstract class Animal {
    String especie;
    public Animal(String n){
        especie = n;
    }
    
    @Override
    public String toString(){
        return especie;
    }
    
    public abstract void hablar();
}
