/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animales;

/**
 *
 * @author jcgutier
 */
public class Gato extends Animal {
    
    public Gato(){
        super("Gato");
    }

    @Override
    public void hablar() {
        System.out.println("Miau Miau !!");
    }
    
}
