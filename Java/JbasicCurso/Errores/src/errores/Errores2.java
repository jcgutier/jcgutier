/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package errores;

import javax.swing.JOptionPane;

/**
 *
 * @author jcgutier
 */
public class Errores2 {
    
    public static void main(String[] args) {
        
        try{
            String input = JOptionPane.showInputDialog("Digite un numero: ");
            int i = Integer.parseInt(input.substring(0));
            System.out.println("El numero es: "+i);
        } catch (Exception ex){
            System.out.println("Ha habido un error "+ex);
        }
        /*catch (NumberFormatException nfe) {
            System.out.println("El formato del numero es erroneo");
        } catch (NullPointerException npe) {
            System.out.println("Usted no ha digitado numero");
        }*/
    }
    
}
