/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resistencias;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Resistencias {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        char salir = 'n';
        System.out.println();
        System.out.println();
        System.out.println("***********************JC***********************");
        System.out.println("***********************JC***********************");
        System.out.println("***********************JC***********************");
        System.out.println("***********************JC***********************");
        System.out.println("***********************JC***********************");
        System.out.println("***********************JC***********************");
        System.out.println("***********************JC***********************");
        System.out.println();
        System.out.println();
        System.out.println("Este programa calcula el error minimo dado un valor en un arreglo"
                + " con 2 resistencias de valores comerciales, ya sea en serie o paralelo, para salir pulsa cualquier letra");
        System.out.println();
        do {
            int h = 1;
            int w, ayu = 0;
            double ayuda[] = new double[17000];
            double ErrorFinal = 100, Res1 = 0, Res2 = 0;
            String tipo = "";
            System.out.println();
            System.out.println();
            System.out.println();
            try {
                System.out.println();
                System.out.print("Introduce el valor deseado (pulsa \"s\" para salir) => ");
                Scanner en = new Scanner(System.in);
                double res = en.nextDouble();
                en.nextLine();
                Arreglo nuevo = new Arreglo();
                Error err = new Error();
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 13; j++) {
                        w = 1;
                        for (int q = 0; q < 10; q++) {
                            for (int c = 0; c < 13; c++) {
                                nuevo.setR1(valores(j) * h);
                                nuevo.setR2(valores(c) * w);
                                //System.out.print(valores(j) * h + "  ");
                                //System.out.print(valores(c) * w + "  ");
                                ayuda[ayu] = err.calcular(nuevo.ArregloSerie(), nuevo.ArregloParalelo(), res);
                                //System.out.println();
                                if (ayuda[ayu] == 0) {
                                    System.out.println("\nALTO AQUI EL ERROR ES CERO PORCIENTO");
                                    tipo = err.toString();
                                    System.out.println("------->    Con R1=" + nuevo.getR1() + " y R2=" + nuevo.getR2());
                                    System.out.println("------->    " + tipo);
                                    System.out.println("Pulsa enter para continuar puede haber otros valores ...\n\n");
                                    en.nextLine();
                                }
                                if (ayuda[ayu] <= ErrorFinal) {
                                    ErrorFinal = ayuda[ayu];
                                    Res1 = nuevo.getR1();
                                    Res2 = nuevo.getR2();
                                    tipo = err.toString();
                                }
                                ayu++;
                            }
                            w *= 10;
                        }
                    }
                    h *= 10;
                }
                System.out.println("\n\nTERMINO EL CALCULO LOS RESULTADOS SON:");
                System.out.println("------->    El error minimo es " + ErrorFinal + "%");
                System.out.println("------->    Con R1=" + Res1 + " y R2=" + Res2);
                System.out.println("------->    " + tipo + "\n\n");
            } catch (Exception ex) {
                salir = 'q';
                System.out.println("\n\n   ¡Hasta pronto!\n\n");
            }
        } while (salir != 'q');
    }

    public static double valores(int i) {
        double valores[] = new double[13];
        valores[0] = 1.0;
        valores[1] = 1.2;
        valores[2] = 1.5;
        valores[3] = 1.8;
        valores[4] = 2.2;
        valores[5] = 2.7;
        valores[6] = 3.3;
        valores[7] = 3.9;
        valores[8] = 4.7;
        valores[9] = 5.6;
        valores[10] = 6.8;
        valores[11] = 8.2;
        valores[12] = 9.1;
        return valores[i];
    }
}