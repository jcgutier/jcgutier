/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resistencias;

/**
 *
 * @author jcgutier
 */
public class Error {

    private double ers;
    private double erp;
    private String TipoArreglo;

    public Error() {
        erp = 0;
        ers = 0;
    }

    public double calcula(double serie, double paralelo, double valor) {
        setErs(Math.abs(((serie * 100) / valor) - 100));
        setErp(Math.abs(((paralelo * 100) / valor) - 100));
        if (getErs() < getErp()) {
            System.out.print("El arreglo serie es mejor con un error de " + getErs() + "%");
            return getErs();
        } else {
            System.out.print("El arreglo paralelo es mejor con un error de " + getErp() + "%");
            return getErp();
        }
    }

    public double calcular(double serie, double paralelo, double valor) {
        setErs(Math.abs(((serie * 100) / valor) - 100));
        setErp(Math.abs(((paralelo * 100) / valor) - 100));
        if (getErs() < getErp()) {
            TipoArreglo = "El Arreglo es en Serie";
            return getErs();
        } else {
            TipoArreglo = "El Arreglo es en Paralelo";
            return getErp();
        }
    }

    @Override
    public String toString() {
        return TipoArreglo;
    }

    /**
     * @return the ers
     */
    public double getErs() {
        return ers;
    }

    /**
     * @param ers the ers to set
     */
    public void setErs(double ers) {
        this.ers = ers;
    }

    /**
     * @return the erp
     */
    public double getErp() {
        return erp;
    }

    /**
     * @param erp the erp to set
     */
    public void setErp(double erp) {
        this.erp = erp;
    }
}
