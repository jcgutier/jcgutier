/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package resistencias;

/**
 *
 * @author jcgutier
 */
public class Arreglo {

    private double r1;
    private double r2;
    private double rts;
    private double rtp;
    private String arr;

    public Arreglo(double R1, double R2) {
        r1 = R1;
        r2 = R2;
    }

    public Arreglo() {
        r1 = 0;
        r2 = 0;
    }

    public double ArregloSerie() {
        rts = r1 + r2;
        return rts;
    }

    public double ArregloParalelo() {
        rtp = (r1 * r2) / (r1 + r2);
        return rtp;
    }
    
    @Override
    public String toString(){
        return arr= "Esto es un arreglo";
    }

    /**
     * @return the r1
     */
    public double getR1() {
        return r1;
    }

    /**
     * @param r1 the r1 to set
     */
    public void setR1(double r1) {
        this.r1 = r1;
    }

    /**
     * @return the r2
     */
    public double getR2() {
        return r2;
    }

    /**
     * @param r2 the r2 to set
     */
    public void setR2(double r2) {
        this.r2 = r2;
    }

    /**
     * @return the rts
     */
    public double getRts() {
        return rts;
    }

    /**
     * @param rts the rts to set
     */
    public void setRts(double rts) {
        this.rts = rts;
    }

    /**
     * @return the rtp
     */
    public double getRtp() {
        return rtp;
    }

    /**
     * @param rtp the rtp to set
     */
    public void setRtp(double rtp) {
        this.rtp = rtp;
    }
}
