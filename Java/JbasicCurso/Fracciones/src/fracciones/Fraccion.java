/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fracciones;

/**
 *
 * @author jcgutier
 */
public class Fraccion {

    private int numerador;
    private int denominador;
    
    public Fraccion(int n, int d){
        numerador=n;
        if(d!=0){
            denominador=d;
        }
        else
            throw new RuntimeException ("El denominador no puede ser cero");
    }
    
    public Fraccion(){
        numerador=1;
        denominador=1;
    }

    public Fraccion(String ns, String ds){
        
        int n = new Integer (ns);
        int d = new Integer (ds);
        numerador=n;
        if(d!=0){
            denominador=d;
        }
        else
            throw new RuntimeException ("El denominador no puede ser cero");
        
    }
    public void simplificar() {

        int divisor = 2;
        while (divisor < numerador && divisor < denominador) {
            if (numerador % divisor == 0 && denominador % divisor == 0) {
                numerador = numerador / divisor;
                denominador = denominador / divisor;

            } else {
                divisor++;
            }
        }
    }

    public double getRepresentacionDecimal() {

        double representacionDecimal = (double) numerador / (double) denominador;
        return representacionDecimal;
    }
    
    @Override
    public String toString(){
        String representacionCadena=""+numerador+"/"+denominador;
        //String representacionCadena="Mi fraccion";
        return representacionCadena;
    }
    
    public void convierteFraccionEquivalente(int multiplicador){
        numerador = numerador*multiplicador;
        denominador= denominador*multiplicador;
    }

    /**
     * @return the numerador
     */
    public int getNumerador() {
        return numerador;
    }

    /**
     * @param numerador the numerador to set
     */
    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    /**
     * @return the denominador
     */
    public int getDenominador() {
        return denominador;
    }

    /**
     * @param denominador the denominador to set
     */
    public void setDenominador(int denominador) {
        if(denominador !=0){
            this.denominador = denominador;
        }
        else{
            throw new RuntimeException("El denominador no puede ser cero");
        }
    }

    public int obtenerParteEntera() {
        int parteEntera = numerador / denominador;
        return parteEntera;
    }
    
    public void invertir(){
        int a=numerador;
        numerador=denominador;
        denominador=a;
    }
}
