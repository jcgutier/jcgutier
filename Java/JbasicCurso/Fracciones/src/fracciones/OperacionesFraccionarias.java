/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fracciones;

/**
 *
 * @author jcgutier
 */
public class OperacionesFraccionarias {
    
    public static Fraccion multiplicacion(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        int numeradorResultado=fraccion1.getNumerador()*fraccion2.getNumerador();
        int denominadorResultado=fraccion1.getDenominador()*fraccion2.getDenominador();
        fraccionResultado=new Fraccion(numeradorResultado, denominadorResultado);
        return fraccionResultado;
    }
    
    public static Fraccion Suma(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        int numeradorResultado=(fraccion2.getDenominador()*fraccion1.getNumerador())+(fraccion1.getDenominador()*fraccion2.getNumerador());
        int denominadorResultado=fraccion1.getDenominador()*fraccion2.getDenominador();
        fraccionResultado=new Fraccion(numeradorResultado, denominadorResultado);
        return fraccionResultado;
    }
    
    public static Fraccion Resta(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        int numeradorResultado=(fraccion2.getDenominador()*fraccion1.getNumerador())-(fraccion1.getDenominador()*fraccion2.getNumerador());
        int denominadorResultado=fraccion1.getDenominador()*fraccion2.getDenominador();
        fraccionResultado=new Fraccion(numeradorResultado, denominadorResultado);
        return fraccionResultado;
    }
    
    public static Fraccion division(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        int numeradorResultado=fraccion1.getNumerador()*fraccion2.getDenominador();
        int denominadorResultado=fraccion1.getDenominador()*fraccion2.getNumerador();
        fraccionResultado=new Fraccion(numeradorResultado, denominadorResultado);
        return fraccionResultado;
    }
}
