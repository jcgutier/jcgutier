/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fracciones;
import java.text.DecimalFormat;

/**
 *
 * @author jcgutier
 */
public class Punto {
    
    private int x;
    private int y;
    
    public Punto(){
        x=1;
        y=1;
    }
    
    public Punto(int X, int Y){
        x=X;
        y=Y;
    }
    
    @Override
    public String toString(){
        String representacionCadena="("+x+","+y+")";
        //String representacionCadena="Mi fraccion";
        return representacionCadena;
    }
    
    public String Polar(){
        double r,a;
        r=Math.sqrt((Math.pow(x, 2)+Math.pow(y, 2)));
        a=Math.atan(y/x);
        DecimalFormat df = new DecimalFormat("0.00");
        String po="("+df.format(r) +","+df.format(a) +")";
        return po;
    }
    
    public double Distacia(Punto p){
        int a=p.getX();
        int b=p.getY();
        double di=Math.sqrt((Math.pow(a-x, 2))+(Math.pow(b-y, 2)));
        return di;
    }
    
    public double Distacia(int a, int b){
        double di=Math.sqrt((Math.pow(a-x, 2))+(Math.pow(b-y, 2)));
        return di;
    }
    
    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }
}
