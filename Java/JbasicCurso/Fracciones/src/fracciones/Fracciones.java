/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fracciones;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Fracciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Fraccion unaFraccion = new Fraccion();
        unaFraccion.setNumerador(80);
        unaFraccion.setDenominador(30);
        System.out.println("Mi Fraccion es: " + unaFraccion);
        System.out.println("Representacion Decimal: " + unaFraccion.getRepresentacionDecimal());
        System.out.println("Parte entera: " + unaFraccion.obtenerParteEntera());
        unaFraccion.simplificar();
        System.out.println("La fraccion simplificada: " + unaFraccion);
        unaFraccion.convierteFraccionEquivalente(5);
        System.out.println("Fraccion equivalente: " + unaFraccion);
        unaFraccion.invertir();
        System.out.println("Mi Fraccion Invertida es: " + unaFraccion);

        Fraccion f = new Fraccion(2, 3);
        System.out.println(f.getRepresentacionDecimal());
        System.out.println(f);

        Fraccion f2 = new Fraccion("5", "10");
        System.out.println(f2);

        Fraccion resMult = OperacionesFraccionarias.multiplicacion(f, f2);
        System.out.println("El resultado de la multiplicacion: " + resMult);

        resMult.simplificar();
        System.out.println("Resultado simplificado: " + resMult);
        System.out.println();
        Fraccion resSum = OperacionesFraccionarias.Suma(f, f2);
        System.out.println("El resultado de la suma: " + resSum);
        resSum.simplificar();
        System.out.println("El resultado de la suma: " + resSum);
        System.out.println();
        Fraccion resRst = OperacionesFraccionarias.Resta(f, f2);
        System.out.println("El resultado de la resta: " + resRst);
        resRst.simplificar();
        System.out.println("El resultado de la resta: " + resRst);
        System.out.println();
        Fraccion resDiv = OperacionesFraccionarias.division(f, f2);
        System.out.println("El resultado de la division: " + resDiv);
        resDiv.simplificar();
        System.out.println("El resultado de la division: " + resDiv);

        Rectangulo r = new Rectangulo(2, 3);
        System.out.println("El resultado del area es: " + r.area());
        System.out.println("El resultado del perimetro es: " + r.perimetro());
        System.out.println();

        Angulo a = new Angulo(45);
        System.out.println("El angulo en radianes: " + a.Radianes());
        a.Suma(360);
        System.out.println("El angulo mas 360 es: " + a);
        Angulo otroAngulo = new Angulo(270);
        a.Resta(otroAngulo);
        System.out.println("Angulo menos " + otroAngulo + "=" + a);
        System.out.println();

        //System.out.println("Ingresa los valores de puntp P (x,y)\n");
        Scanner en = new Scanner(System.in);
        /*System.out.print("X = ");
        int p1=en.nextInt();
        en.nextLine();
        System.out.print("Y = ");
        int p2=en.nextInt();
        en.nextLine();
        Punto unPunto = new Punto(p1,p2)*/
        Punto unPunto = new Punto(1, 2);
        System.out.println("Cartesiana " + unPunto);
        System.out.println("Polar " + unPunto.Polar());
        System.out.println();

        /*System.out.println("Ingresa los valores de puntp Q (x,y)\n");
        System.out.print("X = ");
        p1=en.nextInt();
        en.nextLine();
        System.out.print("Y = ");
        p2=en.nextInt();
        Punto otroPunto = new Punto(p1,p2);*/
        Punto otroPunto = new Punto(3, 5);
        System.out.println("Cartesiana " + otroPunto);
        System.out.println("Polar " + otroPunto.Polar());
        System.out.println();
        double distancia = unPunto.Distacia(otroPunto);
        System.out.println("Distancia entre puntos " + distancia);

        distancia = otroPunto.Distacia(unPunto);
        System.out.println("Distancia entre puntos " + distancia);

        System.out.println("Distancia entre puntos " + unPunto.Distacia(3, 5));
    }
}
