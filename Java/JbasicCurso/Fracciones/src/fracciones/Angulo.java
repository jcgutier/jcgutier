/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fracciones;

/**
 *
 * @author jcgutier
 */
public class Angulo {
    
    private double ang;
    
    public Angulo(){
        ang=1;
    }
    
    public Angulo(int angu){
        if(angu>0&&angu<360){
            ang=angu;
        }
        else {
            throw new RuntimeException("El angulo no puede ser negativo ni mayor a 360");
        }
    }
    
    public double Suma(double num){
        ang+=num;
        if(ang>360){
            ang-=360;
        }
        return ang;
    }
    
    public double Resta(Angulo num){
        ang-=num.getAng();
        if(ang<360){
            ang+=360;
        }
        return ang;
    }
    
    public double Radianes(){
        double rad=(ang*Math.PI)/180;
        return rad;
    }
    
    public String toString(){
        String representacionCadena=""+ang;
        //String representacionCadena="Mi fraccion";
        return representacionCadena;
    }

    /**
     * @return the ang
     */
    public double getAng() {
        return ang;
    }

    /**
     * @param ang the ang to set
     */
    public void setAng(int ang) {
        this.ang = ang;
    }
    
    
}
