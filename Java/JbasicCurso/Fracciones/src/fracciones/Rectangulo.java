/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fracciones;

/**
 *
 * @author jcgutier
 */
public class Rectangulo {
    
    private int longitud;
    private int ancho;
    
    public Rectangulo(){
        longitud=1;
        ancho=1;
    }
    
    public Rectangulo(int largo, int anchura){
        longitud=largo;
        ancho=anchura;
    }
    
    public int area(){
        int resul;
        resul=longitud*ancho;
        return resul;
    }
    
    public int perimetro(){
        int resul;
        resul=(longitud*2)+(ancho*2);
        return resul;
    }

    /**
     * @return the longitud
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the ancho
     */
    public int getAncho() {
        return ancho;
    }

    /**
     * @param ancho the ancho to set
     */
    public void setAncho(int ancho) {
        this.ancho = ancho;
    }
    
}
