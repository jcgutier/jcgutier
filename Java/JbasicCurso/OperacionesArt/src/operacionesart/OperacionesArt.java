/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package operacionesart;

/**
 *
 * @author AulaE2
 */
public class OperacionesArt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("La suma de 5 + 8 es:");
        System.out.println(5+8);
        System.out.println();
        System.out.println("La suma de 78.87 + 32.59 es:");
        System.out.println(78.87+32.59);
        System.out.println();
        byte b1=21;
        short s2 = -577;
        int i2 = 3257;
        long L1 = 854775807;
        System.out.println(b1);
        System.out.println(s2);
        System.out.println(i2);
        System.out.println(L1);
        System.out.println();
        float f1=3566.386f;
        System.out.println(f1);
        System.out.println();
        double d1=23.98888811111;
        double d2=-696.02283e-88;
        System.out.println(d1);
        System.out.println(d2);
        System.out.println();
        boolean bandera=true;
        boolean flag=false;
        System.out.println(bandera);
        System.out.println(flag);
        System.out.println();
        String unString="Juan"+" "+"Carlos";
        System.out.println(unString);
    }
}
