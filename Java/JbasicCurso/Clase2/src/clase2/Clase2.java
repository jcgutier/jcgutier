/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clase2;

/**
 *
 * @author jcgutier
 */
public class Clase2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        boolean muestra;
        System.out.println("***** AND *****");
        
        muestra=true&&true;
        System.out.println(muestra);
        
        muestra=true&&false;
        System.out.println(muestra);
        
        muestra=false&&false;
        System.out.println(muestra);
        
        boolean muestra1;
        System.out.println("***** OR *****");
        
        muestra1=true||true;
        System.out.println(muestra1);
        
        muestra1=true||false;
        System.out.println(muestra1);
        
        muestra1=false||false;
        System.out.println(muestra1);
        
        boolean muestra2;
        System.out.println("***** XOR *****");
        
        muestra2=true^true;
        System.out.println(muestra2);
        
        muestra2=true^false;
        System.out.println(muestra2);
        
        muestra2=false^false;
        System.out.println(muestra2);
        // TODO code application logic here
    }
}
