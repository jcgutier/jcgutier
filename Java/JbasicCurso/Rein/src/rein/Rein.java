/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rein;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author jcgutier
 */
public class Rein {

    /**
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int i;
        double [] miLista = { 1.5,1.3,1,2.7};
        System.out.println(Thread.currentThread().toString());
        System.out.println();
        Circulo micirc = new Circulo();
        System.out.println("La superficie es: "+micirc.calcularSup());
        System.out.println();
        System.out.println("La clase es "+micirc.getClass().getName());
        System.out.println("El tamaño de miLista es: "+miLista.length);
        Date actual = new Date();
        //DateFormat formateador = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);
        System.out.println("La fecha es "+actual);
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("La fecha es: "+formato.format(actual));
        SimpleDateFormat formato1 = new SimpleDateFormat("HH:mm");
        System.out.println("La hora es: "+formato1.format(actual));
        String fechaLeida = "28/07/1821";        
        try {
            Date fechaIndep = formato.parse(fechaLeida);
            System.out.println("La Independencia: " + formato.format(fechaIndep));
            Calendar calendarioIndep = new GregorianCalendar();
            calendarioIndep.setTime(fechaIndep);
            calendarioIndep.add(Calendar.DATE, 1);
            System.out.println("Independencia avanzada un dia: " + calendarioIndep.getTime());
        } 
        catch (ParseException ex) {
            System.out.println("Usuario introdujo formato incorrecto");
        }
    }
}
