/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rein;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Circulo {
    private double radio;
    
    public Circulo()
    {
        radio=0;
    }
    
    /**
     * Regresa el nombre de circulo
     * @return
     */
    @Override
    public String toString()
    {
        return "Circulo";
    }
    
    double calcularSup()
    {
        System.out.print("Introduce el valor del radio => ");
        Scanner en = new Scanner(System.in);
        double r=en.nextDouble();
        radio=r*r*3.1415;
        return radio;
    }

    /**
     * @return the radio
     */
    public double getRadio() {
        return radio;
    }

    /**
     * @param radio the radio to set
     */
    public void setRadio(double radio) {
        this.radio = radio;
    }
    
}
