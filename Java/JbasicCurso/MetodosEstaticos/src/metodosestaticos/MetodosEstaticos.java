/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package metodosestaticos;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class MetodosEstaticos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean salir = false;
        int opcion = 0;
        while (!salir) {
            opcion = MetodosEstaticos.menu();
            if (opcion == 10) {
                salir = true;
            } else {
                int a = MetodosEstaticos.capturaNumero();
                int b = MetodosEstaticos.capturaNumero();
                salir = MetodosEstaticos.descifraOperacion(opcion, a, b);
            }
        }
    }

    public static int suma(int a, int b) {
        int r;
        r = a + b;
        return r;
    }

    public static int menu() {
        System.out.println("1) Suma");
        System.out.println("2) Resta");
        System.out.println("3) Multiplicacion");
        System.out.println("4) Division");
        System.out.println("5) Potencia");
        System.out.println("10) Salir");
        System.out.print("? ");

        Scanner sc = new Scanner(System.in);
        int op = sc.nextInt();
        sc.nextLine();
        return op;
    }

    public static int capturaNumero() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero: ");
        int num = sc.nextInt();
        sc.nextLine();
        return num;
    }

    public static int resta(int a, int b) {
        int r;
        r = a - b;
        return r;
    }

    public static int multiplicacion(int a, int b) {
        int r;
        r = a * b;
        return r;
    }

    public static int division(int a, int b) {
        int r;
        r = a / b;
        return r;
    }

    public static double potencia(int a, int b) {
        double r;
        r = 0;
        for (int i = 0; i < b; i++) {
            if (i == 0) {
                r = a;
            } else {
                r = r * a;
            }
        }
        return r;
    }

    public static boolean descifraOperacion(int op, int a, int b) {
        if (op == 1) {
            Scanner sc = new Scanner(System.in);
            System.out.println();
            System.out.println("El resultado de la suma es: " + MetodosEstaticos.suma(a, b));
            System.out.println("Pulse enter para regresar al menu ...");
            sc.nextLine();
            System.out.println();
        } else if (op == 2) {
            Scanner sc = new Scanner(System.in);
            System.out.println();
            System.out.println("El resultado de la resta es: " + MetodosEstaticos.resta(a, b));
            System.out.println("Pulse enter para regresar al menu ...");
            sc.nextLine();
            System.out.println();
        } else if (op == 3) {
            Scanner sc = new Scanner(System.in);
            System.out.println();
            System.out.println("El resultado de la multiplicacion es: " + MetodosEstaticos.multiplicacion(a, b));
            System.out.println("Pulse enter para regresar al menu ...");
            sc.nextLine();
            System.out.println();
        } else if (op == 4) {
            Scanner sc = new Scanner(System.in);
            System.out.println();
            System.out.println("El resultado de la division es: " + MetodosEstaticos.division(a, b));
            System.out.println("Pulse enter para regresar al menu ...");
            sc.nextLine();
            System.out.println();
        } else if (op == 5) {
            Scanner sc = new Scanner(System.in);
            System.out.println();
            System.out.println("El resultado de " + a + " elevado a la " + b + "° potencia es: " + MetodosEstaticos.potencia(a, b));
            System.out.println("Pulse enter para regresar al menu ...");
            sc.nextLine();
            System.out.println();
        } else if (op == 10) {
            return true;
        }
        return false;
    }
}