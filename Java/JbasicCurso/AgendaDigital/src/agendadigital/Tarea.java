/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agendadigital;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Tarea extends Evento {
    
    private String tarea;
    private boolean realizada=false;
    
    public Tarea(){
        Scanner en = new Scanner(System.in);
        System.out.print("Que Tarea es?: ");
        tarea=en.nextLine();
    }

    /**
     * @return the tarea
     */
    public String getTarea() {
        return tarea;
    }

    /**
     * @param tarea the tarea to set
     */
    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    /**
     * @return the realizada
     */
    public boolean isRealizada() {
        return realizada;
    }

    /**
     * @param realizada the realizada to set
     */
    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }
    
    public String toString(){
        if(realizada){
            return super.toString()+"("+tarea+") -- Tarea realizada";
        }
        else{
         return super.toString()+"("+tarea+") -- Trea Pendiente";   
        }
    }
}
