/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agendadigital;

/**
 *
 * @author jcgutier
 */
public class AgendaDigital {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean salir=false;
        
        while(!salir){
            int op= Agenda.menu();
            if(op == Agenda.AGREGA_CONTACTO)
                Agenda.agregaContacto();
            else if(op==Agenda.IPRIME_CONTACTOS)
                Agenda.imprimeContactos();
            else if(op==Agenda.AGREGA_FAMILIAR)
                Agenda.agregaFamiliar();
            else if(op==Agenda.AGREGA_Evento)
                Agenda.agregaEvento();
            else if(op==Agenda.AGREGA_TAREA)
                Agenda.agregaTarea();
            else if(op==Agenda.IPRIME_Eventos)
                Agenda.imprimeeventos();
            else if(op==Agenda.SALIR)
                salir=true;
            else
                System.out.println("\n\n¡¡Opcion erronea!!\n\n");
        }
        // TODO code application logic here
    }
}
