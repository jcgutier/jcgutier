/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agendadigital;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Familiar extends Contacto {
    private String parentesco;
    
    public Familiar (){
        Scanner sc = new Scanner(System.in);
        System.out.println("¿Cual es el parentesco?");
        parentesco = sc.nextLine();
    }

    /**
     * @return the parentesco
     */
    public String getParentesco() {
        return parentesco;
    }

    /**
     * @param parentesco the parentesco to set
     */
    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }
    
    @Override
    public String toString(){
        return super.toString()+"("+parentesco+")";
    }
    
}
