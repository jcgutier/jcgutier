/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agendadigital;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Evento {
    private String nombre;
    private String fecha;
    
    public Evento(){
        Scanner en = new Scanner(System.in);
        System.out.print("Nombre del evento?: ");
        nombre= en.nextLine();
        System.out.print("Fecha?: ");
        fecha= en.nextLine();
    }
    
    @Override
    public String toString(){
        return nombre+" ***"+fecha+"*** ";
    }
}
