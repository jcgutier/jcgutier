/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agendadigital;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Agenda {
    public static final int AGREGA_CONTACTO = 1;
    public static final int IPRIME_CONTACTOS = 2;
    public static final int SALIR = 10;
    private static Contacto[] contactos = new Contacto[100];
    private static int numContactos = 0;
    public static final int AGREGA_FAMILIAR = 3;
    public static final int AGREGA_TAREA = 5;
    public static final int AGREGA_Evento = 4;
    private static Evento[] eventos = new Evento[100];
    private static int numEventos = 0;
    public static final int IPRIME_Eventos = 6;
    
    private Agenda(){}
    
    public static void agregaEvento(){
        eventos[numEventos++]=new Evento();
    }
    
    public static void agregaTarea(){
        eventos[numEventos++]=new Tarea();
    }
    
    public static void agregaContacto(){
        contactos[numContactos++] = new Contacto();
    }
    
    public static void agregaFamiliar(){
        contactos[numContactos++]=new Familiar();
    }
    
    public static int menu(){
        System.out.println("\n***Menu de opciones***");
        System.out.println("1) Agrega Contacto");
        System.out.println("2) Ver Contactos");
        System.out.println("3) Agrega Familiar");
        System.out.println("4) Agrega Evento");
        System.out.println("5) Agrega Tarea");
        System.out.println("6) Ver Evento");
        System.out.println("10) Salir");
        Scanner sc = new Scanner(System.in);
        int op = sc.nextInt();
        sc.nextLine();
        return op;
    }
    
    public static void imprimeContactos(){
        int i=1;
        for(Contacto c: contactos) {
            if(c!=null)
                System.out.println(""+(i++)+")"+c);
        }
    }
    
    public static void imprimeeventos(){
        int i=1;
        for(Evento c: eventos) {
            if(c!=null)
                System.out.println(""+(i++)+")"+c);
        }
    }
}
