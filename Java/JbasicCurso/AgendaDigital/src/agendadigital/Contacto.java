/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agendadigital;

import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Contacto {
    private String nombre;
    private String tel;
    
    public Contacto(){
        Scanner sc = new Scanner (System.in);
        System.out.print("Nombre?: ");
        nombre= sc.nextLine();
        System.out.print("Telefono?: ");
        tel= sc.nextLine();
    }
    
    @Override
    public String toString(){
        return nombre+" ["+tel+"]";
    }
}
