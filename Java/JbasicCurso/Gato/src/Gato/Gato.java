
package gato;

/**
 * Clase inicial del juego Gato. No requiere de parámetros de línea de comandos.
 *
 * @author Israel Toledo
 * @see <a href="http://es.wikipedia.org/wiki/Tres_en_línea"> http://es.wikipedia.org/wiki/Tres_en_línea </a>
 * @version 1.0.0
 * @see Tablero
 */
public class Gato {

    /**
     * Método inicial del sistema gato.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws 
            InterruptedException, Exception {

        boolean salir = false;
        boolean tiradaValida = false;
        int [] tirada;
        Tablero tablero = new Tablero();
        Usuario usuario = new  Usuario();
        Maquina maquina = new Maquina(tablero);
        //maquina.setNivel(3);
        System.out.println ("**** Juego GATO ****");
        System.out.println ("* Instrucciones: ");
        System.out.println ("Cuando se te solicite la tirada");
        System.out.println ("debes escribir dos números separados");
        System.out.println ("por un espacio, que representen las ");
        System.out.println ("coordenadas del tablero dónde quieres");
        System.out.println ("tirar. ");
        System.out.println ("0 0 | 0 1 | 0 2 ");
        System.out.println ("---------------");
        System.out.println ("1 0 | 1 1 | 1 2");
        System.out.println ("---------------");
        System.out.println ("2 0 | 2 1 | 2 2");
        System.out.println ();
        System.out.println ();
        System.out.println ("Inicia el juego....");
        tablero.dibujar ();
        while ( ! salir){


            /* Tirada del usuario*/
            tiradaValida = false;
            while (! tiradaValida && !tablero.lleno()){
                tirada = usuario.getTirada();                
                tiradaValida = tablero.tiradaValida(tirada);                
                if (tiradaValida) {
                    tablero.marcar(Tablero.X, tirada);
                }
                else {
                    System.out.println ("Tirada inválida, vuelve a intentarlo.");
                }
            }

            tablero.dibujar ();

            /*Tirada de la máquina*/
            System.out.print ("Pensando..");
            for (int d =0; d<4;d++){
            Thread.sleep(1000);
            System.out.print ("..");
            }
            System.out.println (" Ya sé!!!");
            tiradaValida = false;
            while (! tiradaValida && !tablero.lleno()){
                tirada = maquina.getTirada();
                tiradaValida = tablero.tiradaValida(tirada);
                if (tiradaValida) {
                    tablero.marcar(Tablero.O, tirada);
                }
            }

            tablero.dibujar ();

            int gano = tablero.ganador();
            if (gano == Tablero.X){
                System.out.println ("Felicidades! Ganaste!!");
                salir = true;
            }
            if (gano == Tablero.O){
                System.out.println ("Lástima, intentalo de nuevo!, Perdiste. U.U");
                salir = true;
            }

            if (gano == Tablero.VACIA){
                if (tablero.lleno()){
                    System.out.println ("Casi lo logras, empate!!");
                    salir = true;
                }
            }
        }
    }
}
