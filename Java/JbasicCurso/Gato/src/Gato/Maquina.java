package gato;

import java.util.Random;

/**
 * 
 * Clase que representa los movimientos de la maquina
 * en base a un movimiento aleatorio.
 * @author yhavhe
 */
public class Maquina {
    
    /**
     * Constante para el modo facil de juego
     */
    public static final int FACIL = 1;
    
    /**
     * Contacte para modo dificil del juego
     */
    public static final int DIFICIL = 2;
    /**
     * Constante para obtener el nivel del jugo
     */
    private int nivel;
    
    /**
     * Constante para tener la decision sibre el tablero
     */
    private Tablero tablero;
    
    /**
     * Metodo para obtener el nivel.
     * @return nivel seleciconado
     */

    public int getNivel() {
        return nivel;
    }
    
    /**
     * Metodo para marcar error si no se ecoge el nivel adecuado
     * @param nivel que el usuario selecciono.
     */
    public void setNivel(int nivel) {        
        
        if (nivel > 2 || nivel <1) {
            throw new RuntimeException ("El nivel debe ser 1 o 2");
        }
        this.nivel = nivel;
        
    }
    
    /**
     * Contsructor por default del juego
     * @param t iniciliza el tablero
     */

    public Maquina (Tablero t){
        tablero = t;
        nivel = FACIL;
    }
    
    /**
     * Metodo para decidir el tipo de tirada
     * de la maquina, dependiendo del nivel
     * @return metodo de la tirada de la maquina
     */

    public int [] getTirada (){

        if (nivel == Maquina.FACIL) {
            return getTiradaAleatoria();
        }
        else {
            return getTiradaInteligente ();
        }
    }
    /**
     * Metodo para decidir aleatoreamente la tirada
     * de la maquina.
     * @return el valor de la tirada aleatoria
     */

    private int [] getTiradaAleatoria (){

        Random r = new Random();
        int fila = r.nextInt(3);
        int col = r.nextInt(3);
        int [] tirada = new int [2];
        tirada[0] = fila;
        tirada[1] = col;
        return tirada;
    }
    
    /**
     * Metodo para un tirada mas dificil
     * @return error de que aun no se ha implementado.
     */

    private int[] getTiradaInteligente() {
        throw new UnsupportedOperationException("El método no se ha implementado aún");
    }
}
