package gato;

import java.util.Scanner;

/**
 * Clase que representa la desicion
 * del usuario para jugar Gto
 * @author Israel Toledo
 * 
 */
public class Usuario {
    
    /**
     * Metodo que recibe la opcion para
     * tirara del usuario.
     * @return Los valores de la casilla del usuario
     */

    public int [] getTirada (){

        Scanner sc = new Scanner (System.in);
        System.out.println ("Tirada: ");

        int [] tirada = new int [2];
        tirada[0] = sc.nextInt();
        tirada[1] = sc.nextInt();
        sc.nextLine();
        return tirada;
    }
}
