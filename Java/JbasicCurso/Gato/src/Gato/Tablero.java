
package gato;

/**
 * Clase que representa un tablero para jugar
 * el clásico juego Tic Tac Toe, o como es
 * mejor conocido en México: Gato!
 *
 * @author Israel Toledo
 * @see <a href="http://es.wikipedia.org/wiki/Tres_en_línea"> Como jugar </a>
 * @version 1.0.0
 */
public class Tablero {

    /**
     * Constante que indica una casilla vacía.
     */
    public static final int VACIA = 0;    
    /**
     * Constante que indica una casilla con un valor X.
     */
    public static final int X = 1;    
    /**
     * Constante que indica una casilla con un valor O.
     */
    public static final int O = 2;
    
    /**
     * Representación virtual del tablero del gato.
     */
    private int [][] casillas;
    
    /**
     * Constructor por default del juego.
     * Inicializa el tablero a ser una rejjilla de 3x3.
     * Inicia a todas las casillas en 0.
     * 
     */
    public Tablero (){
        casillas = new int [3][3];

        for (int i = 0; i<casillas.length; i++){
            for (int j = 0; j<casillas[i].length; j++) {
                casillas[i][j] = VACIA;
            }
        }
    }
    
    /**
     * Método que valida una tirada de algún jugador.
     * Básicamente revisa que la casilla señalada por la tirada
     * sea una casilla vacía.
     * 
     * @param tirada Arreglo de dos posiciones que representa una tirada de un jugador. 
     *               tirada[0] es la fila, mientras que tirada[1] es la columna.
     * @return false - si la casilla ya ha sido ocupada.<br>
     *         true  - si la casilla está disponible, es decir, se encuentra vacía.
     */
    public boolean tiradaValida (int [] tirada) throws ArrayIndexOutOfBoundsException{
        int fila = tirada[0];
        int col = tirada[1];
        
        if (casillas[fila][col] == VACIA) {
            return true;
        }
        else {
            return false;
        }        

    }        
    
    /**
     * El método marcar sirve para marcar una casilla del tablero con la marca
     * de algún jugador.
     * 
     * @param marca Marca del jugador que tiró. Puede ser X o O.
     * @param tirada Arreglo de dos posiciones que representa la tirada del jugador.
     *               tirada[0] es la fila, tirada[1] es la columna
     */
    public void marcar (int marca, int [] tirada){
        int fila = tirada[0];
        int col = tirada[1];
        casillas[fila][col] = marca;
    }
    
    /**
     * Método para verificar si el tablero se encuentra lleno.
     * 
     * @return true - si todas las casillas han sido ocupadas. <br>
     *         false - si existe alguna casilla vacía.
     */
    public boolean lleno (){

        boolean lleno = true;
        for (int i = 0; i<casillas.length; i++){
            for (int j = 0; j<casillas[i].length; j++) {
                if (casillas[i][j] == VACIA) {
                    lleno = false;
                }
            }
        }
        return lleno;
    }
    
    /**
     * Método para verificar si existe un ganador. Si el ganador existe, regresa
     * el valor entero que representa la marca del ganador.
     * 
     * @return La constante entera que representa la marca del jugador ganador.
     *         Tablero.X o Tablero.O
     */
    public int ganador (){

        /*
         * Verificando si ganó horizontalmente
         */
        if (casillas[0][0] == casillas[0][1] &&
            casillas[0][1] == casillas[0][2]) {
            return casillas[0][0];
        }

        if (casillas[1][0] == casillas[1][1] &&
            casillas[1][1] == casillas[1][2]) {
            return casillas[1][0];
        }

        if (casillas[2][0] == casillas[2][1] &&
            casillas[2][1] == casillas[2][2]) {
            return casillas[2][0];
        }

        /*
         * Verificando si ganó verticalmente
         */
        if (casillas[0][0] == casillas[1][0] &&
            casillas[1][0] == casillas[2][0]) {
            return casillas[0][0];
        }

        if (casillas[0][1] == casillas[1][1] &&
            casillas[1][1] == casillas[2][1]) {
            return casillas[0][1];
        }

        if (casillas[0][2] == casillas[1][2] &&
            casillas[1][2] == casillas[2][2]) {
            return casillas[0][2];
        }
        
        if(casillas[0][0]==casillas[1][1] &&
                casillas[1][1]==casillas[2][2] ){
            return casillas [0][0];
        }
        if(casillas[0][2]==casillas[1][1] &&
                casillas[1][1]==casillas[2][0] ){
            return casillas [0][2];
        }

        return 0;
    }
    
    /**
     * Dibuja con caracteres el tablero del gato.
     */
    public void dibujar() {
        String marca = "";
        for (int i = 0; i<casillas.length; i++){
            for (int j = 0; j<casillas[i].length; j++){
                if(casillas[i][j] == X) {
                    marca = "X";
                }
                if(casillas[i][j] == O) {
                    marca = "O";
                }
                if(casillas[i][j] == VACIA) {
                    marca = " ";
                }
                System.out.print (marca);
                if (j<2) {
                    System.out.print(" | ");
                }
            }
            System.out.println ();
            if (i<2) {
                System.out.println ("---------");
            }
        }
    }
}
