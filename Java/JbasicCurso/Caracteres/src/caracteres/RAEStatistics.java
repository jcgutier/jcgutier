/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package caracteres;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class RAEStatistics {
    
    static int numEnunciados=0;
    static int longEnunciados=0;
    static int numPalabras=0;
    static int longPalabras=0;
    static int letras [] = new int [300];
    public static void main(String[] args) throws FileNotFoundException {
        
        procesaArchivo(new File("Quijote.txt"));
        System.out.println("Numero de enunciados: "+numEnunciados );
        System.out.println("Numero de palabras: "+numPalabras );
        System.out.println("Longitud promedio de los enunciados: "+longEnunciados/numEnunciados );
        System.out.println("Longitud promedio de las palabras: "+longPalabras / numPalabras );
        System.out.println("Cantidad de letras: ");
        for(int i=0; i<letras.length;i++){
            if(letras[i]!=0){
                System.out.println("["+(char)i+"]"+"="+letras[i]);
            }
        }
        //procesaEnunciado("Mas vale pajaro en mano que ciento volando");
    }
    
    private static void procesaPalabra (String palabra){
        numPalabras++;
        longPalabras+=palabra.length();
        System.out.println("Procesando: "+palabra);
        
        for(int i=0;i<palabra.length();i++){
            char letra = palabra.charAt(i);
            if (letra<letras.length){
                letras[letra]++;
            }
        }
    }
    
    private static void procesaEnunciado(String enunciado){
        numEnunciados++;
        longEnunciados+=enunciado.length();
        Scanner escaner = new Scanner(enunciado);
        escaner.useDelimiter("[ ,!¡'¿?:;\t\n]");
        while(escaner.hasNext()){
            procesaPalabra(escaner.next());
        }
    }
    
    private static void procesaArchivo(File selectedFile)throws FileNotFoundException{     
        Scanner escaner = new Scanner (new FileReader (selectedFile));
        escaner.useDelimiter("\\.");
        while (escaner.hasNext()){
            procesaEnunciado(escaner.next());
        }
    }
}
