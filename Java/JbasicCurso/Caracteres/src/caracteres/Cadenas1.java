/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package caracteres;

/**
 *
 * @author jcgutier
 */
public class Cadenas1 {
    
    public static void main(String[] args) {
        
        String word="java";
        System.out.println(word);
        
        char inicial=word.charAt(0);
        System.out.println(inicial);
        
        char mayuscula = Character.toUpperCase(inicial);
        System.out.println(mayuscula);
        
        String subcadena=word.substring(1);
        System.out.println(subcadena);
        
        word=mayuscula + word.substring(1);
        System.out.println(word);
        
    }
    
}
