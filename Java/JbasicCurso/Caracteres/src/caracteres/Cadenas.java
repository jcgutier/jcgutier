/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package caracteres;

/**
 *
 * @author jcgutier
 */
public class Cadenas {
    
    public static void main(String[] args) {
        
        char firstInitial;
        String name="Robert";
        System.out.println(name);
        
        firstInitial="Roberr".charAt(0);
        System.out.println("Primer inicial de la constante "+firstInitial);
        
        firstInitial=name.charAt(0);
        System.out.println("Primer inicial de la variable "+firstInitial);
    }
    
}
