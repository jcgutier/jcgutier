/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package caracteres;
import java.util.Scanner;

/**
 *
 * @author jcgutier
 */
public class Ejc {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String cadena;
        System.out.println("Ingresa un mensaje:");
        System.out.println();
        System.out.print(">>");
        cadena = entrada.nextLine();
        System.out.print("Ingresa la codificacion: ");
        int cod = entrada.nextInt();
        entrada.nextLine();
        System.out.println();
        System.out.print("El mensaje codificado es: ");
        for(int i=0;i<cadena.length();i++){
            char c=cadena.charAt(i);
            int valor = (int)c;
            if(valor==90)
                valor=64;
            else if(valor==122)
                valor=96;
            valor+=cod;
            char c1 = (char)valor;
            System.out.print(c1);
        }
        System.out.println("\n");
    }    
}
