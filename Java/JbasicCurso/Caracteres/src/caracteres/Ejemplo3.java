/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package caracteres;

/**
 *
 * @author jcgutier
 */
public class Ejemplo3 {
    
    public static void main(String[] args) {
        
        String name = "President George Washington";
        
        System.out.println(name.indexOf('P'));
        System.out.println(name.indexOf('e'));
        System.out.println(name.indexOf("George"));
        System.out.println(name.indexOf('e',3));
        System.out.println(name.indexOf("Bob"));
        System.out.println(name.lastIndexOf('e'));
    }
    
}
