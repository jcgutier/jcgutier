# Navegador TOR
src_filepath = "/home/carlos/tor.tar.xz"
extract_path = "/home/carlos/tor_browser/"
tor_url = ENV['B_TOR_URL']
remote_file "#{src_filepath}" do
  source tor_url
  owner 'carlos'
  group 'carlos'
  mode '0744'
  action :create
  not_if { ::File.exist?(extract_path) }
end
bash 'extract_torbrowser' do
  cwd "/home/carlos/"
  code <<-EOH
    mkdir -p #{extract_path}
    tar -xf tor.tar.xz -C #{extract_path}
    chown -R carlos:carlos #{extract_path}
    chmod -R 755 #{extract_path}
    rm #{src_filepath}
    EOH
    not_if { ::File.exist?(extract_path) }
end
####################################


#################################
## Instalacion de herramientas ##
#################################
  # TODO migrate PICARD
  # apt_repository 'musicbrainz' do
    # uri 'http://ppa.launchpad.net/musicbrainz-developers/stable/ubuntu'
    # arch 'amd64'
    # distribution debiandistro
    # components   ['main']
    # keyserver "keyserver.ubuntu.com"
    # key "DD34DEEB1F7B0EA487A01B270CC3AFF5CEDF0F40"
  # end
  # package "picard"
  # # Contenedores de Docker
    # # TODO migrate docker
  # apt_repository 'docker' do
    # uri "https://download.docker.com/linux/ubuntu"
    # arch "amd64"
    # distribution debiandistro
    # components ["stable"]
    # key "https://download.docker.com/linux/ubuntu/gpg"
  # end
  # package "docker-ce"
  #package "docker-ce-cli containerd.io"
  # service "docker" do
    # action [:enable, :start]
  # end
  # Archivos en cloud Dropbox
  # apt_repository 'dropbox' do
  #   uri "http://linux.dropbox.com/ubuntu"
  #   arch "amd64"
  #   distribution debiandistro
  #   components ["main"]
  #   keyserver "pgp.mit.edu"
  #   key "1C61A2656FB57B7E4DE0F4C1FC918B335044912E"
  # end
  # package "dropbox"
  # GQRCODE generador de codigos QR
  apt_repository 'gqrcode' do
    uri "http://ppa.launchpad.net/atareao/atareao/ubuntu"
    arch "amd64"
    distribution debiandistro
    components ["main"]
    keyserver "keyserver.ubuntu.com"
    key "A3D8A366869FE2DC5FFD79C36A9653F936FD5529"
  end
  package "gqrcode"

  # remote_file '/tmp/gitkraken.deb' do
  #   source 'https://www.gitkraken.com/download/linux-deb'
  #   mode '0755'
  #   action :create
  #   not_if "dpkg-query -W gitkraken"
  # end
  # dpkg_package 'gitkraken_install' do
  #   only_if { ::File.exists?('/tmp/gitkraken.deb') }
  #   source '/tmp/gitkraken.deb'
  #   action :install
  # end
end


################################################
## Instalando paquetes de SNAP solo en debian ##
################################################
# case node['platform_family']
# when "debian"
#     bash 'snap_installs' do
#         code <<-EOH
#         SNAP_PACKAGES=(\
#             "notepadqq" \
#             "spotify" \
#             "mailspring" \
#         )
#         for PACKAGE in ${SNAP_PACKAGES[@]};do
#             snap install $PACKAGE > /tmp/chef-snapout_$(date +"%Y%m%d_%H%M%S").txt 2>&1
#         done
#         EOH
#     end
# end
################################################


################################
## Instalando paquetes de pip ##
################################
bash 'pip_installs' do
    code <<-EOH
    PIP_PACKAGES=(\
        "virtualenvwrapper" \
        "cfn-lint" \
        "pydot" \
        "scapy" \
        "docker-compose" \
        "youtube-dl" \
    )
    python3 -m pip install pip --upgrade
    python3 -m pip install setuptools --upgrade
    for PACKAGE in ${PIP_PACKAGES[@]};do
        python3 -m pip freeze | grep $PACKAGE && \
        python3 -m pip install $PACKAGE --upgrade > /tmp/chef-pipout_$(date +"%Y%m%d_%H%M%S").txt 2>&1 || \
        python3 -m pip install $PACKAGE >> /tmp/chef-pipout_$(date +"%Y%m%d_%H%M%S").txt 2>&1
    done
    EOH
end
################################


################################################
## Iniciando el homeserver con docker-compose ##
################################################
# prog = ENV['prog']
# bash 'homeserver' do
#     code <<-EOH
#     docker ps -a| grep homeserver > /tmp/chef-docker-compose_$(date +"%Y%m%d_%H%M%S").txt 2>&1 || \
#         docker-compose -f "#{prog}"/Docker/docker-compose.yaml up -d >> /tmp/chef-docker-compose_$(date +"%Y%m%d_%H%M%S").txt 2>&1
#     EOH
# end
################################################
