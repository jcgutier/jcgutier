current_dir       = File.expand_path(File.dirname(__FILE__))
file_cache_path   "#{current_dir}"
cookbook_path     "#{current_dir}/cookbooks"
#role_path         "#{current_dir}/roles"
data_bag_path     "#{current_dir}/data_bags"
#log_level :debug
json_attribs File.join(File.dirname(File.expand_path(__FILE__)), "node.json")
log_location   STDOUT
