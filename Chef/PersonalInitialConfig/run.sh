#!/bin/bash

TOR_URL="https://www.torproject.org$(curl -s https://www.torproject.org/download/ | grep "Download for Linux" | grep -Eo 'href=\".*\"' | cut -d "=" -f2 | awk -F "\"" '{print $2}')"

sudo -E \
    `#Inicio de variables y versiones` \
    prog=$jcgutier \
    B_TOR_URL=$TOR_URL \
    MINIKUBE_VERSION="1.4.0" \
    `#Fin de variables y versiones` \
    chef-solo --chef-license accept -c $PWD/solo.rb -j $PWD/node.json;ret=$?
sudo rm -rf chef-client-running.pid chef_guid chef-stacktrace.out local-mode-cache/ remote_file/ nodes/
exit $ret
