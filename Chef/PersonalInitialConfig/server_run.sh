#!/bin/bash

sudo -i -E \
    `#Inicio de variables y versiones` \
    prog=$jcgutier \
    `#Fin de variables y versiones` \
    chef-solo -c $PWD/solo.rb -j $PWD/server.json;ret=$?
sudo rm -rf chef-client-running.pid chef_guid chef-stacktrace.out local-mode-cache/ remote_file/ nodes/
exit $ret
