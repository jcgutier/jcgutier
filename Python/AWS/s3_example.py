#!/usr/bin/env python

import boto3

client = boto3.client('s3')
s3 = boto3.resource('s3')

#Lits of buckets
response = client.list_buckets()
print response
#or
print ''
bucket_iterator = s3.buckets.all()
for bckt in bucket_iterator:
    print bckt.name

print ''

#Get the region of a given bucket
response = client.get_bucket_location(Bucket='jcgutier')
print response

print ''

#Create a bucket
#response = client.create_bucket(Bucket='jcgutierpythoncreated',
#                                CreateBucketConfiguration={
#                                    'LocationConstraint': 'us-east-2'
#                                }
#                               )
#print response

print ''

#Create a folder
response = client.put_object(Bucket='jcgutierpythoncreated',
                             Key='Folder1/')
print response

print ''

#Upload file
#upload = s3.meta.client.upload_file('/tmp/hello.txt', 'jcgutierpythoncreated', 'Folder1/hello.txt')
#print upload

print ''

#List of object
response = client.list_objects(Bucket='jcgutierpythoncreated')
print response
print ''
for i in response['Contents']:
    print i['Key']

print ''

#Get the access control policy
response = client.get_bucket_acl(Bucket='jcgutierrez')
print response
print ''
bucket = s3.Bucket('jcgutier')
bucket_acl = bucket.Acl()
print bucket_acl.grants
grant = bucket_acl.grants
if len(grant) > 1:
    print 'It is public'
else:
    print 'It is NOT public'

print ''

for bucket in s3.buckets.all():
    print(bucket.name)
    acl = bucket.Acl()
    for grant in acl.grants:
        if grant['Grantee']['Type'].lower() == 'group' and grant['Grantee']['URI'] == 'http://acs.amazonaws.com/groups/global/AllUsers':
            print '    It is public'
