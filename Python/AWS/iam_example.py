#!/usr/bin/env python

import boto3

client = boto3.client('iam')
iam = boto3.resource('iam')

#Group list
response = client.list_groups()
print 'The actual groups and their attached policies are:'
for group_info in response['Groups']:
    print '    {0}'.format(group_info['GroupName'])
    for policies in iam.Group(group_info['GroupName']).attached_policies.all():
        print '        {0}'.format(policies.policy_name)
print ''

'''
#Group's attached poliies
for i in iam.Group('Dev').attached_policies.all():
    print i
    print i.policy_name
'''

#Users list
response = client.list_users()
print 'The actual users and their groups are:'
for users_info in response['Users']:
    print '    {0}'.format(users_info['UserName'])
    for group in iam.User(users_info['UserName']).groups.all():
        print '        {0}'.format(group.name)

'''
#create user
response = client.create_user(UserName='Python_user')
print(response)

#Attache policy to user
response = client.attach_user_policy(UserName='Python_user',PolicyArn='arn:aws:iam::aws:policy/AmazonS3FullAccess')
print response

#Deattach policy (needed before deleting user)
response = client.detach_user_policy(UserName='Python_user',PolicyArn='arn:aws:iam::aws:policy/AmazonS3FullAccess')
print response

#Delete user
response = client.delete_user(UserName='Python_user')
print response
'''

'''
#Create Group
response = client.create_group(GroupName='Pythons_Group')
print response

#Attach a policy to a group
response = client.attach_group_policy(GroupName='Pythons_Group',PolicyArn='arn:aws:iam::aws:policy/AmazonS3FullAccess')
print response

#Detach group policy (needed before deleting group)
response = client.detach_group_policy(GroupName='Pythons_Group',PolicyArn='arn:aws:iam::aws:policy/AmazonS3FullAccess')
print response

#Delete group
response = client.delete_group(GroupName='Pythons_Group')
print response
'''
