#!/usr/bin/env python

import boto3

client = boto3.client('ec2')

response = client.describe_instances()
print "We have {0} EC2 instances".format(len(response['Reservations'][0]['Instances']))
#for i in response['Reservations']:
for i in response['Reservations']:
    for j in i['Instances']:
        print j
        print j['Monitoring']
        print j['PublicDnsName']
        print j['State']
        print j['LaunchTime']
        print j['PublicIpAddress']
