#!/usr/bin/env python

import boto3

ec2 = boto3.resource('ec2')
vpc = ec2.Vpc('vpc-e44e298c')

client = boto3.client('ec2')

#Detach IGW to the given VPC
#The DryRun checks if you have the required permission for this action. If you do 
# the error response will be 'DryRunOperation' otherwise 'UnauthorizedOperation'
#response = vpc.detach_internet_gateway(DryRun=True,InternetGatewayId='igw-a38f62cb')
#response = vpc.detach_internet_gateway(InternetGatewayId='igw-a38f62cb')
#print response

#Attach IGW to VPC, DryRun same as detach.
#response = vpc.attach_internet_gateway(DryRun=True,InternetGatewayId='igw-a38f62cb')
#response = vpc.attach_internet_gateway(InternetGatewayId='igw-a38f62cb')
#print response

#Crear un IGW
#response = client.create_internet_gateway(DryRun=True|False)
#response = client.create_internet_gateway()
#print response

#Delete IGW
#response = client.create_internet_gateway(DryRun=True|False)
#response = client.delete_internet_gateway(InternetGatewayId='igw-07479a5d8833a37a1')
#print response


#List of IGW and its attachments
print 'List of IGWs'
for igw in ec2.internet_gateways.all():
    print igw.id
    for vpc in ec2.InternetGateway(igw.id).attachments:
        print '    {0}'.format(vpc['State'])
        print '    {0}'.format(vpc['VpcId'])

print ''

#List of VPCs
print 'List of VPCs'
for vpc in ec2.vpcs.all():
    print vpc.id

print ''

#attachments of IGW
print 'Attachments of igw-id'
igw = ec2.InternetGateway('igw-a38f62cb')
for vpc in igw.attachments:
    print vpc['State']
    print vpc['VpcId']

print ''

#List of all route tables
print 'List of RTs'
for rt in ec2.route_tables.all():
    print rt.id

print ''
print 'List of routes assosiated with an specific rt-id, and its pvc-id'
route_table = ec2.RouteTable('rtb-a9451dc1')
for route in route_table.routes_attribute:
    print route
print route_table.vpc_id

#Create a route in a RT
#response = route_table.create_route(DestinationCidrBlock='192.168.0.0/24', GatewayId='igw-0a79b2df1f6aa9e94')

#Delete route in a RT
#response = client.delete_route(DestinationCidrBlock='192.168.0.0/24', RouteTableId='rtb-a9451dc1')
#print response

#Create a RT
#response = client.create_route_table(VpcId='vpc-e44e298c')
#print response

#Delete RT
#response = client.delete_route_table(RouteTableId='rtb-0f38a8f520042bf30')
#print response

#Assosiate RT with Subnet
#route_table_association = route_table.associate_with_subnet(SubnetId='string')

print ''

#List of all NACLs
print 'List of NACLs'
for nacl in ec2.network_acls.all():
    print nacl.id
    net_acl = ec2.NetworkAcl(nacl.id)
    print '    Subnets'
    for subnet in net_acl.associations:
        print '    {0}'.format(subnet)
    for entry in net_acl.entries:
        if entry['Egress']:
            print '    Outbound Rule'
            print '        {0}'.format(entry)
        else:
            print '    Inbound Rule'
            print '        {0}'.format(entry)

#Create NACL and asisgnit a name
#response = client.create_network_acl(VpcId='vpc-e44e298c')
#nacl_new_id = response['NetworkAcl']['NetworkAclId']
#network_acl = ec2.NetworkAcl(nacl_new_id)
#tag = network_acl.create_tags(Tags=[{'Key':'Name','Value':'python_nacl'}])
#print tag

#Delete NACL
#response = client.delete_network_acl(NetworkAclId='acl-0b2ac481c081f0c2d')
#print response

#Create rule of an NACL
#response = client.create_network_acl_entry(
#        CidrBlock='0.0.0.0/0',
#        Egress=False,
#        NetworkAclId='acl-0d779220272476647',
#        PortRange={
#                    'From': 53,
#                    'To': 53,
#                },
#        Protocol='6',
#        RuleAction='allow',
#        RuleNumber=200,
#)
#print response

#Delete NACL rule
#response = client.delete_network_acl_entry(
#        Egress=False,
#        NetworkAclId='acl-0d779220272476647',
#        RuleNumber=200,
#)
#print response

#Assosiate a subnet to a NACL
#network_acl = ec2.NetworkAcl('acl-0d779220272476647')
#response = network_acl.replace_association(AssociationId='aclassoc-1ae1f270')
#print response

#List of all Subnets
print 'List of all subnets'
for subnet in ec2.subnets.all():
    print subnet.id, subnet.availability_zone, subnet.cidr_block, subnet.state
print ''
response = client.describe_route_tables()
for p in response['RouteTables'][0]['Associations']:
    print p

print ''
response = client.describe_subnets()
for p in response['Subnets'][0]:
    print p
print ''
response = client.describe_route_tables(
    Filters=[
        {
            'Name': 'association.subnet-id',
            'Values': [
                'subnet-0c96ac0900b9e2b1c'
            ]
        }
    ]
)
print(response['RouteTables'][0]['Associations'][0]['RouteTableId'])
