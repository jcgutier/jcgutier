#!/usr/bin/env python

import unittest
from Python.Home.SendTelegram import sendtelegram

class sendtelegram_test(unittest.TestCase):
    def test_sendtelegram_ok(self):
        mensaje = 'Mensaje de prueba'
        chatid = '698412637'
        response = sendtelegram.sendmessagetojcgutier(mensaje, chatid)
        expected = True
        self.assertEqual(response, expected)
    def test_sendtelegram_notok(self):
        mensaje = 'Mensaje de prueba fallido'
        chatid = ''
        response = sendtelegram.sendmessagetojcgutier(mensaje, chatid)
        expected = False
        self.assertEqual(response, expected)
    '''
    def test_edirec_notok(self):
        path='/tmpo'
        expected = edirec.main(path)
        self.assertEqual(expected, False)
    '''
