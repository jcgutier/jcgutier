#!/usr/bin/env python

import unittest
from Python.Home import edirec

'''
Para correr estas pruebas con pytest se ejecuta desde el directorio padre:
    python -m pytest --cov-config Tests/.coveragerc --cov=Home --cov-report=term-missing Tests/
En el archivo .coveragec se especifican las configuraciones, en este caso se excluye la lina 
de if __name__ == "__main__"", para que se tenga un 100% de covertura
En caso que se quiera usar unittest se ejecuta:
    python -m unittest Tests.edirec_test
'''

class edirec_test(unittest.TestCase):
    def test_edirec_ok(self):
        path='/tmp'
        expected = edirec.main(path)
        self.assertEqual(expected, True)
    def test_edirec_notok(self):
        path='/tmpo'
        expected = edirec.main(path)
        self.assertEqual(expected, False)
