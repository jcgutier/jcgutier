#!/usr/bin/env python

import timeit
import time
import threading

start = timeit.default_timer()


def do_something(seconds):
    print 'Sleeping {} seconds(s)...'.format(seconds)
    time.sleep(seconds)
    print 'Done Sleeping...'

threads = []

for _ in range(10): # el '_' es una variable en blanco que no es usada.
    t = threading.Thread(target=do_something, args=[1.5])
    t.start()
    threads.append(t)

for thread in threads:
    thread.join()

finish = timeit.default_timer()

print 'Finished in {} second(s)'.format(round(finish-start))
