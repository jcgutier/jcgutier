#!/usr/bin/env python

resistencia_deseada = 450

resistencias1 = [1, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.1, 5.6, 6.8, 8.2]
resistencias2 = [1, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.1, 5.6, 6.8, 8.2]
multiplicador = [1, 10, 100, 1000, 10000, 100000, 1000000]

for mult in multiplicador:
    for resistencia1 in resistencias1:
        resistencia1 = resistencia1 * mult
        for resistencia2 in resistencias2:
            for mult2 in multiplicador:
                resistencia2 = mult2 * resistencia2
                resistencia_total = resistencia1 + resistencia2
                if resistencia_total == resistencia_deseada:
                    print(f"Las resistencias en serie R1={resistencia1} y R2={resistencia2} son {resistencia_deseada}")
                else:
                    if resistencia_total == 

for mult in multiplicador:
    for resistencia1 in resistencias1:
        resistencia1 = resistencia1 * mult
        for resistencia2 in resistencias2:
            for mult2 in multiplicador:
                resistencia2 = mult2 * resistencia2
                resistencia_total = (resistencia1 * resistencia1) / (resistencia1 + resistencia2)
                if resistencia_total == resistencia_deseada:
                    print(f"Las resistencias en paralelo R1={resistencia1} y R2={resistencia2} son {resistencia_deseada}")
