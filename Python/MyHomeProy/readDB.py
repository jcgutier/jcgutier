#!/usr/bin/python

from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData
from sqlalchemy.orm import mapper, sessionmaker

#Part 1: create engine
engine = create_engine('sqlite:///gastos.db')

#Part 2: metadata
metadata = MetaData()

filesystem_table = Table('control', metadata,
    Column('id', Integer, primary_key=True),
    Column('ingreso', Integer),
    Column('ahorro', Integer),
    Column('total', Integer),
    Column('restante', Integer),
)

metadata.create_all(engine)

#Part 3: mapped class
class Filesystem(object):
    def __init__(self, ingreso, ahorro, total, restante):
        self.ingreso = ingreso
        self.ahorro = ahorro
        self.total = total
        self.restante = restante

    def __repr__(self):
        return "[Filesystem('%s','%s','%s','%s')]" % (self.ingreso, self.ahorro, self.total, self.restante)

#Part 4: mapper function
mapper(Filesystem,filesystem_table)

#Part 5: create session
#Session = sessionmaker(bind=engine, autoflush=True, transactional=True)
Session = sessionmaker(bind=engine, autoflush=True)
session = Session()

#Part 6: crawl file system and populate database with results
#Part 7: commit to the database

#Part 8: query
for record in session.query(Filesystem):
    print "ID %s, Ing: %s , Aho: %s, ta: %s,Rest: %s " % (record.id,record.ingreso, record.ahorro, record.total, record.restante)
