from flask import render_template, flash, redirect, request, url_for
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from flask import Flask
 
app = Flask(__name__)
app.config.from_object('config')

class ReusableForm(Form):
    depot = StringField('Depot:', validators=[validators.DataRequired()])
    newstr = StringField('New String:', validators=[validators.DataRequired()])
    comments = StringField('Comments:', validators=[validators.DataRequired()])

@app.route('/', methods=['GET', 'POST'])
def sndbx():
    form = ReusableForm(request.form)
    if request.method == 'POST':
        depot=request.form['depot']
        newstr=request.form['newstr']
        comments=request.form['comments']
        if form.validate():
            com = '/opt/home/gos24x7/apps/Sandboxchange/p4sync.py '+depot+' '+newstr+' "'+comments+'"'
            succs = subprocess.call(com, shell=True)
            if succs == 0:
                flash('Success!', 'success')
            else:
                flash('Error with script please validate data', 'error')
        else:
            flash('Error missing fields', 'error')
    return render_template('sandbox.html', form=form) 


if __name__ == "__main__":
    app.run()
