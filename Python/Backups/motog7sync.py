#!/usr/bin/env python

#########################################################
## Necesita que este activo el servidor SSH(SimpleSSH) ##
## en el cel y que se haya mandado la llave publica.   ##
#########################################################

from scapy.all import *
import sys
import subprocess
import argparse
import os

def arping(iprange):
    """Arping function takes IP Address or Network,
    returns nested mac/ip list"""
    conf.verb=0
    ans, _ = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=iprange), timeout=2)
    collection = []
    for _, rcv in ans:
        result = rcv.sprintf(r"%ARP.psrc% %Ether.src%").split()
        collection.append(result)
    return collection

def copyfromremote(dirIP, local, remoto, usuario):
    copyCommand = f"rsync -ahvn --progress --delete -e 'ssh -p 2222' {usuario}@{dirIP}:{remoto} {local}"
    subprocess.call(copyCommand, shell=True)
    cntinue = input(f'Recibiendo el dir: "{remoto}". Continuar? (y/n)')
    if cntinue.lower() == 'y':
        copyCommand = f"rsync -ahv --progress --delete -e 'ssh -p 2222' {usuario}@{dirIP}:{remoto} {local}"
        subprocess.call(copyCommand, shell=True)
        subprocess.call(f"chown carlos:carlos -R {local}", shell=True)
        print("###############################################################################################################################")
        print ("")

def copytoremote(dirIP, local, remoto, usuario):
    copyCommand = f"rsync -ahvn --progress --delete -e 'ssh -p 2222' {local} {usuario}@{dirIP}:{remoto}"
    subprocess.call(copyCommand, shell=True)
    cntinue = input(f'Mandando el dir: "{local}". Continuar? (y/n)')
    if cntinue.lower() == 'y':
        copyCommand = f"rsync -ahv --progress --delete -e 'ssh -p 2222' {local} {usuario}@{dirIP}:{remoto}"
        subprocess.call(copyCommand, shell=True)
        subprocess.call(f"chown carlos:carlos -R {local}", shell=True)
        print("###############################################################################################################################")
        print ("")

def enviayrecibe(componente, directorioLocal, directorioRemoto):
    directorioLocal = f'{directorioLocal}/{directorios[componente][1]}'
    directorioRemoto = f'{directorioRemoto}/{directorios[componente][0]}'
    copyfromremote(ip, directorioLocal, directorioRemoto, usuario)
    copytoremote(ip, directorioLocal, directorioRemoto, usuario)

def enviatodo(directorios, directorioLocal, directorioRemoto):
    for componente in directorios:
        directorioLocal = f'{directorioLocal}/{directorios[componente][1]}'
        directorioRemoto = f'{directorioRemoto}/{directorios[componente][0]}'
        copytoremote(ip, directorioLocal, directorioRemoto, usuario)
        directorioLocal = os.environ["HOME"]
        directorioRemoto = "/storage/emulated/0"

def recibetodo(directorios, directorioLocal, directorioRemoto):
    for componente in directorios:
        directorioLocal = f'{directorioLocal}/{directorios[componente][1]}'
        directorioRemoto = f'{directorioRemoto}/{directorios[componente][0]}'
        copyfromremote(ip, directorioLocal, directorioRemoto, usuario)
        directorioLocal = os.environ["HOME"]
        directorioRemoto = "/storage/emulated/0"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Sync with moto')
    parser.add_argument('-a', '--address', help="IP address")
    parser.add_argument('-r', '--range', help="IP range")
    parser.add_argument('-u', '--user', help="Usuario para ssh")
    parser.add_argument('-w', '--wallpaper', action='store_true')
    parser.add_argument('-l', '--llamadas', action='store_true')
    parser.add_argument('-p', '--pics', action='store_true')
    parser.add_argument('-W', '--whatsapp', action='store_true')
    parser.add_argument('-d', '--descargas', action='store_true')
    parser.add_argument('-s', '--scan', action='store_true')
    parser.add_argument('-m', '--musica', action='store_true')
    parser.add_argument('-g', '--gwell', action='store_true')
    parser.add_argument('-S', '--screenshot', action='store_true')
    parser.add_argument('-e', '--enviar', action='store_true')
    parser.add_argument('-R', '--recibir', action='store_true')
    args = parser.parse_args()
    address = args.address
    iprange = args.range
    usuario = args.user or os.environ["USER"]
    wallpaper = args.wallpaper
    llamadas = args.llamadas
    pics = args.pics
    whatsapp = args.whatsapp
    descargas = args.descargas
    scan = args.scan
    musica = args.musica
    gwell = args.gwell
    screenshot = args.screenshot
    enviar = args.enviar
    recibir = args.recibir
    print(usuario)
    if iprange:
        for rangeip in sys.argv[1:]:
            print("arping", rangeip)
            iplist=arping(rangeip)
    elif address:
        iplist = [[address, '58:d9:c3:b0:c2:0a']]
    else:
        iplist=arping('192.168.0.0/24')
    print("Las IP que encontre son:")
    for macip in iplist:
        print(macip)
    for ipmac in iplist:
        print(ipmac[1])
        if ipmac[1] == '58:d9:c3:b0:c2:0a':
            ip=ipmac[0]
            print("La IP de Moto G7 es:  ", ip)
            directorios = {"Wallpaper": ["Imagenes/Cool/Wallpaper/Cel/", "Imagenes/Otros/Wallpaper/Cel/"],
                            "Llamadas": ["CubeCallRecorder/All/", "Documentos/CallsRecordings/PendienteRevision/"],
                            "Pics": ["Pictures/", "Imagenes/PendienteRevisionCel/Pictures/"],
                            "Whatsapp": ["WhatsApp/Media/", "Imagenes/PendienteRevisionCel/Whatsapp/Media/"],
                            "Descargas": ["Download/", "Documentos/RevPendienteCel/Download/"],
                            "Scan": ["hpscan/", "Documentos/RevPendienteCel/hpscan/"],
                            "Musica": ["Music/", "Musica/"],
                            "Gwell": ["gwellvideorec/", "Videos/Gutipendiente/"],
                            "Screenshot": ["screenshot/", "Imagenes/PendienteRevisionCel/Guticam/"]
                            }
            directorioLocal = os.environ["HOME"]
            directorioRemoto = "/storage/emulated/0"
            if wallpaper:
                componente = "Wallpaper"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if llamadas:
                componente = "Llamadas"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if pics:
                componente = "Pics"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if whatsapp:
                componente = "Whatsapp"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if descargas:
                componente = "Descargas"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if scan:
                componente = "Scan"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if musica:
                componente = "Musica"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if gwell:
                componente = "Gwell"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if screenshot:
                componente = "Screenshot"
                enviayrecibe(componente, directorioLocal, directorioRemoto)
            if not any([wallpaper, llamadas, pics, whatsapp, descargas, scan, musica, gwell, screenshot]):
                if enviar:
                    enviatodo(directorios, directorioLocal, directorioRemoto)
                elif recibir:
                    recibetodo(directorios, directorioLocal, directorioRemoto)
                else:
                    print("")
                    print("################################################################")
                    print("## Inicia proceso para recibir datos de todos los directorios ##")
                    print("################################################################")
                    print("")
                    recibetodo(directorios, directorioLocal, directorioRemoto)
                    print("")
                    print("###############################################################")
                    print("## Inicia proceso para enviar datos de todos los directorios ##")
                    print("###############################################################")
                    print("")
                    enviatodo(directorios, directorioLocal, directorioRemoto)
