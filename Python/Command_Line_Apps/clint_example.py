#!/usr/bin/env python

from clint.textui import puts, indent, colored,prompt, validators, progress
from clint import resources

puts('Not idented text')
with indent(4):
    puts('Idented text')
with indent(4, quote=' >'):
    puts('quoted text')
    puts('pretty cool, eh?')
puts(colored.red('red text'))
path = prompt.query('Installation Path', default='/usr/local/bin/', validators=[validators.PathValidator()])
for color in colored.COLORS:
    puts(getattr(colored,color)('Text in {0:s}'.format(color.upper())))
for i in progress.dots(range(20)):
    i=1
