#!/usr/bin/env python

##########################################
## Ejemplo de expresiones condicionales ##
##########################################
a = 1
b = 1
valor = 10 if a == b else 8
print(valor)
b = 2
valor = 10 if a == b else 8
print(valor)
##########################################