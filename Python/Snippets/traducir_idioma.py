#!/usr/bin/env python
import argparse
import os
import sys

#############################################################################
## Se necesita configurar una cuenta con el servicio gratuito de Microsoft ##
## translate, con una cuenta de Azure para usar la API de traduccion.      ##
##                                                                         ##
## Se tiene que configurar la llave de acceso en una variable de ambiente  ##
## llamada 'MS_TRANSLATOR_KEY' para que sea leida por el programa.         ##
##                                                                         ##
## Tambien se necesita instalar requests, para hacer una llamada a la API  ##
## Instalacion: pip install requests                                       ##
#############################################################################
def translate(text, source_language, dest_language):
    import json
    import requests
    if 'MS_TRANSLATOR_KEY' not in os.environ: 
        print('Error: the translation service is not configured.')
        sys.exit(1)
    auth = {'Ocp-Apim-Subscription-Key': os.environ['MS_TRANSLATOR_KEY']}
    r = requests.get('https://api.microsofttranslator.com/v2/Ajax.svc'
                     '/Translate?text={}&from={}&to={}'.format(
                         text, source_language, dest_language),
                     headers=auth)
    if r.status_code != 200:
        print('Error: the translation service failed.')
        sys.exit(1)
    print(json.loads(r.content.decode('utf-8-sig')))

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--mensaje")
parser.add_argument("-s", "--source")
parser.add_argument("-d", "--destination")
args = parser.parse_args()
mensaje = args.mensaje
source_language = args.source
dest_language = args.destination
translate(mensaje, source_language, dest_language)
