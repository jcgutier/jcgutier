#!/usr/bin/env python

from elasticsearch import Elasticsearch
import json
import pprint

pp = pprint.PrettyPrinter(indent=1)
es = Elasticsearch('http://localhost:9200')
es.index(index='my_index', id=1, body={'text': 'this is a test'})
es.index(index='my_index', id=2, body={'text': 'a second test'})
result = es.search(index='my_index', body={'query': {'match': {'text': 'this test'}}})
pp.pprint(result)
print()
result = es.search(index='my_index', body={'query': {'match': {'text': 'second'}}})
pp.pprint(result)
es.indices.delete('my_index')
