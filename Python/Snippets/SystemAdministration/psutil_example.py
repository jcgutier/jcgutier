#!/usr/bin/env python

#Psutil systemAdmin tool to monitor a host
from psutil import cpu_percent, net_io_counters, virtual_memory, swap_memory, disk_usage, disk_partitions, net_io_counters, net_connections, net_if_addrs, net_if_stats, sensors_temperatures, sensors_fans, sensors_battery, users, boot_time, pids, Process, test

#Monitor CPU overall
print(cpu_percent())
print(cpu_percent(interval=1))
print(cpu_percent(interval=1))
print(cpu_percent(interval=1))
#Monitor CPU per cpu
print(cpu_percent(interval=1, percpu=True))

#Monitor Network
print net_io_counters()
print net_io_counters()[1]
print net_io_counters()[0]
print(net_io_counters(pernic=True))
print(net_connections())
print(net_if_addrs())
print(net_if_stats())

#Monitor memory
print(virtual_memory())
print(swap_memory())

#Monitor Disks
print(disk_partitions())
print(disk_usage('/'))

#Monitor Sensors
print(sensors_temperatures())
print(sensors_fans())
print(sensors_battery())

#Monitor other info
print(users())
print(boot_time())

#Monitor and manage pids
print(pids())
p = Process(14268)
print(p.name())
print(p.exe())
print(p.cwd())
print(p.cmdline())
print(p.pid)
print(p.ppid())
print(p.parent())
print(p.children())
print(p.status())
print(p.username())
print(p.create_time())
print(p.terminal())
print(p.uids())
print(p.gids())
print(p.cpu_times())
print(p.cpu_percent(interval=1.0))
print(p.cpu_affinity())
print(p.cpu_affinity([0, 1]))
print(p.cpu_num())
print(p.memory_info())
print(p.memory_full_info())
print(p.memory_percent())
print(p.memory_maps())
print(p.io_counters())
print(p.open_files())
print(p.connections())
print(p.num_threads())
print(p.num_fds())
print(p.threads())
print(p.num_ctx_switches())
print(p.nice())
p.nice(10) #Set nice to 10
print(p.environ())
print(p.as_dict())
print(p.is_running())
p.suspend()
p.resume()
p.terminate()
print(test())
