#!/usr/bin/env python
import argparse

#####################################################
## Detecion de idioma con guess_language-spirit    ##
## Instalacion: pip install guess_language-spirit  ##
#####################################################
def idioma_guess_language_spirit(mensaje):
    from guess_language import guess_language
    language = guess_language(mensaje)
    print(language)

#########################################
## Detecion de idioma con langdetect   ##
## Instalacion: pip install langdetect ##
## Nota: parece que trabaja mejor este ##
#########################################
def idioma_langdetect(mensaje):
    from langdetect import detect
    idioma = detect(mensaje)
    print(idioma)

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--mensaje")
args = parser.parse_args()
mensaje = args.mensaje
idioma_guess_language_spirit(mensaje)
idioma_langdetect(mensaje)