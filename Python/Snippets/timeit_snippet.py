#!/usr/bin/env python

import random
import timeit
def compare_list_map_generator_loops_time():
    TAX_RATE = .08
    txns = [random.randrange(100) for _ in range(100000)]
    def get_price(txn):
        return txn * (1 + TAX_RATE)
    def get_prices_with_map():
        return list(map(get_price, txns))
    def get_prices_with_generator():
        return list(get_price(txn) for txn in txns)
    def get_prices_with_comprehension():
        return [get_price(txn) for txn in txns]
    def get_prices_with_loop():
        prices = []
        for txn in txns:
            prices.append(get_price(txn))
        return prices
    print(f"Map: {timeit.timeit(get_prices_with_map, number=100)}")
    print(f"Generator: {timeit.timeit(get_prices_with_generator, number=100)}")
    print(f"Comprehension: {timeit.timeit(get_prices_with_comprehension, number=100)}")
    print(f"Loop: {timeit.timeit(get_prices_with_loop, number=100)}")

compare_list_map_generator_loops_time()