#!/usr/bin/env python

import os

def main(path):
    if os.path.isdir(path):
        print "{} is a directory".format(path)
        return True
    else:
        print "{} is not a directory".format(path)
        return False

if __name__ == "__main__":
    main('/tmp')
