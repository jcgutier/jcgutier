#!/home/carlos/.virtualenvs/Homep3/bin/python3

import os
import subprocess
import socket


try:
    #Getting Enviroment variables
    api_token = os.environ['TELEGRAM_API_TOKEN']
    #Setting the content of the service file
    hostname=socket.gethostname()
    servicecontent = f'''[Unit]
Description=Startup service
Wants=network-online.target
After=network.target network-online.target

[Service]
Environment="TELEGRAM_API_TOKEN={api_token}"
Environment="TEST=this_is_a_test"
ExecStart=/opt/startupnotifications/startupnotifications.sh

[Install]
WantedBy=default.target'''
    print('The service content is:')
    print(servicecontent)

    #Writing to the file
    with open('/etc/systemd/system/startnoti.service', 'w') as servicefile:
        servicefile.write(servicecontent)
    print('')
    print('Content written to file')

    print('')
    #Reloading the systemd daemon, enabling and starting the service
    subprocess.run(['systemctl', 'daemon-reload'])
    subprocess.run(['systemctl', 'enable', 'startnoti'])
    subprocess.run(['systemctl', 'start', 'startnoti'])
    subprocess.run(['systemctl', 'status', 'startnoti'])
except KeyError as err:
    print(err)
    print('Please run with: sudo -E')

