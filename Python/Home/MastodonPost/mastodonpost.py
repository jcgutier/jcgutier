#! /usr/bin/env python

import os
import argparse
from mastodon import Mastodon

parser = argparse.ArgumentParser(description='Post a message with image on Mastodon.')
parser.add_argument('-m', '--mensaje', required=True, help='Mensaje a mandar')
parser.add_argument('-i', '--imagen', help='Imagen a mandar')
args = parser.parse_args()
mensaje = args.mensaje
imagen = args.imagen

try:
    # Token y url de la Instancia
    mastodon = Mastodon(
        access_token = os.environ['MASTODONTOKEN'],
        api_base_url = 'https://mastodon.social/'
    )

    if imagen:
        media = mastodon.media_post(imagen)
        mastodon.status_post(mensaje, media_ids=media)
    else:
        mastodon.status_post(mensaje)
    print("Toot posteado")
except KeyError as err:    
    if "MASTODONTOKEN" in err.args:
        print("Configura el token como la variable de ambiente MASTODONTOKEN")
    else:
        print(err.with_traceback())