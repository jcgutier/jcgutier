#!/usr/bin/env python

import requests
import argparse
import os
import json

def get_me(api_token):
    url=f"https://api.telegram.org/bot{api_token}/getMe"
    response = requests.get(url)
    print(response.text)

def send_message(api_token, mensaje, chatid):
    url=f"https://api.telegram.org/bot{api_token}/sendMessage?text={mensaje}&chat_id={chatid}"
    response = requests.get(url)
    resp = response.json()
    if resp["ok"]:
        print("Mensaje enviado exitosamente")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Send a message on telegram')
    parser.add_argument('-m', '--message', help='Message to send')
    parser.add_argument('-c', '--chatid', default='698412637', help='Message to send')
    option = parser.parse_args()
    mensaje = option.message
    chatid = option.chatid
    api_token = os.environ["TELEGRAM_API_TOKEN"]
    # get_me(api_token)
    send_message(api_token, mensaje, chatid)
