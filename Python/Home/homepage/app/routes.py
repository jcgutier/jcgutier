from flask import render_template, request
from app import app

@app.route('/')
@app.route('/index')
def index():
    current = str(request.url_rule).strip('/')
    return render_template('index.html', current=current)

@app.route('/personal_profile')
def personal_profile():
    current = str(request.url_rule).strip('/')
    return render_template('personal_profile.html', current=current)
