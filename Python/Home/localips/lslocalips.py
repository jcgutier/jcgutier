#!/usr/bin/env python

from scapy.all import *
import sys
import subprocess

def arping(iprange):
    """Arping function takes IP Address or Network,
    returns nested mac/ip list"""
    conf.verb=0
    ans,unans=srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=iprange), timeout=2)
    collection = []
    for snd, rcv in ans:
        result = rcv.sprintf(r"%ARP.psrc% %Ether.src%").split()
        collection.append(result)
    return collection

if __name__ == '__main__':
    if len(sys.argv) > 1:
        for rangeip in sys.argv[1:]:
            print "arping", rangeip
            iplist=arping(rangeip)
    else:
        iplist=arping('192.168.0.0/24')
    for macip in iplist:
        print macip
