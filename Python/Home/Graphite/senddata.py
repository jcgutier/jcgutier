import graphyte
import time
import psutil

CARBON_SERVER = '10.6.6.108'

#graphyte.init(CARBON_SERVER, prefix='system.sync')
while True:
    cpuusage = psutil.cpu_percent()
    graphyte.init(CARBON_SERVER)
    graphyte.send('server.cpuusage', cpuusage)
    message = 'server.cpuusage {} {}\n'.format(cpuusage, int(time.time()))
    print 'sending message:\n%s' % message
    time.sleep(1)
