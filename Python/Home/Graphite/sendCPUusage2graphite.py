#!/usr/bin/env python

import socket
import time
import psutil

CARBON_SERVER = '10.6.6.108'
CARBON_PORT = 2003

while True:
    cpuusage = psutil.cpu_percent()
    message = 'server.cpuusage {} {}\n'.format(cpuusage, int(time.time()))
    print 'sending message:\n%s' % message
    sock = socket.socket()
    sock.connect((CARBON_SERVER, CARBON_PORT))
    sock.sendall(message)
    sock.close()
    time.sleep(1)
