from termcolor import colored
from cprint import * 

print colored('hello', 'red'), colored('world', 'green', attrs=['reverse'])
print colored('hola', 'grey')
print colored('hola', 'red')
print colored('hola', 'green')
print colored('hola', 'yellow')
print colored('hola', 'blue')
print colored('hola', 'magenta')
print colored('hola', 'cyan')
print colored('hola', 'white')
print colored('world', 'green', attrs=['bold'])
print colored('world', 'green', attrs=['dark'])
print colored('world', 'green', attrs=['underline'])
print colored('world', 'green', attrs=['blink'])
print colored('world', 'green', attrs=['reverse'])
print colored('world', 'green', attrs=['concealed'])
cprint(str) # WHITE 
cprint.ok(str)	# BLUE 
cprint.info(str)	# GREEN 
cprint.warn(str)	# YELLOW 
cprint.err(str, interrupt=False)	# BROWN 
cprint.fatal(str, interrupt=False)	# RED  
print colored('Hello, World!', 'green', 'on_red')
