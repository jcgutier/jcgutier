#!/usr/bin/python

import os

def main():
    for dirpath, dirnames, filenames in os.walk('/home/carlos//Documents/BackupUbuntu/Documentos/Notas'):
        #print dirpath, dirnames, filenames
        for filen in filenames:
            fullpath = os.path.join(dirpath, filen)
            webpath = '/home/carlos/Documents/Webs/'
            for i in range(1,len(fullpath.split('/'))-7):
                webpath=webpath+fullpath.split('/')[6+i]+'/'
            #print fullpath, webpath.rstrip('/'), filen.split('.')[0]
            to_html(fullpath, webpath.rstrip('/'), filen.split('.')[0])

def to_html(note_file, webpath, filename):
    n=0
    sec = 0
    sec1 = 0
    lista = 0
    code = 0
    with open(note_file,'r') as f:
        #contenido = f.read()
        #print contenido
        contenido = f.readlines()
    if '=====' in contenido[0]:
        print 'Escribiendo: '+webpath+'/'+filename+'.html'
        if os.path.isdir(webpath):
            pass
        else:
            print 'Creando:', webpath
            os.makedirs(webpath)
        with open(webpath+'/'+filename+'.html','w') as webfile:
            webfile.write('<html>')
            webfile.write('<head>')
            webfile.write('<title>'+contenido[1].strip()+r'</title>')
            webfile.write('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />')
            webfile.write('</head>')
            webfile.write('<body>')
            for linea in contenido:
                if linea == contenido[1]:
                    webfile.write('<h1>'+contenido[1].strip()+r'</h1>')
                elif '======' in linea:
                    pass
                    if n != 0:
                        sec1 = 1
                        webfile.write('<hr/>')
                elif '----' in contenido[n+1]:
                    webfile.write('<h3>'+linea.strip()+'</h3>')
                    sec = 1
                elif sec == 1:
                    pass
                    sec = 0
                elif sec1 == 1:
                    webfile.write('<h2>'+linea.strip()+'</h2>')
                    sec1 = 0
                elif '|||' in linea:
                    if lista == 1:
                        lista = 0
                        webfile.write('</ul>')
                    else:
                        lista = 1
                        webfile.write('<ul>')
                elif lista == 1:
                    webfile.write('<li>'+linea.strip()+'</li>')
                elif ':::' in linea:
                    if code == 1:
                        code = 0
                        webfile.write('</code>')
                    else:
                        code = 1
                        webfile.write('<code>')
                elif code == 1:
                    webfile.write(linea.strip()+'<br/>')
                else:
                    webfile.write('<p>'+linea.strip()+'</p>')
                n+=1 
            webfile.write('</body>')
            webfile.write('</html>')


if __name__=='__main__':
    main()
