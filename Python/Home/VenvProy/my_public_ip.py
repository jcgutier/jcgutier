#!/usr/bin/env python

import requests

response = requests.get('https://httpbin.org/ip')
print('My public IP is {0}'.format(response.json()['origin']))
