#!/usr/bin/python3

import subprocess
import pip
import sys
#import apt
#import sys
#import platform

##################################
## Instalacion de paquetes snap ##
##################################
#if "Linux" in platform.system() and "Ubuntu" in platform.dist():
snap_packages = [
    "notepadqq",
    "spotify",
    "mailspring",
]
for snappkg in snap_packages:
    print("Instalando ", snappkg)
    print(subprocess.run(["snap", "install", snappkg], stdout=subprocess.PIPE).stdout.decode('utf-8'))
##################################


##################################
## Instalacion de paquetes pip3 ##
##################################
pip_packages = [
    "pip",
    "setuptools",
    "virtualenvwrapper",
    "scapy",
    "awscli",
]
import pkg_resources

dists = [d.project_name for d in pkg_resources.working_set]
for pippkg in pip_packages:
    print("Instalando ", pippkg)
    if pippkg in dists:
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', pippkg, '--upgrade'])
    else:
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', pippkg])
#################################



'''
with open("aptpackagesnames.txt") as aptpackages:
    pkg_names = [x.strip() for x in aptpackages.readlines() if not x.startswith("#")]
print("Paquetes a instalar:", pkg_names)

#Agregando los repositorios de clementine, ejemplo
#if 'clementine' in pkg_names:
#    print("Agregando el repositorio de clementine")
#    print(subprocess.run(["add-apt-repository", "-y", "ppa:me-davidsansome/clementine"], stdout=subprocess.PIPE).stdout.decode('utf-8'))

print("Actualizando la cache de apt")
cache = apt.cache.Cache()
cache.update()
cache.open()

for packagename in pkg_names:
    pkg = cache[packagename]
    if pkg.is_installed:
        print("{pkg_name} ya esta instalado".format(pkg_name=packagename))
    else:
        pkg.mark_install()
        try:
            cache.commit()
        except Exception as arg:
            print(sys.stderr, "Sorry, package installation failed [{err}]".format(err=str(arg)))

#snaps_names = ['']
'''
