#!/usr/bin/pyton

from itertools import islice
import subprocess, os, sys

#ConfigFile="commondefines.cfg"
ConfigFile="new.txt"

def p4sync(FullBranch):
	#Sync the path to get the latest version
	command="p4 sync "+FullBranch
	print "[[Executing]] -> ", command
	subprocess.call(command, shell=True)

def p4edit(FullBranch, ticket):
	FullBranch=FullBranch.lstrip('//')
	command="p4 edit ~/p4/"+FullBranch
	print "[[Executing]] -> ", command
	subprocess.call(command, shell=True)
	Home = os.getenv("HOME")
	PathFile=Home+"/p4/"+FullBranch
	#BkpFile=Home+"/"+ConfigFile+".bkp"
	#Backup="cp "+PathFile+" "+BkpFile
	#print "[[Executing]] -> ", Backup
	#subprocess.call(Backup, shell=True)
	file = open(PathFile, "r+")
	text = file.read()
	if (text.find('prueba')>=0):
		file.seek(0)
		file.write(text.replace('prueba','test'))
	else:
		print "ERROR: No se encontro la cadena"
	file.close()
	#diff="diff "+BkpFile+" test/file.txt"
	#print "[[Executing]] -> ", diff
	#subprocess.call(diff, shell=True)
	diff="p4 diff ~/p4/"+FullBranch
	print "[[Executing]] -> ", diff
	subprocess.call(diff, shell=True)
	submit='p4 submit -d "Under Ticket: '+ticket+'"'
	print "[[Executing]] -> ", submit
	subprocess.call(submit, shell=True)

def p4filelog(FullBranch):
        FullBranch=FullBranch.lstrip('//')
        command="p4 filelog ~/p4/"+FullBranch
	print "[[Executing]] -> ", command
	subprocess.call(command, shell=True)
	

if __name__ == '__main__':
	import argparse
	
	parser = argparse.ArgumentParser(description='Sandbox change configuration.')
	parser.add_argument("-b", "--branch", dest="branch", help="Branch of the title")
	parser.add_argument("-l", "--logfile", dest="logfile", action='store_true', help="Show the history of file")
	parser.add_argument("-t", "--ticket", dest="ticket", help="Ticket for this change")
	option = parser.parse_args()
	if (len(option.ticket)==0):
		print "ERROR: There is no ticket use -h for help"
		sys.exit(2)
	#FullBranch=option.branch+"/etc/commondefines.cfg"
	FullBranch=option.branch+"/etc/new.txt"
	p4sync(FullBranch)
	p4edit(FullBranch, option.ticket)
	if option.logfile:
                p4filelog(FullBranch)
