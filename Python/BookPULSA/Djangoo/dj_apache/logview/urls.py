"""dj_apache URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
#from django.conf.urls.defaults import *
from . import views

#"""
urlpatterns = [
    #url(r'^$', views.index, name='index')
    url(r'^$', views.list_files)
    #url(r'^$', 'dj_apache.logview.views.list_files')
    url(r'^(?P<sortmethod>.*?)/(?P<filename>.*?)/$','views.view_log')
]

"""
urlpatterns = patterns('',
    (r'^$', 'dj_apache.logview.views.list_files'),
    (r'^viewlog/(?P<sortmethod>.*?)/(?P<filename>.*?)/$',
        'dj_apache.logview.views.view_log'),
)
"""
