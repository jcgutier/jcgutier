#!/usr/bin/python

from sqlalchemy import Table, Column, Integer, String, MetaData
from sqlalchemy.orm import mapper, sessionmaker
from sqlalchemy import create_engine

engine = create_engine('sqlite:///memory.db')
metadata = MetaData()
filesystem_table = Table('filesystem', metadata,
    Column('id', Integer, primary_key=True),
    Column('path', String(500)),
    Column('file', String(255)),
)
class Filesystem(object):
    def __init__(self, path, filed):
        self.path = path
        self.file = filed

    def __repr__(self):
        return "[Filesystem('%s','%s')]" % (self.path, self.file)
#Part 4: mapper function
mapper(Filesystem,filesystem_table)
#Part 5: create session
Session = sessionmaker(bind=engine, autoflush=True)
session = Session()
#Part 8: query
for record in session.query(Filesystem):
    print "ID: %s, File: %s " % (record.id, record.file)
