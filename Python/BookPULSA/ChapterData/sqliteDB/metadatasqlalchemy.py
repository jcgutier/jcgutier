#!/usr/bin/python

from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.orm import mapper, sessionmaker
import os

#path
path = " /tmp"

#Part 1: create engine
engine = create_engine('sqlite:///memory.db')

#Part 2: metadata
metadata = MetaData()

filesystem_table = Table('filesystem', metadata,
    Column('id', Integer, primary_key=True),
    Column('path', String(500)),
    Column('file', String(255)),
)

metadata.create_all(engine)

#Part 3: mapped class
class Filesystem(object):
    def __init__(self, path, filed):
        self.path = path
        self.file = filed

    def __repr__(self):
        return "[Filesystem('%s','%s')]" % (self.path, self.file)

#Part 4: mapper function
mapper(Filesystem,filesystem_table)

#Part 5: create session
#Session = sessionmaker(bind=engine, autoflush=True, transactional=True)
Session = sessionmaker(bind=engine, autoflush=True)
session = Session()

#Part 6: crawl file system and populate database with results
#for dirpath, dirnames, filenames in os.walk(path):
#    print "hola"
#    for file in filenames:
#        print file
#        fullpath = os.path.join(dirpath, file)
#        print fullpath
#        record = Filesystem(fullpath, file)
#        print record
        #session.save(record)

for root, dirs, files in os.walk("/tmp"):
    for name in files:
        fullpath = os.path.join(root, name)
        #print fullpath
        record = Filesystem(fullpath, name)
        print record
        #session.save(record)
        session.add(record)

#Part 7: commit to the database
session.commit()

#Part 8: query
for record in session.query(Filesystem):
    print "Database Record Number: %s, Path: %s , File: %s " % (record.id,record.path, record.file)
