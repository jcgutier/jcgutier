#!/usr/bin/python

import os
import shutil
import filecmp

print os.getcwd()
print os.mkdir("os_mod_explore")
print os.listdir("os_mod_explore")
print os.listdir("/home/carlos")
print os.stat("os_mod_explore")
print os.rename("os_mod_explore", "test_dir1_renamed")
print os.listdir('.')
print os.rmdir('test_dir1_renamed')
print "=== shutil ==="
print os.chdir("/tmp")
#print os.makedirs("test/test_subdir1/test_subdir2")
print shutil.copytree("test", "test-copy")
print shutil.move("test-copy", "test-copy-moved")
print shutil.rmtree("test-copy-moved")
print "=== filecmp ==="
print filecmp.cmp("file0.txt", "file1.txt")
print filecmp.cmp("file0.txt", "file00.txt")
print filecmp.dircmp("dirA", "dirB").diff_files
print filecmp.dircmp("dirA", "dirB").same_files
print filecmp.dircmp("dirA", "dirB").report()
