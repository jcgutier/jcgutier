#!/usr/bin/python

from checksum import createChecksum
from diskwalk_api import diskwalk
import os
import subprocess
import argparse

def deldup(fpath):
    d = diskwalk(fpath)
    files = d.enumeratePaths()
    #print len(files)
    dup = []
    record = {}
    for file in files:
        compound_key = (os.path.getsize(file),createChecksum(file))
        if compound_key in record:
            print file
            #subprocess.call('phototonic "'+file+'" &', shell=True)
            #print record[compound_key]
            #subprocess.call('phototonic "'+record[compound_key]+'"', shell=True)
            #var = raw_input("Quieres eliminar: "+file+'?  ')
            #if var == 'y':
            #    os.remove(file)
            #    print "Eliminado -> "+file
        else:
            record[compound_key] = file

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='Instances in cfg files and running on servers')
    PARSER.add_argument("-p", "--path", dest="path", help="PORT for server")
    OPTION = PARSER.parse_args()
    print 'Path: %s' % OPTION.path
    deldup(OPTION.path)
