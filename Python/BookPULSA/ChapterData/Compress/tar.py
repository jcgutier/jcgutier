import tarfile
import os

tar = tarfile.open("largefile.tar", "w")
tar.add("largeFile.txt")
tar.close()
print "=== tar tree ==="
tar = tarfile.open("temp.tar", "w")
for root, dir, files in os.walk("/tmp"):
    for file in files:
        fullpath = os.path.join(root,file)
        tar.add(fullpath)
tar.close()
print "=== tar con bzip2 ==="
tar = tarfile.open("largefilecompressed.tar.bzip2", "w|bz2")
tar.add("largeFile.txt")
tar.close()
print "=== tar con gzip ==="
tar = tarfile.open("largefile.tar.gzip", "w|gz")
tar.add("largeFile.txt")
tar.close()
