#!/usr/bin/python

import xml.etree.cElementTree as ET

def buscachild(root):
    if len(root):
        for child in root:
            print child.tag
            buscachild(child)

tree = ET.parse('example.xml')
root = tree.getroot()
print root.tag
buscachild(root)
print "=== .iter() ==="
for neighbor in root.iter():
    print neighbor.tag
for neighbor in root.iter('neighbor'):
    print neighbor.attrib
