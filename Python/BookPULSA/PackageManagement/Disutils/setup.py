#Installer for distutils example script

from distutils.core import setup

setup(name="distutils_example",
      version="0.1",
      description="A Completely Useless Script That Prints",
      author="Joe Blow",
      author_email = "joe.blow@pyatl.org",
      url = "http://www.pyatl.org")
