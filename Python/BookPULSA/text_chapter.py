#!/usr/bin/python

import subprocess

res = subprocess.Popen(['uname', '-sv'], stdout=subprocess.PIPE)
uname = res.stdout.read().strip()
print uname
print "===in & not in ==="
print 'Linux' in uname
result = 'Linux' in uname
print result
print 'Java' in uname
print 'Java' not in uname
print "=== find() & index ==="
print uname.index('Linux')
#uname.index('linux')
print uname.find('Linux')
print uname.find('linux')
smp_index = uname.index('SMP')
print smp_index
print uname[smp_index:]
print uname[:smp_index]
print "=== startswith() & endswith() ==="
some_string = "Raymond Luxury-Yacht"
print some_string
print some_string.startswith("Raymond")
print some_string.startswith("Ray")
print some_string.startswith("ay")
print some_string.endswith("Luxury-Yacht")
print some_string.endswith("Yacht")
print some_string.endswith("Yach")
print "=== lstrip() strip() & strip()==="
spacious_string = "\n\t Some Non-Spacious Text\n \t\r"
print spacious_string
print spacious_string.lstrip()
print spacious_string.rstrip()
print spacious_string.strip()
foo_str = "<fooooooo>bl>ah<foo>"
print foo_str
print foo_str.strip("<foo>")
foo_str = "<fooooooo>>blah<foo>"
print foo_str
print foo_str.strip("<foo>")
print foo_str.strip(">o<f")
print "=== split() ==="
comma_delim_string = "pos1,pos2,pos3"
print comma_delim_string
pipe_delim_string = "pipepos1|pipepos2|pipepos3"
print pipe_delim_string
print comma_delim_string.split(',')
print pipe_delim_string.split('|')
multi_delim_string = "pos1XXXpos2XXXpos3"
print multi_delim_string
print multi_delim_string.split("XXX")
print multi_delim_string.split("XX")
print multi_delim_string.split("X")
two_field_string = "8675309,This is a freeform, plain text, string"
print two_field_string
print two_field_string.split(',', 1)
multiline_string = """This
is
a multiline
piece of
text"""
print multiline_string
print multiline_string.split()
lines = multiline_string.splitlines()
print lines
for line in lines:
    print "START LINE::"
    print line.split()
    print "::END LINE"
print "=== join() ==="
some_list = ['one', 'two', 'three', 'four']
print some_list
print ','.join(some_list)
print ', '.join(some_list)
print '\t'.join(some_list)
print ''.join(some_list)
some_list = range(10)
print some_list, type(some_list[0])
#",".join(some_list) -> Error
print ",".join([str(i) for i in some_list])
print "=== replace() ==="
replacable_string = "trancendental hibernational nation"
print replacable_string
print replacable_string.replace("nation", "natty")
print "=== Unicode ==="
unicode_string = u'this is a unicode string'
print unicode_string
print unicode('this is a unicode string')
unicode_string = u'abc_\u03a0\u03a3\u03a9_\u0414\u0424\u042F'
print unicode_string
print unicode_string.encode('utf-8')
