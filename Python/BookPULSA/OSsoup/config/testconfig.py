#!/usr/bin/python

import subprocess
import ConfigParser
import re

def readConfig(file="server.cfg"):
    Config = ConfigParser.ConfigParser()
    Config.read(file)
    reob = re.compile(r'\w+')
    total = 0
    servercfg={}
    for each_section in Config.sections():
        if 'ea.com' in each_section:
            print each_section
            sect = 0
            for (each_key, each_val) in Config.items(each_section):
                if each_key == 'runset':
                    newval = each_val.strip().split('\n')
                    for va in newval:
                        comp = reob.findall(va)
                        sect = sect + int(comp[1])
            print sect
            servercfg[each_section]=sect
            total = total + sect
    print "TOTAL = "+str(total)
    print servercfg

readConfig()
