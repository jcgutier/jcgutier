#!/usr/bin/python

import paramiko
import os

hostname = '192.168.1.77'
port = 22
username = 'carlos'
password = 'Hpuser$04nw'
dir_path = '/home/carlos/jsjk'

if __name__ == "__main__":
    t = paramiko.Transport((hostname, port))
    t.connect(username=username, password=password)
    sftp = paramiko.SFTPClient.from_transport(t)
    files = sftp.listdir(dir_path)
    for f in files:
        print 'Retrieving', f
        sftp.get(os.path.join(dir_path, f), f)
    t.close()
