#!/usr/bin/python

import paramiko
import sys

def connssh(hostname):
    port = 22
    username = 'carlos'
    password = 'Hpuser$04nw'
    paramiko.util.log_to_file('paramiko.log')
    s = paramiko.SSHClient()
    try:
        s.load_system_host_keys()
        #s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        s.connect(hostname, port, username, password)
        stdin, stdout, stderr = s.exec_command('ls -l')
        print stdout.read()
    except paramiko.ssh_exception.NoValidConnectionsError:
        print "Connection failed"
    except paramiko.ssh_exception.SSHException:
        print "Please check known_host that has RSA key, ecdsa not supported"
    finally:
        s.close()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        connssh(sys.argv[1])
    else:
        connssh('localhost')
