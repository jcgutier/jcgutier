#!/usr/bin/python

import paramiko
import sys
import getpass

def connssh(hostname):
    port = 22
    username = 'carlos'
    paramiko.util.log_to_file('paramiko.log')
    s = paramiko.SSHClient()
    password = getpass.getpass()
    try:
        s.load_system_host_keys()
        s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        s.connect(hostname, port, username, password)
        stdin, stdout, stderr = s.exec_command('hostname -i')
        print stdout.read()
        return True
    except paramiko.ssh_exception.NoValidConnectionsError:
        print "Connection failed"
        return False
    except paramiko.ssh_exception.SSHException:
        print "Please check known_host that has RSA key, ecdsa not supported"
        return False
    except:
        print "Fallo la conexion"
        return False
    finally:
        s.close()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        connssh(sys.argv[1])
    else:
        connssh('localhost')
