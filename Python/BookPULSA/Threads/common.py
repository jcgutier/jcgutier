import subprocess
import time

IP_LIST = [ 'google.com', 'yahoo.com', 'yelp.com', 'amazon.com', 'freebase.com', 'clearink.com', 'ironport.com', 'some.com']
cmd_stub = 'ping -c 5 %s'

def do_ping(addr):
    print time.asctime(), "DOING PING FOR", addr
    cmd = cmd_stub % (addr,)
    return subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
