#!/usr/bin/python
import re
re_string = "{{(.*?)}}"
some_string = "this is a string with {{words}} embedded in {{curly brackets}} to show an {{example}} of {{regular expressions}}"
print some_string
print "=== ==="
for match in re.findall(re_string, some_string):
    print "MATCH->", match
re_string = "{{(.*)}}"
print "=== ==="
for match in re.findall(re_string, some_string):
    print "MATCH->", match
print "=== ==="
re_string = "{{.*?}}"
print type(re.findall(re_string, some_string))
for match in re.findall(re_string, some_string):
    print "MATCH->", match
print "=== compiled ==="
re_obj = re.compile("{{(.*?)}}")
for match in re_obj.findall(some_string):
    print "MATCH->", match
print "=== ==="
raw_pattern = r'\b[a-z]+\b'
non_raw_pattern = '\b[a-z]+\b'
some_string = 'a few little words'
print some_string
print re.findall(raw_pattern, some_string)
print re.findall(non_raw_pattern, some_string)
some_other_string = 'a few \blittle\b words'
print some_other_string
print re.findall(non_raw_pattern, some_other_string)
print "=== groups ==="
re_obj = re.compile(r'(A\W+\b(big|small)\b\W+\b(brown|purple)\b\W+\b(cow|dog)\b\W+\b(ran|jumped)\b\W+\b(to|down)\b\W+\b(the)\b\W+\b(street|moon).*?\.)',re.VERBOSE)
print re_obj.findall('A big brown dog ran down the street. A small purple cow jumped to the moon.')
print "=== ==="
re_obj = re.compile(r"""(A\W+\b(big|small)\b\W+\b
    (brown|purple)\b\W+\b(cow|dog)\b\W+\b(ran|jumped)\b\W+\b
    (to|down)\b\W+\b(the)\b\W+\b(street|moon).*?\.)""",
    re.VERBOSE)
print re_obj.findall('A big brown dog ran down the street. \
A small purple cow jumped to the moon.')
re_obj = re.compile(r"""(A\W+\b(big|small)\b\W+\b
    (brown|purple)\b\W+\b(cow|dog)\b\W+\b(ran|jumped)\b\W+\b
    (to|down)\b\W+\b(the)\b\W+\b(street|moon).*?\.)""",
    )
print re_obj.findall('A big brown dog ran down the street. \
A small purple cow jumped to the moon.')
print "=== ==="
some_string = "this is a string with {{words}} embedded in {{curly brackets}} to show an {{example}} of {{regular expressions}}"
print some_string
re_obj = re.compile("{{(.*?)}}")
print re_obj.sub(r'%s' % "{{cambio}}", some_string)
