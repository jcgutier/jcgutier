#!/usr/bin/python

from __future__ import with_statement

outputfile = open("foo_out.txt", "w")
outputfile.write("This is\nSome\nRandom\nOutput Text\n")
outputfile.close()
try:
    f = open('writeable.txt', 'w')
    f.write('quick line here\n')
finally:
    f.close()
with open('writeable.txt', 'w') as f:
    f.write('this is a writeable file\n')
#f -> Error because ir is closed
f = open("foo_out.txt", "r")
print f.read()
print "=== ==="
f = open("foo_out.txt", "r")
print f.readline()
print f.readline(7)
print f.readline()
print f.readline(7)
print f.readline()
print "=== ==="
f = open("foo_out.txt", "r")
inputt=f.readlines()
print inputt
print "=== ==="
f = open("some_writable_file.txt", "w")
f.write("Test\nFile\n")
f.close()
g = open("some_writable_file.txt", "r")
print g.read()
f = open("writelines_outfile.txt", "w")
f.writelines("%s\n" % i for i in range(10))
f.close()
g = open("writelines_outfile.txt", "r")
print g.read()
def myRange(r):
    i = 0
    while i < r:
        yield "%s\n" % i
        i += 1
f = open("writelines_generator_function_outfile", "w")
f.writelines(myRange(10))
f.close()
g = open("writelines_generator_function_outfile", "r")
print g.read()
