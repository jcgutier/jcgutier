import pickle

some_dict = {'a': [1, 5, 7, 'some name'], 'b': 2}
pickle_file = open('some_dict', 'w')
pickle.dump(some_dict, pickle_file)
pickle_file.close()
pickle_file = open('some_dict', 'r')
another_name_for_some_dict = pickle.load(pickle_file)
print another_name_for_some_dict
list_of_dicts = [{str(i): i} for i in range(5)]
print list_of_dicts
pickle_file = open('list_of_dicts.pkl', 'w')
for d in list_of_dicts:
    pickle.dump(d, pickle_file)
pickle_file.close()
pickle_file = open('list_of_dicts.pkl', 'r')
while 1:
    try:
        print pickle.load(pickle_file)
    except EOFError:
        print "EOF Error"
        break
