import pickle
default_pickle_file = open('default.pkl', 'w')
binary_pickle_file = open('binary.pkl', 'wb')
d = {'a': 1}
pickle.dump(d, default_pickle_file)
pickle.dump(d, binary_pickle_file, -1)
default_pickle_file.close()
binary_pickle_file.close()
