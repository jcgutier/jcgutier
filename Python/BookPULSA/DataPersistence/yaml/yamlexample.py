import yaml
yaml_file = open('test.yaml', 'w')
d = {'foo': 'a', 'bar': 'b', 'bam': [1, 2,3]}
yaml.dump(d, yaml_file, default_flow_style=False)
yaml_file.close()
yaml_file = open('test.yaml', 'r')
my_obj = yaml.load(yaml_file)
print my_obj
yaml_file.close()
