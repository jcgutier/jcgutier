import sqlite3
from tabulate import tabulate

imprime = []
conn = sqlite3.connect('inventory.db')
try:
    conn.execute('''CREATE TABLE "inventory_operatingsystem"
              ("id" integer NOT NULL PRIMARY KEY,
              "name" varchar(50) NOT NULL,
              "description" text NULL)''')
except sqlite3.OperationalError as e:
    pass
cursor = conn.execute("insert into inventory_operatingsystem (name, description) values ('Linux', '2.0.34 kernel');")
#print cursor.fetchall()
conn.commit()
cursor = conn.execute('select * from inventory_operatingsystem;')
#print cursor.fetchall()
for row in conn.execute('select * from inventory_operatingsystem;'):
    imprime.append(row)
print tabulate(imprime, headers=['ID','OS','Desc'], tablefmt="rst")
conn.close()
