#!/usr/bin/env python
import nmap # import nmap.py module
nm = nmap.PortScanner() # instantiate nmap.PortScanner object
print nm.scan('127.0.0.1', '22-443') 
print ''
print nm.command_line() #command used
print ''
print nm.scaninfo()
print ''
print nm.all_hosts()
print ''
print nm['127.0.0.1'].hostname()  # get one hostname for host 127.0.0.1, usualy the user record
print ''
print nm['127.0.0.1'].state() # get state of host 127.0.0.1 (up|down|unknown|skipped) 
print ''
print nm['127.0.0.1'].all_protocols()
print ''
print nm['127.0.0.1']['tcp'].keys()
print ''
print nm['127.0.0.1'].all_tcp() # get all ports for tcp protocol (sorted version)
print ''
print nm['127.0.0.1'].all_udp() # get all ports for udp protocol (sorted version)
print ''
print nm['127.0.0.1'].all_ip() # get all ports for ip protocol (sorted version)
print ''
print nm['127.0.0.1'].all_sctp() # get all ports for sctp protocol (sorted version)
print ''
print nm['127.0.0.1'].has_tcp(22) # is there any information for port 22/tcp on host 127.0.0.1
print ''
print nm['127.0.0.1']['tcp'][22] # get infos about port 22 in tcp on host 127.0.0.1
print ''
print nm['127.0.0.1'].tcp(22) # get infos about port 22 in tcp on host 127.0.0.1
print ''
print  nm['127.0.0.1']['tcp'][22]['state'] # get state of port 22/tcp on host 127.0.0.1 (open
print ''

