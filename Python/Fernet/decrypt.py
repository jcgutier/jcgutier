#!/usr/bin/env python

import os
from cryptography.fernet import Fernet

files = []
current_file_name = os.path.basename(__file__)
key_file = "thekey.key"

ignore_files = [current_file_name, key_file, "example.py"]

for lfile in os.listdir():
    if lfile in ignore_files:
        continue
    if os.path.isfile(lfile):
        files.append(lfile)

print(files)

with open(key_file, "rb") as thekey:
    secretkey = thekey.read()

for efile in files:
    with open(efile, "rb") as thefile:
        contents = thefile.read()
    contents_descrypted = Fernet(secretkey).decrypt(contents)
    with open(efile, "wb") as thefile:
        thefile.write(contents_descrypted)

