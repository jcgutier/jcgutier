#!/usr/bin/env python

import requests

def getpublicip():
    r = requests.get('http://httpbin.org/ip')
    ip = r.json()
    return ip['origin']

if __name__ == "__main__":
    print(getpublicip())
