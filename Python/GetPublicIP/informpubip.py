#!/usr/bin/env python

import os
import smtplib
import logging
from getpubip import getpublicip
from quoteoftheday import getquote

#Global config
logging.basicConfig(filename='pubip.log', level=logging.INFO, format='%(asctime)s - %(levelname)-12s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger(__name__)

def sendmemail(publicip, quotetxt, authorquote):
    mail_server = 'smtp.gmail.com'
    mail_server_port = 587
    
    from_addr = 'infolinuxforme@gmail.com'
    to_addr = 'jcgutierrezo28@gmail.com'
    
    from_header = 'From: %s\r\n' % from_addr
    #to_header = 'To: %s\r\n\r\n' % to_addr
    #subject_header = 'Subject: nothing interesting'
    
    #email_message = '%s\n%s\n%s\n\n%s' % (from_header, to_header, subject_header, body)
    #email_message = '%s\n%s' % (from_header, body)
    SUBJECT='Cambio de IP'
    body = """
    <p>Hola</p>
    La IP del sevidor es:  {}
    <br><br><br>
    
    <span style="margin-right:5px;color:#000000;font-size:15px;font-family:helvetica, arial">MX Server</span><br>
    <span style="text-align:left;color:#aaaaaa;font-size:13px;font-family:helvetica, arial">{} {}</span>
    """.format(publicip, quotetxt, authorquote)
    email_message = 'MIME-Version: 1.0\nContent-type: text/html\nSubject: {}\n\n{}'.format(SUBJECT, body)
    
    s = smtplib.SMTP(mail_server, mail_server_port)
    s.set_debuglevel(1)
    s.starttls()
    s.login("infolinuxforme", "OJSityagyoyljI7")
    s.sendmail(from_addr, to_addr, email_message)
    s.quit()

if __name__ == "__main__":
    log.info('Inicia la validacion de la IP ...')
    publicip = getpublicip()
    #print(publicip)
    log.info('IP actual es: {}'.format(publicip))
    quotetxt, authorquote = getquote()
    log.debug(quotetxt, authorquote)
    puipfile = 'fileofpuip.txt'
    if os.path.isfile(puipfile):
        pipf = open(puipfile, 'r')
        puip = pipf.read()
        log.info('IP en el archivo: {}'.format(puip))
        if puip == publicip:
            log.info('Las IP son iguales')
        else:
            puipf = open(puipfile, 'w')
            puipf.write(publicip)
            sendmemail(publicip, quotetxt, authorquote)
            log.info('Mail del cambio de IP mandado')
    else:
        puipf = open(puipfile, 'w')
        puipf.write(publicip)
        sendmemail(publicip, quotetxt, authorquote)
        log.info('Archivo inexistente, se ha escrito la IP actual y se mando el mail con la IP')
    log.info('... termina la validacion')
