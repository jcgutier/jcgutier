#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup

def getquote():
    result = requests.get('https://www.brainyquote.com/quote_of_the_day')
    soup = BeautifulSoup(result.text, 'html.parser')
    qotd = soup.find("a", class_ = "b-qt").getText()
    aoqotd = soup.find("a", class_ = "bq-aut").getText()
    return qotd, aoqotd

if __name__ == "__main__":
    print(getquote())
