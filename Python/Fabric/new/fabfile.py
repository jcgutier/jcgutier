from fabric.api import *

env.hosts=["eadpgos0063.bio-iad.ea.com","eadpgos0065.bio-iad.ea.com","eadpgos0066.bio-iad.ea.com","eadpgos0067.bio-iad.ea.com","eadpgos0123.bio-iad.ea.com","eadpgos0124.bio-iad.ea.com","eadpgos0125.bio-iad.ea.com","eadpgos0126.bio-iad.ea.com","eadpgos0127.bio-iad.ea.com","eadpgos0128.bio-iad.ea.com"]
def cronch():
    output['warnings'] = False
    output['running'] = False
    env.warn_only = True
    usrs = []
    games = []
    otusr = []
    a = sudo("ls -l /var/spool/cron | grep -v total | wc -l")
    a=int(a)+1
    print a
    for x in range(a):
        if x > 0:
            com = "ls -l /var/spool/cron | grep -v total | awk 'NR=="+str(x)+"{print $9}'"
            user = sudo(com)
            print user
            com = "ps aux | grep /opt/home/"+user+" | grep -v grep | wc -l"
            pr = run(com)
            if int(pr) > 0:
                com="ls -l /opt/home/"+user+" | grep -v total | wc -l"
                itr = run(com)
                itr = int(itr)+1
                for w in range(itr):
                    if w > 0:
                        com = "ls -l /opt/home/"+user+" | grep -v total | awk 'NR=="+str(w)+"{print $9}'"
                        ot = run(com)
                        com = "ps aux | grep "+ot+" | grep -v grep | wc -l"
                        nu = run(com)
                        if int(nu) == 0:
                            games.append(ot)
                otusr.append(user)
            else:
                usrs.append(user)
    print "Users that should not have crontab are:", usrs
    for u in usrs:
        print "---------------------------------------------------------------------------------------------"
        print "---------------------------------------------------------------------------------------------"
        print "---------------------------------------------------------------------------------------------"
        print u+" Crontab:"
        sudo("crontab -l -u "+u+" | grep logrotate")
    print "borrar:"
    for o in otusr:
        for g in games:
            sudo("crontab -l -u "+o+" | grep "+g)
    print "Presiona cualquier tecla ........"
    testVar = raw_input("Presss to continue .....")
