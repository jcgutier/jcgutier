#!/usr/bin/env python

import requests
import re

payload = {'ad1': '2', 'Af': 'dream', 'ds': '123', 'ed': '02/12/2018', 'ht': '1845', 'ln': 'esp', 'ob': 'MEX', 'rm': '1', 'sd': '27/11/2018'}
#payload = {'ad1': '2', 'Af': 'dream', 'ds': '123', 'ed': '10/06/2018', 'ht': '1845', 'ln': 'esp', 'ob': 'MEX', 'rm': '1', 'sd': '05/06/2018'}
r = requests.post('http://reservaciones.dreamsresorts.com.mx/Hoteles/Tarifas', data = payload, allow_redirects=False)
aux = 0
busqueda = re.compile(r'>(.*?)<')
for line in r.text.splitlines():
    if 'kdo--rates--room-type__title' in line and 'Deluxe Vista Tropical' in line:
        #print(line.strip())
        aux = 1
        print(busqueda.findall(line)[1])
    elif aux == 1 and 'notAvaliable' in line:
        aux = 0
        print('No Disponible\n')
    elif 'p class="kdo-product--main-price"' in line and 'MXN' in line and aux == 1:
        #print(line.strip())
        aux = 0
        print(busqueda.findall(line)[2])
        precio = busqueda.findall(line)[2]
list_prec = precio.split(',')
precio = ''.join(list_prec)
with open('DeluxVistaTropialPrecio.out', 'r') as file:
    precio_pasado = file.read()
if int(precio_pasado) > int(precio):
    print('El precio actual es menor')
    with open('DeluxVistaTropialPrecio.out', 'w') as file:
        file.write('{0}\n'.format(precio))
else:
    print('El precio actual es mayor o igual')
