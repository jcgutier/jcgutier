import shelve
from tabulate import tabulate

total = 0
gastos_fijos = shelve.open('gasto.s')
print '================================================='
print 'Gastos fijos'
print '================================================='
opcion = raw_input('Quieres agregar, modificar, borrar o ver los actuales? (a/m/b/v) -> ')
if opcion == 'a':
    cuantos = input('Cuantos gastos fijos quieres agregar? -> ')
    for i in range(cuantos):
        key = raw_input('Concepto -> ')
        value = input('Valor -> ')
        gastos_fijos[key] = value
    opcion = 'v'
elif opcion == 'm':
    concepto = raw_input('Concepto a modificar -> ')
    valor = input('Nuevo valor del concepto -> ')
    gastos_fijos[concepto] = valor
    opcion = 'v'
elif opcion =='b':
    concepto = raw_input('Que concepto quieres borrar -> ')
    del gastos_fijos[concepto]
    opcion = 'v'
if opcion == 'v':
    print ''
    data = sorted([(k,v) for k,v in gastos_fijos.items()])
    print tabulate(data, headers=['Concepto','Valor'], tablefmt="rst")
    for k,v in gastos_fijos.items():
        total = total + v
    print tabulate([['Total\t       =',total]], tablefmt='plain')
    print ''
else:
    print 'ERROR: opcion incorrecta'
print '================================================='
gastos_fijos.close()
