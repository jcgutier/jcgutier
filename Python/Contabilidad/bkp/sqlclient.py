import sqlite3

conn = sqlite3.connect('conta.db')
c = conn.cursor()
while True:
    commando = raw_input('-> ')
    if commando == 's':
        break
    elif 'select' in commando.lower():
        for row in c.execute(commando):
            print row
    elif commando == 'commit':
        conn.commit()
    else:
        c.execute(commando)
conn.close()
