import shelve
import datetime
import sqlite3
from tabulate import tabulate

total_gastos_fijos = 0
gastos_fijos = shelve.open('gasto.s')
for k,v in gastos_fijos.items():
    total_gastos_fijos = total_gastos_fijos + v
#print gastos_fijos

#Ahorro
ahorrojc = 2500
print '================================================='
print 'Contabilidad'
print '================================================='
pagojc = input('Introduce tu ingreso quincenal -> ')
pagoeva = input('Introduce el ingreso quincenal de Eva -> ')
tcjc_extra = input('Introduce los gastos extra de la TC -> ')
print 'El total de gastos fijos es:', total_gastos_fijos
print '================================================='
libre = pagojc - ahorrojc - total_gastos_fijos - tcjc_extra/2
print 'Libres:', libre
retiro_pagos_fijos = gastos_fijos['agua'] + gastos_fijos['gas'] + gastos_fijos['mantenimiento'] + gastos_fijos['gasolina'] + gastos_fijos['comidas']
print 'Retiro pagos fijos:', retiro_pagos_fijos
credito_jc = gastos_fijos['credito jc'] + tcjc_extra/2
print 'Pago TC JC:', credito_jc
print '================================================='
retiro_total = retiro_pagos_fijos+libre
print 'Retiro total:', retiro_total
restante_en_cuenta = (pagojc-retiro_pagos_fijos-libre-ahorrojc)
print 'Restante en cuenta:', restante_en_cuenta
print '================================================='
fecha_hoy = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
conn = sqlite3.connect('conta.db')
c = conn.cursor()
try:
    c.execute('''CREATE TABLE librito
             (fecha text PRIMARY KEY,
              quincena integrer NOT NULL,
              tarjetacreditojc integrer NOT NULL,
              libres integrer NOT NULL,
              retirototal integrer NOT NULL)''')
    c.execute('''CREATE TABLE ahorros
             (fecha text PRIMARY KEY,
             ahorro integrer NOT NULL)''')
    c.execute('''CREATE TABLE restantecuenta
             (fecha text PRIMARY KEY,
             restante integrer NOT NULL)''')
except sqlite3.OperationalError:
    pass
c.execute("INSERT INTO librito VALUES ('"+fecha_hoy+"',"+str(pagojc)+","+str(credito_jc)+","+str(libre)+","+str(retiro_total)+")")
#c.execute("SELECT name FROM sqlite_master WHERE type='table'")
#print c.fetchone()
c.execute("INSERT INTO restantecuenta VALUES ('"+fecha_hoy+"',"+str(restante_en_cuenta)+")")
print ''
imprime=[]
for row in c.execute('SELECT * FROM librito'):
    imprime.append(row)
print tabulate(imprime, headers=['Fecha','Quincena','TC JC','Libres','Retiro Total'], tablefmt="rst")
print ''
imprime=[]
for row in c.execute('SELECT * FROM ahorros'):
    imprime.append(row)
if len(imprime) == 0:
    ahorro = input('Introduce el ahorro total -> ')
else:
    print imprime
    ahorro = imprime[-1][1]
    print ahorro
ahorro_total = ahorro + pagoeva + ahorrojc
c.execute("INSERT INTO ahorros VALUES ('"+fecha_hoy+"',"+str(ahorro_total)+")")
for row in c.execute('SELECT * FROM ahorros'):
    imprime.append(row)
print tabulate(imprime, headers=['Fecha','Ahorro'], tablefmt="rst")
print ''
imprime=[]
for row in c.execute('SELECT * FROM restantecuenta'):
    imprime.append(row)
print tabulate(imprime, headers=['Fecha','Restante'], tablefmt="rst")
print ''
conn.commit()
conn.close()
