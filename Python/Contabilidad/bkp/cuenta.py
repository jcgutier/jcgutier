import shelve
from tabulate import tabulate
import sqlite3
import datetime

def marcopago(data, pago):
    a = 0
    for i in data:
        if pago in i:
            data[a][2]='X'
        a+=1
    print ''
    print tabulate(data, headers=['Concepto','Valor','Pagado'], tablefmt="rst")
    print ''

data = []
gastos_fijos = shelve.open('gasto.s')
registro_cuentas = shelve.open('cuenta.s')
for k,v in sorted(registro_cuentas.items()):
    lista_aux = []
    lista_aux.append(k)
    for a in v:
        lista_aux.append(a)
    data.append(lista_aux)
conn = sqlite3.connect('conta.db')
c = conn.cursor()
imprime=[]
for row in c.execute('SELECT * FROM restantecuenta'):
    imprime.append(row)
print tabulate(imprime, headers=['Fecha','Restante'], tablefmt="rst")
restante = imprime[-1][-1]
print tabulate(data, headers=['Concepto','Valor','Pagado'], tablefmt="rst")
print ''
while True:
    pago = raw_input('Concepto pagado (s para salir) -> ')
    if pago == 's':
        break
    elif pago not in gastos_fijos:
        print 'Invalido'
    else:
        marcopago(data, pago)
        temp_list = registro_cuentas[pago]
        temp_list[1] = 'X'
        registro_cuentas[pago] = temp_list
        restante = restante - gastos_fijos[pago]
        print 'Restante en cuenta = ', restante
        print ''
fecha_hoy = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
c.execute("INSERT INTO restantecuenta VALUES ('"+fecha_hoy+"',"+str(restante)+")")
conn.commit()
conn.close()
if restante == 0:
    data=[]
#data = sorted([(k,v) for k,v in gastos_fijos.items()])
    for k,v in sorted(gastos_fijos.items()):
        if 'agua' in k:
            obj_append = [k, v, 'X']
        elif 'gas' in k:
            obj_append = [k, v, 'X']
        elif 'mantenimiento' in k:
            obj_append = [k, v, 'X']
        elif 'gasolina' in k:
            obj_append = [k, v, 'X']
        elif 'comida' in k:
            obj_append = [k, v, 'X']
        else:
            obj_append = [k, v, '-']
        data.append(obj_append)
    for i in data:
        registro_cuentas[i[0]] = [i[1],i[2]]
registro_cuentas.close()
