function hourToMinutes (hours) {
    let result = hours * 60
    return result
}

let a = hourToMinutes(10)
console.log(a)

// Variable of function type. and anonymous function.
let dayToHours = function(days) {return days * 24}
let c = dayToHours(1)
console.log(c)