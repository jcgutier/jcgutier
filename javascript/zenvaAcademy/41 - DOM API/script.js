// 1. Select
let sign = document.getElementById("sign")
console.log(sign)

// 2. Modify
console.log(sign.textContent)
sign.textContent = "Welcome, travelers!"

// Modify HTML content
// sign.innerHTML = "<p>Hello<p>"
 sign.innerHTML = sign.innerHTML + "<p>Hello</p>"

// Change style
sign.style.color = "blue"
