player = {
    health: 100,
    fun: 0,
    eatApple: function() {
        console.log('eat apple')
        this.health += 10
    },
    eatCandy: function() {
        this.health -= 5
        this.fun += 5
    },
    play: function() {
        this.fun += 10
    }
}

player.eatApple()
console.log(player)

player.eatCandy()
console.log(player)

player.play()
console.log(player)
