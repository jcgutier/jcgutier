let balance = 100
let itemPrice = 1000

console.log("Your current balance is:", balance)

if(balance >= itemPrice) {
    // balance = balance - itemPrice
    balance -= itemPrice
    console.log("Item acquired, your remaining balance is:", balance)
} else {
    console.log("Your balance is not enough to buy the item")
}
