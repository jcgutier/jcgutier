#!/usr/bin/env bash

# set -x

skipdryrun='false'
data_operation='get'

while getopts "asv :m:" flag; do
  case "${flag}" in
    a) skipdryrun='true' ;;
    s) data_operation='send' ;;
    m) RHOSTN=$OPTARG ;;
    *) echo "Unexpected option ${flag}" ;;
  esac
done


#------------------------------#
# Start logging to output file #
#------------------------------#
LOGFILE="/tmp/data${data_operation}_$(date +%Y%m%d_%H%M%S).out"
echo "Starting data ${data_operation} on date: $(date)" > "$LOGFILE"
echo "" >> "$LOGFILE"
if [ $skipdryrun == "false" ]; then
    printf "Log file: $LOGFILE\n" | tee -a $LOGFILE
fi
exec 18>>"$LOGFILE"       # Open 18 file descriptor pointing to log file.
BASH_XTRACEFD=18
script_dir=$(dirname "$0")
. $script_dir/../colored_messages.sh
#------------------------------#


#------------------------#
# Getting data server ip #
#------------------------#
if prc /usr/bin/ping "${RHOSTN}" -c 1; then
    homeserver="${RHOSTN}"
else
    log_message "Host ${RHOSTN} is not available, searching for default LOCAL_SERVER\n"
    if [ -z "$LOCAL_SERVER" ]; then
        read -p "Local server is not defined, enter server name/IP [default=homeserver] ? ->  " ipservidor
    else
        ipservidor="$LOCAL_SERVER"
    fi
    if [ -n "$ipservidor" ]; then
        homeserver=$ipservidor
        RHOSTN="$ipservidor"
    else
        homeserver="homeserver"
        RHOSTN="homeserver"
    fi
    if ! prc ping "$homeserver" -c 1; then
        log_message "\n${homeserver} is not available\n"
        exit 2
    fi
fi
log_message_ret "Data ${data_operation} from machine: $homeserver"
if [ $skipdryrun == "true" ]; then
    var1="y"
fi
#------------------------#


############################################
## Function to get data for any directory ##
############################################
function data_action {
    EXCLUDES=()
    for i in "${exclude[@]}"
    do
        EXCLUDES+=("--exclude '$i'")
    done
    if [ $skipdryrun == "false" ]; then
        log_message "\nDry run on: $1"
        echo ""
        if [ ${data_operation} == "get" ]; then
            prco rsync -ahvn --progress --del "${EXCLUDES[@]}" "$homeserver:~/$1/" "$HOME/$1"/ 2>&1 | tee -a "$LOGFILE"
        else
            prco rsync -ahvn --progress --del "${EXCLUDES[@]}" "$HOME/$1/" "$homeserver:~/$1/" 2>&1 | tee -a "$LOGFILE"
        fi
        read -p "Continue (y/n) ? " var1
        log_message "Continue: $var1" >> "$LOGFILE"
    fi
    if [ "$var1" == "y" ]; then
        log_message_ret "Data ${data_operation} on $1 ..."
        if [ ${data_operation} == "get" ]; then
            prc rsync -ahv --progress --del "${EXCLUDES[@]}" "$homeserver:~/$1/" "$HOME/$1/"
        else
            prc rsync -ahv --progress --del "${EXCLUDES[@]}" "$HOME/$1/" "$homeserver:~/$1/"
        fi
        colored_message $? "Data ${data_operation} on $1"
    fi
}
############################################

#------------------------------#
# Looking override config file #
#------------------------------#
backup_config_file="${script_dir}/backup_data.json"
override_config_file="${HOME}/.config/backup_data_config.yaml"
if prc readlink -e "$override_config_file"; then
    if prc yq -e e ".\"${RHOSTN%.local}\"" - < "${override_config_file}"; then
        prc export backup_config_file="${override_config_file}"
    else
        prc export RHOSTN="default"
    fi
fi
#------------------------------#

#----------------------------------------------------#
# Reading folders and excludes from backup_data.json #
#----------------------------------------------------#
for folder in $(yq e ".\"${RHOSTN%.local}\"[].Folder" - < "${backup_config_file}"); do
    index=0
    exclude=()
    excludes=$(cat "${backup_config_file}" | yq e ".\"${RHOSTN%.local}\"[] | select(.Folder==\"${folder}\") | .Excludes[]" -)
    for excl in ${excludes}; do
        exclude[${index}]=${excl}
        index=$((index + 1))
    done
    data_action "${folder}"
done
#----------------------------------------------------#
