#!/usr/bin/env bash

RED=`tput setaf 1`
LIGTH_GREEN=`tput setaf 2`
GREEN=`tput setaf 10`
BLUE=`tput setaf 4`
YELLOW=`tput setaf 11`
RESET=`tput sgr0`
CHECK_MARK="[\xE2\x9C\x94]"

resultado_mensaje(){
    if [ $1 -eq 0 ]; then
        echo -e "${GREEN}$CHECK_MARK $2 ${RESET}"
    elif [ $1 -eq 4 ]; then
        echo -e "${BLUE}$CHECK_MARK $2 ${RESET}"
    elif [ $1 -eq 3 ]; then
        echo -e "${YELLOW}[!] $2 ${RESET}"
    elif [ $1 -eq 22 ]; then
        echo -e "${LIGTH_GREEN}$CHECK_MARK $2 ${RESET}"
    else
        echo -e "${RED}[x] $2 ${RESET}"
    fi
}