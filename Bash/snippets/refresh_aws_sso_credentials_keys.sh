#!/usr/bin/env bash

set -e

# Termina ejecuciones pasads de este script excepto la misma ejecucion en curso
ps aux | grep ${BASH_SOURCE[0]} | grep -v $$ | grep -v grep | awk '{print $2}' | xargs kill -9 &> /dev/null || echo "No previous run found"

echo "Refreshing credentials every 50 mins fro the profile $1"

timeout=3000
while true
do
    export AWS_PROFILE=$1
    aws sts get-caller-identity > /dev/null
    ROLE_ARN=$(grep -A 1 $AWS_PROFILE ~/.aws/config | grep role_arn | awk -F = ''{print $2})
    RESP=$(aws sts assume-role --role-arn $ROLE_ARN --role-session-name $1)
    ACCESSKEYID=$(echo $RESP | jq .Credentials.AccessKeyId | awk -f \" '{print $2}')
    SECRETKEY=$(echo $RESP | jq .Credentials.SecretAccessKey | awk -f \" '{print $2}')
    SESSIONTOKEN=$(echo $RESP | jq .Credentials.SessionToken | awk -f \" '{print $2}')
    EXPIRATION=$(echo $RESP | jq .Credentials.Expiration | awk -f \" '{print $2}')
    cat << EOF > ~/.aws/credentials
[$1]
aws_access_key_id=$ACCESSKEYID
aws_secret_access_key=$SECRETKEY
aws_session_token=$SESSIONTOKEN
# Expiration $EXPIRATION
EOF
    sleep $timeout
done
