#!/usr/bin/env bash

#  Random password generator for Bash 2.x +
#+ by Antek Sawicki <tenox@tenox.tc>,
#+ who generously gave usage permission to the ABS Guide author.
#
# ==> Comments added by document author ==>
#
# Snippet updates:
# Taken from: https://tldp.org/LDP/abs/html/contributed-scripts.html#PW
# Modified to accept parameters and generate n-lenght of password and
# include or not special characters.

MATRIX="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
SPECIAL_MATRIX=$MATRIX
SPECIAL_MATRIX+='!#$%&=+*-_ '

while getopts 'sl:c:' flag; do
  case "${flag}" in
    l) LENGHT=${OPTARG} ;;
    s) INCLUDE_SPECIAL_CHARS=true ;;
	c) SPECIAL_MATRIX="$MATRIX$OPTARG" ;;
    *) echo "Unexpected option ${flag}" ;;
  esac
done

# ==> Password will consist of alphanumeric characters.
LENGTH=${LENGHT:=8}
# ==> May change 'LENGTH' for longer password.

while [ "${n:=1}" -le "$LENGTH" ]
# ==> Recall that := is "default substitution" operator.
# ==> So, if 'n' has not been initialized, set it to 1.
do
    if [ -z "$INCLUDE_SPECIAL_CHARS" ]; then
	    PASS="$PASS${MATRIX:$(($RANDOM%${#MATRIX})):1}"
    else
        PASS="$PASS${SPECIAL_MATRIX:$(($RANDOM%${#SPECIAL_MATRIX})):1}"
    fi
	# ==> Very clever, but tricky.

	# ==> Starting from the innermost nesting...
	# ==> ${#MATRIX} returns length of array MATRIX.

	# ==> $RANDOM%${#MATRIX} returns random number between 1
	# ==> and [length of MATRIX] - 1.

	# ==> ${MATRIX:$(($RANDOM%${#MATRIX})):1}
	# ==> returns expansion of MATRIX at random position, by length 1. 
	# ==> See {var:pos:len} parameter substitution in Chapter 9.
	# ==> and the associated examples.

	# ==> PASS=... simply pastes this result onto previous PASS (concatenation).

	# ==> To visualize this more clearly, uncomment the following line
	# echo "$PASS"
	# ==> to see PASS being built up,
	# ==> one character at a time, each iteration of the loop.

	(( n+=1 ))
	# ==> Increment 'n' for next pass.
done

echo "$PASS"      # ==> Or, redirect to a file, as desired.

exit 0