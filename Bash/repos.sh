#!/usr/bin/env bash

file_name=$(basename $0)
file_name=${file_name%%.*}

LOGFILE="/tmp/${file_name}_$(date +%Y%m%d_%H%M%S).out"

echo "Starting ${file_name} process on date: $(date)" > $LOGFILE
echo "" >> $LOGFILE

script_dir=$(dirname $0)
. $script_dir/colored_messages.sh

get_git_repos(){
    if [ "$1" == "GITHUB" ]; then
        GIT_REPOS="$(curl -s https://api.github.com/users/jcgutier/repos | jq '.[].ssh_url')"
    elif [ "$1" == "GITLAB" ]; then
        GIT_REPOS=$(curl -s https://gitlab.com/api/v4/users/jcgutier/projects  | jq '.[].ssh_url_to_repo')
    fi
    GIT_FOLDER=${!1}
    for REPO_URL in $GIT_REPOS; do
        if [ ! -d $GIT_FOLDER ]; then
            mkdir -p $GIT_FOLDER
        fi
        cd $GIT_FOLDER
        REPO_NAME="$(echo $REPO_URL | awk -F\/ '{print $2}' | cut -d '.' -f1)"
        if [ -d $REPO_NAME ]; then
            echo -ne "Actualizando $REPO_NAME ... \033[0K\r"
            cd $REPO_NAME
            git pull origin master &> /dev/null || git pull origin main &> /dev/null
            colored_message $? "Actualizacion de $REPO_NAME                            "
        else
            echo -ne "Clonando $REPO_NAME ... \033[0K\r"
            NEW_REPO_URL=$(echo $REPO_URL | awk -F\" '{print $2}')
            git clone $NEW_REPO_URL --quiet
            colored_message 22 "Actualizacion de $REPO_NAME                            "
        fi
    done
}

git_repo_status(){
    GIT_FOLDER=${!1}
    cd $GIT_FOLDER
    for REPO in $(ls -d */); do
        cd $GIT_FOLDER/$REPO
        ls .git &> /dev/null
        if [ $? -eq 0 ]; then
            GIT_STATUS=$(git status 2> /dev/null)
            #GIT_STATUS=$(git status)
            if [[ "$GIT_STATUS" == *"Your branch is behind"* ]]; then
                colored_message 3 "${REPO::-1} need to be updated on your local"
            elif [[ "$GIT_STATUS" == *"Changes "* ]] || [[ "$GIT_STATUS" == *"Untracked "* ]] || [[ "$GIT_STATUS" == *"is ahead"* ]]; then
                colored_message 3 "${REPO::-1} has pending changes"
            else
                colored_message 0 "${REPO::-1} is up to date"
            fi
        fi
    done
}

main(){
    if [ "$GETREPOS" == "true" ]; then
        echo "Repos de github.com:"
        get_git_repos GITHUB
        echo "Repos de gitlab:"
        get_git_repos GITLAB
    fi
    if [ "$STATUSREPOS" == "true" ]; then
        GIT_DIRS=$(for user_dir in $(ls -d ~/*/ | grep -vE "AppData|go" ); do find $user_dir -type d -name "*.git" ; done)
        declare -a LOCAL_GIT_DIRS
        for REPOSITORY in $GIT_DIRS
        do
            cd "$REPOSITORY/../../"
            LOCAL_GIT_DIRS+=($PWD)
        done
        UNIQ_LOCAL_REPOS="$(printf "%s\n" "${LOCAL_GIT_DIRS[@]}" | sort -u)"
        for LOCAL_REPO in $UNIQ_LOCAL_REPOS
        do
            echo ""
            REPO_DIR=$(echo $LOCAL_REPO | awk -F "/" '{print $(NF-1)"/"$NF}')
            echo "Status repos de ${REPO_DIR,,}"
            git_repo_status LOCAL_REPO
        done
    fi
}

while getopts 'gs' flag; do
    case "${flag}" in
        g) GETREPOS="true" ;;
        s) STATUSREPOS="true" ;;
        *) echo "Unexpected option ${flag}" ;;
    esac
done
main
