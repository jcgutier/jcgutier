#!/bin/bash

#Assignment
a=123
echo "The value of \"a\" is $a"

#Assignment ussing let
let a=16+5
echo "The value of \"a\" is $a"

#In a for loop (really, a type of disguised assignment)
for a in 7 8 9 10 11
do
  echo -n "$a"
done
echo 

#In a read statement (also a type of assignment
echo -n "Enter \"a\" "
read a
echo "The value of \"a\" is now $a"

#Command subtitution
a=`echo HELLO`
echo $a
a=`ls -l`
echo $a
echo "Quoted preservs white spaces"
echo "$a"
R=$(df -h)
echo $R
echo "$R"
