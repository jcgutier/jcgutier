#!/bin/bash

grep '[Ff]irst' *.txt

#Las comillas pueden hacer que el echo ponga todos los slatos de linea
echo $(ls -l)
echo "$(ls -l)"

#Citando comillas
Lista="one two three"
for a in $Lista
do
  echo "$a"
done
echo "---"
for a in "$Lista"
do
  echo "$a"
done
