#!/usr/bin/env bash

if [ -z $LOGFILE ]; then
    LOGFILE="/tmp/colored_messages_$(date +%Y%m%d_%H%M%S).out"
    echo "[WARNING] No log file specified, defaulting to $LOGFILE"
    exec 18>>"$LOGFILE"       # Open 18 file descriptor pointing to log file.
    BASH_XTRACEFD=18
fi

#------------------#
# Colored messages #
#------------------#
RED=$(tput setaf 1)
GREEN=$(tput setaf 10)
BLUE=$(tput setaf 4)
YELLOW=$(tput setaf 3)
RESET=$(tput sgr0)
CHECK_MARK="[\xE2\x9C\x94]"
colored_message(){
    if [ "$1" -eq 0 ]; then
        printf "\r\033[0K${GREEN}$CHECK_MARK $2 ${RESET}\n" | tee -a "$LOGFILE"
    elif [ "$1" -eq 3 ]; then
        printf "\r\033[0K${YELLOW}[!] $2 ${RESET}\n" | tee -a "$LOGFILE"
    elif [ "$1" -eq 4 ]; then
        printf "\r\033[0K${BLUE}$CHECK_MARK $2 ${RESET}\n" | tee -a "$LOGFILE"
    else
        printf "\r\033[0K${RED}[x] $2. See log file: $LOGFILE${RESET} \n" | tee -a "$LOGFILE"
    fi
}
log_message(){
    echo >> "$LOGFILE"
    printf "$1" | tee -a "$LOGFILE"
    echo >> "$LOGFILE"
}
log_message_ret(){
    printf "\r\033[0K$1" | tee -a "$LOGFILE"
    echo >> "$LOGFILE"
}
#------------------#


#----------------------------------------------------------#
# Defining function to print and run a command on log file #
#----------------------------------------------------------#
function prc
{
    local PS4='-${GREEN}Running cmd: ${RESET}'
    local -
    # set -o xtrace
    set -x
    "$@" &>> $LOGFILE
}
#----------------------------------------------#


#----------------------------------------------------------#
# Defining function to print and run a command on log file #
#----------------------------------------------------------#
function prco
{
    local PS4='-${GREEN}Running cmd: ${RESET}'
    local -
    # set -o xtrace
    unset BASH_XTRACEFD
    set -x
    "$@"
}
#----------------------------------------------#

