#! /bin/bash

if [ -z $MASTODONTOKEN ]
then
  echo "Configura el token como la variable de ambiente MASTODONTOKEN"
  exit 1
fi

if [ -z $1 ]
then
  read -p "Mensaje: " mensaje
else
  mensaje="$1"
fi

curl -X POST -Ss https://mastodon.social/api/v1/statuses \
  --header "Authorization: Bearer ${MASTODONTOKEN}" \
  -d "status=${mensaje}" > /dev/null

if [ $? -eq 0 ]
then
  echo "Mensaje enviado"
fi
