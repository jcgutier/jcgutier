#include <stdio.h>
#include <stdlib.h>

void calculo_voltaje(void);
void calculo_intensidad(void);
void calculo_resistencia(void);

/*Ejecuta la funcion que recibe como parametro*/
void calculo_ley_ohm(void (*funcion)(void)); 

void (*ley_ohm[3])(void) =
{
     calculo_voltaje,
     calculo_intensidad,
     calculo_resistencia
};

int main(int argc, char *argv[])
{
    int eleccion;
    
    printf("Eliga una opcion del siguiente menu: \n\n");
    printf("1) Voltaje \n2) Intensidad \n3) Resistencia \n\n");
    printf("opcion >>> ");
    scanf("%d",&eleccion);
    switch(eleccion)
    {
                    case 1: calculo_ley_ohm(*ley_ohm[0]);
                         break;
                    case 2: calculo_ley_ohm(*ley_ohm[1]);
                         break;
                    case 3: calculo_ley_ohm(*ley_ohm[2]);
                         break;
    }
    printf("\n\n\n");
  
  system("PAUSE");	
  return 0;
}

void calculo_voltaje(void)
{
     float voltaje;
     float intensidad;
     float resistencia;    
     
     printf("Introduzca la intensidad en amperios => ");
     scanf("%f",&intensidad);
     printf("Introduzca la resistencia en ohms => ");
     scanf("%f",&resistencia);
     voltaje=intensidad*resistencia;
     printf("\n\n\nEl Voltaje es: %0.2f V",voltaje);
}

void calculo_intensidad(void)
{
     float voltaje;
     float intensidad;
     float resistencia;    
     
     printf("Introduzca el voltaje es volts => ");
     scanf("%f",&voltaje);
     printf("Introduzca la resistencia en ohms => ");
     scanf("%f",&resistencia);
     intensidad=voltaje/resistencia;
     if(intensidad<1)
     {
                     intensidad*=1000;
                     printf("\n\n\nLa intensidad es: %0.2f mA",intensidad);
     }
     else
                     printf("\n\n\nLa intensidad es: %0.2f A",intensidad);
}

void calculo_resistencia(void)
{
     float voltaje;
     float intensidad;
     float resistencia;    
     
     printf("Introduzca el voltaje es volts => ");
     scanf("%f",&voltaje);
     printf("Introduzca la intensidad en amperios => ");
     scanf("%f",&intensidad);
     resistencia=voltaje/intensidad;
     if(resistencia>999)
     {
                        resistencia/=1000;
                        printf("\n\n\nLa resistencia es: %0.2f KOhms",resistencia);
     }
     else
                        printf("\n\n\nLa resistencia es: %0.2f Ohms",resistencia);
}

void calculo_ley_ohm(void (*funcion)(void))
{
     funcion();
}
