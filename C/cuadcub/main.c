#include <stdio.h>
#include <stdlib.h>
#include "pot.h"

int main(int argc, char *argv[])
{
  int n,t,t1;
  printf("Este programa calcula el cuadrado y cubo de un numero dado");
  printf("\n\n  Introduzca el numero para calcular => ");
  scanf("%d",&n);
  t=n*n;
  t1=n*n*n;
  printf("\n\tEl numero al cuadrado es %d \n\n\tEl numero al cubo es %d\n\n",t,t1);
  system("PAUSE");	
  return 0;
}
