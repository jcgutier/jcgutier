#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char seleccion[2];
    char nombre_archivo[13];
    char eleccion[2];
    int valor_seleccion;
    char caracter;
    FILE *puntero_a_archivo;
    printf("\n\n\n");
    printf("Selecciona una de las siguientes opciones:\n");
    printf("1) Crear un archivo y escribir datos sobre el (sobrescribe datos en un archivo existente).\n");
    printf("2) A�adir nuevos datos a un archivo existente.\n");
    printf("3) Leer datos desde un archivo existente.\n");
    
    do
    {
               printf("Su seleccion es: ");
               gets(eleccion);
               valor_seleccion=atoi(eleccion);
               switch(valor_seleccion)
               {
                                      case 1: strcpy(seleccion,"w");
                                              break;
                                      case 2: strcpy(seleccion,"a");
                                              break;
                                      case 3: strcpy(seleccion,"r");
                                              break;
                                      default: printf("Esta no es una seleccion.\n");
                                               valor_seleccion=0;
               }
    }while(valor_seleccion==0);
    printf("introduzca el nombre del archivo => ");
    gets(nombre_archivo);
    puntero_a_archivo=fopen(nombre_archivo,seleccion);
    if(puntero_a_archivo==NULL)
    {
                                                                 printf("\nNo se pudo abrir el archivo %s\n\n",nombre_archivo);
                                                                 system("PAUSE");
                                                                 exit(-1);
    }
    switch(valor_seleccion)
    {
                           case 1: 
                           case 2: printf("Introduzca los caracteres a grabar: \n");
                                   while((caracter=getchar())!='\n')
                                                                     caracter=putc(caracter,puntero_a_archivo);
                                   break;
                           case 3: printf("\nEl contenido del archivo es: \n\n");
                                   while((caracter=getc(puntero_a_archivo))!=EOF)
                                   printf("%c",caracter);
                                   printf("\n\n");
                                   break;
    }
    fclose(puntero_a_archivo);
    
  
  system("PAUSE");	
  return 0;
}
