#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
        char dato;
        struct nodo *hijo_izq;
        struct nodo *hijo_der;
}arbol;

void preorden(arbol *ptr);
void ordenSimetrico(arbol *ptr);
void postorden(arbol *ptr);

int main(int argc, char *argv[])
{
    arbol *n1,*n2,*n3,*n4,*n5;
    
    n1=malloc(sizeof(arbol));
    n2=malloc(sizeof(arbol));
    n3=malloc(sizeof(arbol));
    n4=malloc(sizeof(arbol));
    n5=malloc(sizeof(arbol));
    
    n1->dato='*';
    n1->hijo_izq=n2;
    n1->hijo_der=n3;
    
    n2->dato='+';
    n2->hijo_izq=n4;
    n2->hijo_der=n5;
    
    n3->dato='C';
    n3->hijo_izq=NULL;
    n3->hijo_der=NULL;
    
    n4->dato='A';
    n4->hijo_izq=NULL;
    n4->hijo_der=NULL;
    
    n5->dato='B';
    n5->hijo_izq=NULL;
    n5->hijo_der=NULL;
    
    printf("Recorrido en pre-orden >>> ");
    preorden(n1);
    printf("\nRecorrido en orden simetrico >>> ");
    ordenSimetrico(n1);
    printf("\nRecorrido en post-orden >>> ");
    postorden(n1);
    printf("\n");
  
  system("PAUSE");	
  return 0;
}

void preorden(arbol *ptr)
{
     if(ptr!=NULL)
     {
                  printf("%c",ptr->dato);
                  preorden(ptr->hijo_izq);
                  preorden(ptr->hijo_der);
     }
}

void ordenSimetrico(arbol *ptr)
{
     if(ptr!=NULL)
     {
                  ordenSimetrico(ptr->hijo_izq);
                  printf("%c",ptr->dato);
                  ordenSimetrico(ptr->hijo_der);
     }
}

void postorden(arbol *ptr)
{
     if(ptr!=NULL)
     {
                  postorden(ptr->hijo_izq);
                  postorden(ptr->hijo_der);
                  printf("%c",ptr->dato);
     }
}
