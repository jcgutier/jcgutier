#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
        char dato;
        struct nodo *enlace;
}pila;

void mostrar_pila(pila *ptr);
int vacia(pila *ptr);
void introducir(pila **ptr, char item);
void extraer(pila **ptr, char *item);

int main(int argc, char *argv[])
{
    pila *pila=NULL;
    char item;
    
    introducir(&pila,'a');
    introducir(&pila,'b');
    introducir(&pila,'c');
    printf("La pila es como sigue: ");
    mostrar_pila(pila);
    extraer(&pila,&item);
    printf("El primer elemento obtenido es %c\n",item);
    extraer(&pila,&item);
    printf("El primer elemento obtenido es %c\n",item);
    extraer(&pila,&item);
    printf("El primer elemento obtenido es %c\n",item);
    extraer(&pila,&item);
  
  system("PAUSE");	
  return 0;
}

void mostrar_pila(pila *ptr)
{
     while(ptr!=NULL)
     {
                     printf("%c",ptr->dato);
                     ptr=ptr->enlace;
     }
     printf("\n");
}

int vacia(pila *ptr)
{
    if(ptr==NULL)
                 return 1;
    else
                 return 0;
}

void introducir(pila **ptr, char item)
{
     pila *p;
     if(vacia(*ptr))
     {
                    p=malloc(sizeof(pila));
                    if(p!=NULL)
                    {
                               p->dato=item;
                               p->enlace=NULL;
                               *ptr=p;
                    }
     }
     else
     {
                    p=malloc(sizeof(pila));
                    if(p!=NULL)
                    {
                               p->dato=item;
                               p->enlace=*ptr;
                               *ptr=p;
                    }
     }
}

void extraer(pila **ptr, char *item)
{
     pila *p1;
     p1=*ptr;
     if(vacia(p1))
     {
                  printf("Error la pila esta vacia");
                  *item=NULL;
     }
     else
     {
                  *item=p1->dato;
                  *ptr=p1->enlace;
                  free(p1);
     }
}   
