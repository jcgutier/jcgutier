#include <stdio.h>
#include <stdlib.h>

typedef struct
{
        char fabricante[20];
        int cantidad;
        float precio_unitario;
} registro_piezas;

registro_piezas leerdatos(void);

void imprimir(registro_piezas resistencia);

int main(int argc, char *argv[])
{
    registro_piezas *ptr_reg;
    float valor_total;
    
    /*Asignacion dinamica de memoria al puntero de la estructura*/
    ptr_reg=(registro_piezas *) malloc(sizeof(registro_piezas));
    printf("Nombre del Fabricante => ");
    gets(ptr_reg->fabricante);
    
    printf("Numero de Piezas => ");
    scanf("%d",&ptr_reg->cantidad);
    
    printf("Precio de cada pieza => ");
    scanf("%f",&ptr_reg->precio_unitario);
    
    valor_total=ptr_reg->cantidad * ptr_reg->precio_unitario;
    
    printf("\n\n");
    printf("Articulo:             Resistencias\n\n");
    printf("Fabricante            %s\n\n",ptr_reg->fabricante);
    printf("Precio Unitario       $%f\n\n",ptr_reg->precio_unitario);
    printf("Cantidad              %d\n\n",ptr_reg->cantidad);
    printf("Valor total           $%f\n\n",valor_total);
    
    /*Liberar la memoria asignada*/
    free(ptr_reg);
    /*printf("**************         Con Funciones        **************\n\n");
    
    registro_piezas resistencia;
    
    resistencia=leerdatos();
    
    imprimir(resistencia);*/
    
  
  system("PAUSE");	
  return 0;
}

registro_piezas leerdatos(void)
{
                registro_piezas reg;
                printf("Nombre del Fabricante => ");
                gets(reg.fabricante);
    
                printf("Numero de Piezas => ");
                scanf("%d",&reg.cantidad);
    
                printf("Precio de cada pieza => ");
                scanf("%f",&reg.precio_unitario);
                
                return(reg);
}

void imprimir(registro_piezas resistencia)
{
     printf("Articulo:             Resistencias\n\n");
     printf("Fabricante            %s\n\n",resistencia.fabricante);
     printf("Precio Unitario       $%f\n\n",resistencia.precio_unitario);
     printf("Cantidad              %d\n\n",resistencia.cantidad);
}
