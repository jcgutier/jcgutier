#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    union
    {
         int valor_entero;
         float valor_real;
    }entero_o_real;
    
    printf("La uniodn tiene %d bytes.\n",sizeof(entero_o_real));

    entero_o_real.valor_entero=123;
    printf("El valor entero es. %d\n",entero_o_real.valor_entero);
    printf("La direccion de comienzo es %X\n",&entero_o_real.valor_entero);
    
    entero_o_real.valor_real=123.45;
    printf("El valor entero es. %f\n",entero_o_real.valor_real);
    printf("La direccion de comienzo es %X\n",&entero_o_real.valor_real);
        
  
  system("PAUSE");	
  return 0;
}
