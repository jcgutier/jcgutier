#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
        char dato;
        struct nodo *enlace;
}cola;

void mostrar_cola(cola *ptr);
int vacia(cola *ptr);
void encolar(cola **cabeza,char item);
void desencolar(cola **cabeza, char *item);


int main(int argc, char *argv[])
{
    cola *cabeza=NULL;
    char item;
    
    encolar(&cabeza,'a');
    encolar(&cabeza,'b');
    encolar(&cabeza,'c');
    printf("La cola es como sigue: ");
    mostrar_cola(cabeza);
    desencolar(&cabeza,&item);
    printf("El primer elemento obtenido de la cola es %c\n",item);
    desencolar(&cabeza,&item);
    printf("El segundo elemento obtenido de la cola es %c\n",item);
    desencolar(&cabeza,&item);
    printf("El tercero elemento obtenido de la cola es %c\n",item);
    desencolar(&cabeza,&item);
  
  system("PAUSE");	
  return 0;
}

void mostrar_cola(cola *ptr)
{
     while(ptr!=NULL)
     {
                     printf("%c",ptr->dato);
                     ptr=ptr->enlace;
     }
     printf("\n");
}

int vacia(cola *ptr)
{
    if(ptr==NULL)
                 return 1;
    else
                 return 0;
}

void encolar(cola **cabeza, char item)
{
     cola *p;
     p=malloc(sizeof(cola));
     if(p!=NULL)
     {
                p->dato=item;
                p->enlace=*cabeza;
                *cabeza=p;
     }
}

void desencolar(cola **cabeza, char *item)
{
     cola *p1,*p2;
     p1=*cabeza;
     if(vacia(p1))
     {
                printf("Error la cola esta vacia");
                *item=NULL;
     }
     else
     {
         p2=*cabeza;
         while(p2->enlace!=NULL)
         {
                                p1=p2;
                                p2=p2->enlace;
         }
         *item=p2->dato;
         p1->enlace=NULL;
         free(p2);
         if(p1==p2)
                   *cabeza=NULL;
     }
}  
