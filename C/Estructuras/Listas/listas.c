#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
        char dato;
        struct nodo *enlace;
}lista;

void mostrar_lista(lista *ptr);
void insertar_al_principio(lista **ptr,char elemento);
void insertar_al_final(lista *ptr,char item);
void insertar_enmedio(lista *ptr,char letra,char item);
void elimina_principio(lista **ptr);
void elimina_final(lista **ptr);
int buscar(lista *ptr,char c);

int main(int argc, char *argv[])
{
    char c;
    lista *n1,*n2,*n3;
    
    n1=malloc(sizeof(lista));
    n2=malloc(sizeof(lista));
    n3=malloc(sizeof(lista));
    
    n1->dato='b';
    n1->enlace=n2;
    n2->dato='c';
    n2->enlace=n3;
    n3->dato='d';
    n3->enlace=NULL;
    
    printf("La Lista enlazada es como sigue: ");
    mostrar_lista(n1);
    insertar_al_principio(&n1,'a');
    printf("La nueva lista enlazada es: ");
    mostrar_lista(n1);
    insertar_al_final(n1,'e');
    printf("La nueva lista enlazada es: ");
    mostrar_lista(n1);
    //printf("Introduce despues de que caracter quieres poner la X => ");
    //c=getchar();
    //insertar_enmedio(n1,c,'x');
    //printf("La nueva lista enlazada es: ");
    mostrar_lista(n1);
    elimina_principio(&n1);
    printf("La nueva lista enlazada es: ");
    mostrar_lista(n1);
    elimina_final(&n1);
    printf("La nueva lista enlazada es: ");
    mostrar_lista(n1);
    printf("Introduce el caracter que quieres buscar => ");
    c=getchar();
    if(buscar(n1,c)==1)
                    printf("\nCaracter encontrado\n\n");
    else
                    printf("\nCaracter no encontrado\n\n");
    
  system("PAUSE");	
  return 0;
}

void mostrar_lista(lista *ptr)
{
     /*Mientras que el valor donde apunte *ptr no sea nulo, recorre toda la 
     lista hasta que sea el final(nulo), y ve imprimiendo cada caracter que
     encuentres en transcurso*/
     while(ptr!=NULL)
     {
                     printf("%c",ptr->dato);
                     ptr=ptr->enlace;
     }
     printf("\n");
}

void insertar_al_principio(lista **ptr,char elemento)
{
     /*Se crea un apuntador a una estructura lista*/
     lista *nueva;
     /*Se le asigna memoria del tama�o de lista*/
     nueva=malloc(sizeof(lista));
     /*Si nuevo no apunto a nulo(al final), entonces a la nueva estructura 
     asignale el valor que se desa incluir y apunta su enlace al principio 
     de la lista, despues apunta al principo de la lista justo en el valor 
     que se acaba de incluir*/
     if(nueva!=NULL)
     {
                    nueva->dato=elemento;
                    nueva->enlace=*ptr;
                    *ptr=nueva;
     }
}

void insertar_al_final(lista *ptr,char item)
{
     /*Se crea un apuntador a una estructura lista*/
     lista *nuevo;
     /*Mientras que el apuntador de inicio no sea al finalm recorre toda 
     la lista, hasta llegar a nulo(final)*/    
     while(ptr->enlace!=NULL)
                             ptr=ptr->enlace;
     /*Se le asigna memoria del tama�o de lista al nuevo apuntador*/
     nuevo=malloc(sizeof(lista));
     /*Si el nuevo apuntador no apunta al final de la lista(nulo), entonces 
     el final de la lista apuntalo al nuevo apuntador, asignale el valor 
     deseado y convierte a este elemento en el final de la lista*/
     if(nuevo!=NULL)
     {
                    ptr->enlace=nuevo;
                    nuevo->dato=item;
                    nuevo->enlace=NULL;
     }
}

void insertar_enmedio(lista *ptr,char letra,char item)
{
     /*Se crea un apuntador a una estructura lista*/
     lista *nuevo,*sig;
     /*Recorre la lista hasta que encuentre el caracter al cual le 
     vamos a poner en frente otra letra*/
     while(ptr->dato!=letra)
                            ptr=ptr->enlace;
     /*Apunta a sig al mismo lugar al que apunta ptr*/
     sig=ptr;
     /*Avanza un lugar en la lista*/
     sig=sig->enlace;
     /*Asignale momeoria al nuevo apuntador*/
     nuevo=malloc(sizeof(lista));
     /*Si el nuevo apuntador no esta dirigido al final(nulo), apunta el enlace 
     del caracter deseado al caracter que se quiere insertar, despues asigna 
     los valores para agregar, y enlaza el carcter agregado al caracter siguiente*/
     if(nuevo!=NULL)
     {
                   ptr->enlace=nuevo;
                   nuevo->dato=item;
                   nuevo->enlace=sig;
     }
}

void elimina_principio(lista **ptr)
{
     lista *p;
     //Dirige al apuntador p al mismo lugar donde apunta *ptr
     p=*ptr;
     if(p!=NULL)
     {
                //Avanza un caracter
                p=p->enlace;
                //Borra de memoria el caracter que se acaba de pasar
                free(*ptr);
     }
     //Dirige a *ptr a donde apunta p que es el segundo caracter
     *ptr=p;
}
void elimina_final(lista **ptr)
{
     lista *p1,*p2;
     //Dirige a p1 a donde apunta *ptr, que es al inicio
     p1=*ptr;
     if(p1!=NULL)
     {
                 //Si donde apunta p1 es el final, libera la memoria y apunta al princio a nulo
                 if(p1->enlace==NULL)
                 {
                                     free(*ptr);
                                     *ptr=NULL;
                 }
                 else
                 {
                                     /*De otra manera recorre la lista hasta que llegue al final
                                     y asignale a p2 un valor atras*/
                                     while(p1->enlace!=NULL)
                                     {
                                                            p2=p1;
                                                            p1=p1->enlace;
                                     }
                                     //Convierte a p2 como el final de la lista
                                     p2->enlace=NULL;
                                     //Borra de la memoria el antiguo final
                                     free(p1);
                 }
     }
}

int buscar(lista *p,char c)
{
     if(p==NULL)
                printf("Lista no existe por lo que el caracter no esta");
     else
     {
         do
         {
                if(p->dato==c)
                              return 1;
                p=p->enlace;
         }while(p!=NULL);
         return 0;
     }
}
