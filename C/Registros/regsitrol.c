#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct 
{
        char nombre[15];
        int cantidad;
        float precio_unitario;
}tipo_pieza;

int main(int argc, char *argv[])
{
    int s=0;
    tipo_pieza pieza;
    FILE *puntero;
    char *pPath,*pro;
    pPath = getenv ("OS");
    pro = getenv("PROCESSOR_IDENTIFIER");
    if (pPath!=NULL)
       printf ("El sistema es: %s\nEl prcesador es: %s\n\n",pPath,pro);
    
    puntero=fopen("Piezas.log","rb");
    printf("-------------------------------------------------------------------------\n");
    printf("|Reg\t|Marca\t\t\t|Cantidad\t|Precio Unitario\t|\n");
    printf("-------------------------------------------------------------------------\n");
    while(fread(&pieza,sizeof(pieza),1,puntero)==1)
    {
                                                   printf("|%d\t",s);
                                                   if(strlen(pieza.nombre)<=6)
                                                                  printf("|%s\t\t\t",pieza.nombre);
                                                   else
                                                                  printf("|%s\t\t",pieza.nombre);
                                                   printf("|%d\t\t",pieza.cantidad);
                                                   if(pieza.precio_unitario>999)
                                                                                printf("|%0.2f\t\t|",pieza.precio_unitario);
                                                   else
                                                                                printf("|%0.2f\t\t\t|",pieza.precio_unitario);
                                                   printf("\n");
                                                   s++;
    }
    printf("-------------------------------------------------------------------------\n");
    
    fclose(puntero);
  
  system("PAUSE");	
  return 0;
}
