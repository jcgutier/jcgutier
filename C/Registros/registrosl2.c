#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
        char nombre[15];
        int cantidad;
        float precio_unitario;
}tipo_pieza;

int main(int argc, char *argv[])
{
    tipo_pieza pieza;
    FILE *puntero;
    int num_registro;
    long int desplazamiento;
    
    if((puntero=fopen("Piezas.log","rb"))==NULL)
    {
                                              printf("No puedo Abrir el archivo");
                                              exit(-1);
    }
    
    printf("Introduzca el numero de registro: ");
    scanf("%d",&num_registro);
    desplazamiento=num_registro*sizeof(pieza);
    
    if(fseek(puntero,desplazamiento,0)!=0)
    {
                                          printf("El punteor ha superado el limite del archivo");
                                          exit(-1);
    }
    
    fread(&pieza,sizeof(pieza),1,puntero);
                                                   printf("El nombre de la pieza es: %s\n",pieza.nombre);
                                                   printf("La cantidad de piezas son: %d\n",pieza.cantidad);
                                                   printf("El precio de cada pieza es: %f\n",pieza.precio_unitario);
    
    fclose(puntero);
  
  system("PAUSE");	
  return 0;
}
