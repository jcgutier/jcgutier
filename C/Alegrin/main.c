#include <allegro.h> 
#include <string.h>
       

void init();
void deinit();

int main() {
    char *ptr;
    int i=0;
    int j=0;
    double circle_pos_x =0;
    double circle_pos_x1 =0;
    double circle_pos_x2 =0;
    double circle_pos_x3 =100;
    double circle_pos_y =0;
    double circle_pos_y1 =0;
    double circle_pos_y2 =0;
    double circle_pos_y3 =100;
    BITMAP* buffer;
    char str_buf[256];
    char str_buf1[256];
    char str_buf2[256];
    
	init();

	
	buffer = create_bitmap(SCREEN_W,SCREEN_H);	
	void clear_bitmap(BITMAP* bitmap);
	
	sprintf(str_buf,"El tama�o de la pantalla es: %d x %d",SCREEN_W,SCREEN_H);
	sprintf(str_buf1,"Eltama�o de la cadena es de: %d",strlen(str_buf));
	while (!key[KEY_ESC]) {
		//clear_bitmap(screen);
		clear_bitmap(buffer);
		/*Circulo fijo*/
		//circlefill(screen,SCREEN_W/2,SCREEN_H/2,100,makecol(0,0,255) );
		/*Circulo Estatico*/
		textout(buffer, font,str_buf,0,0,makecol(150,150,255) );
		textout(buffer, font,str_buf1,0,10,makecol(150,150,255) );
		textprintf(buffer,font,0,20,makecol(100,255,100),"El tama�o dela pantalla es: %d x %d",SCREEN_W,SCREEN_H);
		textprintf(buffer,font,0,30,makecol(circle_pos_x/SCREEN_W*255,circle_pos_x/SCREEN_W*255,circle_pos_x/SCREEN_W*255),"Posicion del disco: %d", (int)circle_pos_x);
		textprintf(buffer,font,0,40,makecol(255,255,255),"Posicion y del disco: %d", (int)circle_pos_y);
		textprintf(buffer,font,0,50,makecol(255,255,255),"Posicion y del disco en juego: x%d , y%d ", (int)circle_pos_x3,(int)circle_pos_y3);
		circlefill(buffer,(int)circle_pos_x1,SCREEN_H/4,50,makecol(0,0,255) );
		circlefill(buffer,(int)circle_pos_x,SCREEN_H/2,50,makecol(0,0,255) );
		circlefill(buffer,(int)circle_pos_x2,SCREEN_H*0.75,50,makecol(0,0,255) );
		circlefill(buffer,SCREEN_W/4,(int)circle_pos_y1,50,makecol(0,0,255) );
		circlefill(buffer,SCREEN_W/2,(int)circle_pos_y,50,makecol(0,0,255) );
		circlefill(buffer,SCREEN_W*0.75,(int)circle_pos_y2,50,makecol(0,0,255) );
		circlefill(buffer,(int)circle_pos_x3,(int)circle_pos_y3,30,makecol(0,255,255) );
		text_mode(-1);
		textprintf_centre(buffer,font,320,60,makecol(255,0,255),"Hola");
		textprintf_centre(buffer,font,320,70,makecol(255,0,255),"La version de sistema es: %d %d y %d",os_type,os_revision,os_version);
		if(os_multitasking)textprintf_centre(buffer,font,320,80,makecol(255,0,255),"El sistema es multitasking");
		else textprintf_centre(buffer,font,320,90,makecol(255,0,255),"El sistema no es multitasking");
		j=0;
		i=1;
		ptr=cpu_vendor;
		while(*ptr!=NULL)
		{
                        textprintf(buffer,font,j,80,makecol(100,255,100),"%c",*ptr);
                        j+=8;
                        *ptr++;
                        i++;
        } 
		circle_pos_x1 += 2;
		circle_pos_x += 2.7;
		circle_pos_x2 += 1.9;
		circle_pos_y1 += 2;
		circle_pos_y += 1;
		circle_pos_y2 += 1;
		if(circle_pos_x-50>= SCREEN_W)circle_pos_x=0;
		if(circle_pos_x1-50>= SCREEN_W)circle_pos_x1=0;
		if(circle_pos_x2-50>= SCREEN_W)circle_pos_x2=0;
        if(circle_pos_y-50>= SCREEN_H)circle_pos_y=0;
        if(circle_pos_y1-50>= SCREEN_H)circle_pos_y1=0;
        if(circle_pos_y2-50>= SCREEN_H)circle_pos_y2=0;
        if(key[KEY_W])circle_pos_y=0;
        if(key[KEY_Q])circle_pos_y1=0;
        if(key[KEY_E])circle_pos_y2=0;
        if(key[KEY_RIGHT])circle_pos_x3+=0.1;
        if(key[KEY_LEFT])circle_pos_x3-=0.1;
        if(key[KEY_DOWN])circle_pos_y3+=0.1;
        if(key[KEY_UP])circle_pos_y3-=0.1;
        //if(circle_pos_y3==circle_pos_y)break;
		blit(buffer, screen, 0,0,0,0,SCREEN_W,SCREEN_H);
	}

	deinit();
	return 0;
}
END_OF_MAIN()

void init() {
	int depth, res;
	allegro_init();
	depth = desktop_color_depth();
	if (depth == 0) depth = 32;
	set_color_depth(depth);
	res = set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
	if (res != 0) {
		allegro_message(allegro_error);
		exit(-1);
	}

	install_timer();
	install_keyboard();
	install_mouse();
	/* add other initializations here */
}

void deinit() {
	clear_keybuf();
	/* add other deinitializations here */
}
