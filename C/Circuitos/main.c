#include <stdio.h>
#include <stdlib.h>
void paralelo(void);
float corriente(void);
float voltaje(void);

int main(int argc, char *argv[])
{
  char op;
  int c=0,l=0,a=0;
  float pot;
  do
  {
    system("cls");
    printf("Este programa resuelve algunas operaciones de "); 
    printf("circuitos electricos\n\n");
    printf(" Elija la opcion deseada:\n\n");
    printf("\tA.-  Equivalente de Resistencias Paralelo\n");
    printf("\tB.-  Calculo del Voltaje\n");
    printf("\tC.-  Calculo de la Corriente\n");
    if(c>=1)
    printf("\tW.-  Potencia con los datos anteriores\n");
    printf("\tX.-  Salir del Programa\n\n");
    printf(" Tu opcion es: ");
    scanf("%c",&op);
    switch (op)
    {
           case 'A':
                paralelo();
                break;
           case 'B':
                pot=voltaje();
                c++;
                break;
           case 'C':
                pot=corriente();
                c++;
                break;
           case 'W':
                system("cls");
                printf("\n\n\tLa Potencia con los datos anteriores es: %f\n\n\n\n",pot);
                system("pause");
                break;
    };
  }while(op!='X');           
  return 0;
}

void paralelo()
{
     float r1,r2,rt;
     system("cls");
     printf("Este programa calcula el valor resultante de dos resistencias");
     printf(" en paralelo\n\n");
     printf("\tIntroduzca los valores de las resistecias");
     printf("\n\n\t\tR1=");
     scanf("%f",&r1);
     printf("\n\t\tR2=");
     scanf("%f",&r2);
     rt=(r1+r2)/(r1*r2);
     printf("\nEl valor de la resistencia equivalente es de Rt=%5.2f\n\n",rt);                                            
     system("pause");
}

float voltaje()
{
     float v,i,r,p;
     system("cls");
     printf("Este programa calcula el voltaje en una resistencia");
     printf("dada la corriente\n\n");
     printf("\tIntroduzca el valor de la resistecia");
     printf("\n\n\t\tR=");
     scanf("%f",&r);
     printf("\n\tIntroduzca el valor de la corriente\n");
     printf("\n\t\tI=");
     scanf("%f",&i);
     v=i*r;
     printf("\nEl valor del voltaje es de V=%5.2f\n\n",v);
     p=v*i;
     system("pause");
     return(p);
}

float corriente()
{
     float v,i,r,p;
     system("cls");
     printf("Este programa calcula la corriente en una resistencia");
     printf("dado el valor del voltaje\n\n");
     printf("\tIntroduzca el valor de la resistecia");
     printf("\n\n\t\tR=");
     scanf("%f",&r);
     printf("\n\tIntroduzca el valor del voltaje\n");
     printf("\n\t\tV=");
     scanf("%f",&v);
     i=v/r;
     printf("\nEl valor de la corriente es de I=%f\n\n",i);
     p=v*i;
     system("pause");
     return(p);
}
