#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>

void llama(int *p);
void ahex(int n);
void abin(char *valor);

int main(int argc, char *argv[])
{
    /*----------------------------------------------------------------------
          Programa de Loading
    int i,r;
    printf("Introduce un numero =>");
    scanf("%d",&r);
    printf("\n \n ");
    for(i=0;i<=r;i++)
    {
                     printf("Loading ... %d ",i);
                     if(i%2==0)
                     printf("\/ \r");
                     else
                     printf("\\ \r");
    }
    getch();
    ----------------------------------------------------------------------*/
    /*----------------------------------------------------------------------
           Imprimir los valores condicionales
    float valor;
    valor=(3==3);
    printf("%f",valor);
    ----------------------------------------------------------------------*/
    /*----------------------------------------------------------------------
         Convertidor a hexadecimal
        int n;
        int a,b;

        printf("\n\n\n");
        printf("Introduzca un numero entero de 0 a 255 >>>");
        scanf("%i",&n);

        a=n/16; //Obtiene el valor de los 4 bist superiores
        b=n%16; //Obtiene el valor de los 4 bits inferiores

        printf("\nEl numero en hexadecimal es: ");
        ahex(a);
        ahex(b);
        printf("\n\n\n");
    getch();
    return 0;
    ----------------------------------------------------------------------*/
    /*----------------------------------------------------------------------
      Serie de Fibonacci
        int n,anterior,nuevo,temp,j;
        char p;
        printf("\nIntroduzca el numero de terminos que se generaran =>");
        scanf("%i",&n);
        anterior=0;
        nuevo=1;
        printf("\n%i %i ",anterior,nuevo);
        for(j=1;j<=n-2;j++)
        {
                temp=anterior+nuevo;
                anterior=nuevo;
                nuevo=temp;
                printf("%i ",nuevo);
        }
        ----------------------------------------------------------------------*/
        /*----------------------------------------------------------------------
            Valores
        unsigned char contador;
        contador=0;
        while(contador!=255)
        {
                            printf(" %d-%X ",contador,contador);
                            contador ++;
        }
        printf(" %d-%X ",contador,contador);
        contador++;
        printf(" %d-%X ",contador,contador);                    
        ----------------------------------------------------------------------*/
        /*----------------------------------------------------------------------
             Tam�o de los char (octetos)
        char caracter;
        int numero;
        printf("El tama�o de un caracter en su sistema es %d octetos.\n",sizeof(caracter));
        printf("El tama�o de un entero en su sistema es %d octetos,\n",sizeof(numero));
        ----------------------------------------------------------------------*/
        /*----------------------------------------------------------------------
            Punteros
        char valor;
        char *puntero;
        valor=97;
        printf("%u => |%d| <= direccion y datos de valor.\n",&valor,valor);
        puntero=&valor;
        printf("%u => |%d| <= direccion y datos de puntero.\n",&puntero,puntero);
        printf("El valor almacenado en puntero = %d\n",puntero);
        printf("Direcion de: &puntero = %u\n",&puntero);
        printf("Valor referenciado por puntero: *puntero = %d\n",*puntero);
        *puntero=2;
        printf("Nuevo valor almacenado en valor= %d\n",valor);
        printf("El valor almacenado en puntero = %d\n",puntero);
        llama(&valor);
        printf("Nuevo valor almacenado en valor= %d\n",valor);
        ----------------------------------------------------------------------*/
        /*----------------------------------------------------------------------
           Conversion a Binario
        unsigned char numero;
        printf("Introduzca un numero del 0 al 255 \n >>>");
        scanf("%d",&numero);
        printf("El numero %d en binario es: ",numero);
        abin(&numero);
        ----------------------------------------------------------------------*/
        /*----------------------------------------------------------------------
           caracteres
        char cadena[] = "Hola";
        char *ptr;
        int i=0;
        ptr=cadena;
        printf("La cadena es %s\n",cadena);
        printf("La cadena por cada caracter es:\n");
        printf("Direccion %d |%c|\n",ptr,*ptr);
        printf("Direccion %d |%c|\n",ptr,*(ptr+1));
        printf("Direccion %d |%c|\n",ptr,*(ptr+2));
        printf("Direccion %d |%c|\n",ptr,*(ptr+3));
        printf("El valor de isdigit %d",isalpha('G'));
        printf("\n");
        while(*(ptr+i)!=NULL)
        {
                         printf(" | %c | \n",*(ptr+i));
                         i++;
        }
        ----------------------------------------------------------------------*/
        /*----------------------------------------------------------------------
           Conviente a mayusculas la cadena introducida y elimina signos
        char cadena[80];
        int i=0;
        printf("Introduzca una cadena que contenga letras, numero y caracteres especiales =>\n");
        gets(cadena);
        while(cadena[i]!=NULL)
        {
                              if((ispunct(cadena[i])==0)&&(isdigit(cadena[i])==0))
                              {
                                                                                  if(islower(cadena[i])!=0)
                                                                                                           cadena[i]=cadena[i]&0xdf;
                                                                                  printf("%c",cadena[i]);
                              }
                              i++;
        }
        ----------------------------------------------------------------------*/
        char cadena[10];
        int numero;
        printf("Introduzca un entero con signo => ");
        gets(cadena);
        numero=atoi(cadena);
        printf("\n");
        printf("Introdujo en numero %d",numero);
        
        
        getch();
        return 0;
}

void llama(int *p)
{
     *p=5;
}

void ahex(int n)
{
        if((n>=0) && (n<=9))
                printf("%i",n);
        else
        {
                switch(n)
                {
                        case 10 : printf("A"); break;
                        case 11 : printf("B"); break;
                        case 12 : printf("C"); break;
                        case 13 : printf("D"); break;
                        case 14 : printf("E"); break;
                        case 15 : printf("F"); break;
                }
        }
}

void abin(char *valor)
{
     unsigned char position = 0x80;
     char temp;
     while(position!=0)
     {
                       temp= *valor & position;
                       //printf("el valor de temp es: %d & %d ",*valor,position);
                       (temp == 0) ? printf("0 "):printf("1 ");
                       //getch();
                       position >>= 1;
     }
}
