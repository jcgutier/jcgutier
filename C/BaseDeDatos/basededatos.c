#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void escribeR();
void lectura_T();
void lectura_P();


typedef struct 
{
        char nombre[15];
        int cantidad;
        float precio_unitario;
}tipo_pieza;

int main(int argc, char *argv[])
{
    int op;
    do
    {
                printf("Este es el menu principal de la base de datos escoge una accion: \n\n");
                printf("1.- Escribe un registro dentro de la base de datos.\n");
                printf("2.- Lee toda la base de datos\n");
                printf("3.- En construccion ...\n");
                printf("0.- Salir ...\n\n");
                printf("Opcion >>> ");
                scanf("%d",&op);
                getchar();
                switch(op)
                {
                          case 1: escribeR();
                                  break;
                          case 2: lectura_T();
                                  break;
                          case 3: lectura_P();
                                  break;
                          case 0: break;
                          default: printf("No existe esa seleccion\n");
                                   op=5;
                                   system("cls");
                }
     }while(op!=0);
  return 0;
}

void escribeR()
{
    char s;
    tipo_pieza pieza;
    FILE *puntero;
    printf("\n\n\n");
    
    puntero=fopen("BDV1.JC","ab");
    
    do
    {
                                     if(s=='s')
                                               getchar();
                                     printf("\nNombre de la pieza => ");
                                     gets(pieza.nombre);
                                     printf("Numero de piezas => ");
                                     scanf("%d",&pieza.cantidad);
                                     printf("Precio de cada pieza => ");
                                     scanf("%f",&pieza.precio_unitario);
                                     fwrite(&pieza,sizeof(pieza),1,puntero);
                                     //fprintf(puntero,"%s   %d   %f",pieza.nombre_pieza,pieza.cantidad,pieza.precio_unitario);
                                     printf("Quiere introducir mas piezas? (s/n) ");
                                     getchar();
                                     s=getchar();
    }while(s=='s');
    
    fclose(puntero);
    printf("\n\n\n");
}


void lectura_T()
{
    int s=0;
    tipo_pieza pieza;
    FILE *puntero;
    char *pPath,*pro;
    printf("\n\n\n");
    if((puntero=fopen("BDV1.JC","rb"))==NULL)
    {
                                              printf("ERROR!!! No existe el archivo de la Base de Datos. Consulta a tu Programador ... \n");
                                              system("PAUSE");
                                              exit(-1);
    }
    pPath = getenv ("OS");
    pro = getenv("PROCESSOR_IDENTIFIER");
    if (pPath!=NULL)
       printf ("El sistema es: %s\nEl prcesador es: %s\n\n",pPath,pro);
    
    printf("-------------------------------------------------------------------------\n");
    printf("|Reg\t|Marca\t\t\t|Cantidad\t|Precio Unitario\t|\n");
    printf("-------------------------------------------------------------------------\n");
    while(fread(&pieza,sizeof(pieza),1,puntero)==1)
    {
                                                   printf("|%d\t",s);
                                                   if(strlen(pieza.nombre)<=6)
                                                                  printf("|%s\t\t\t",pieza.nombre);
                                                   else
                                                                  printf("|%s\t\t",pieza.nombre);
                                                   printf("|%d\t\t",pieza.cantidad);
                                                   if(pieza.precio_unitario>999)
                                                                                printf("|%0.2f\t\t|",pieza.precio_unitario);
                                                   else
                                                                                printf("|%0.2f\t\t\t|",pieza.precio_unitario);
                                                   printf("\n");
                                                   s++;
    }
    printf("-------------------------------------------------------------------------\n");
    
    fclose(puntero);
    printf("\n\n\n");
}

void lectura_P()
{
    tipo_pieza pieza;
    FILE *puntero;
    int num_registro;
    long int desplazamiento;
    printf("\n\n\n");
    if((puntero=fopen("BDV1.JC","rb"))==NULL)
    {
                                              printf("ERROR!!! No existe el archivo de la Base de Datos. Consulta a tu Programador ... \n");
                                              system("PAUSE");
                                              exit(-1);
    }
    
    printf("Introduzca el numero de registro: ");
    scanf("%d",&num_registro);
    desplazamiento=num_registro*sizeof(pieza);
    
    if(fseek(puntero,desplazamiento,0)!=0)
    {
                                          printf("El punteor ha superado el limite del archivo");
                                          exit(-1);
    }
    
    fread(&pieza,sizeof(pieza),1,puntero);
                                                   printf("El nombre de la pieza es: %s\n",pieza.nombre);
                                                   printf("La cantidad de piezas son: %d\n",pieza.cantidad);
                                                   printf("El precio de cada pieza es: %f\n",pieza.precio_unitario);
    
    fclose(puntero);
    printf("\n\n\n");
}
