#include <stdio.h>
#include <stdlib.h>

void anex(int n);

int main(int argc, char *argv[])
{
  int n,a,b;
  printf("Este programa convierte un numero decimal en");
  printf(" un numero hexadecimal\n\n");
  printf("\tIntroduzca un nuemro del 0 al 255 => ");
  scanf("%d",&n);
  printf("\n\n");
  a=n/16;
  b=n%16;
  anex(a);
  anex(b);
  printf("\n\n");
  system("PAUSE");	
  return 0;
}

void anex(int n)
{
     if((n>=0)&&(n<=9))
       printf("%i",n);
     else
     {
         switch(n)
         {
                  case 10: printf("A"); break;
                  case 11: printf("B"); break;
                  case 12: printf("C"); break;
                  case 13: printf("D"); break;
                  case 14: printf("E"); break;
                  case 15: printf("F"); break;
         }
     }
}
