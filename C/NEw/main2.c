#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 
int main()
{
        FILE *archivo;
        FILE *archivos;
        char caracter;
        char vista[1000][1000];
        char reportes[1000][100];
        char remedy[1000][100];
        char det[1000][100];
        char ffecha[1000][100];
        char ifecha[1000][100];
        char nombre_aux[1000];
        int i=0;
        int j=0;
        int k=0;
        int q=0;
        int w=0;
        int f=0;
        int ie=0;
        int ayuda=0;
        time_t tiempo = time(0);
        struct tm *tlocal = localtime(&tiempo);
        char fecha[128];
        char hora[128];
        strftime(fecha,128,"\"20%y/%m/%d\"",tlocal);
        strftime(hora,128,"\"%H:%M\"",tlocal);
 
        archivo = fopen("Informe.csv","r");
 
        if (archivo == NULL){
 
                printf("\nError de apertura del archivo. \n\n");
        }else{
 
 
            printf("\nEl contenido del archivo de prueba es: \n");
 
            while (feof(archivo) == 0)
            {
                caracter = fgetc(archivo);
                if(caracter=='\n'||caracter==',')
                {
                                                 //vista[j][i]=NULL;
                                                 i=0;
                                                 j++;
                                                 if(j%7==0) w++;
                                                 if(j%7==1) q++;
                                                 if(j%7==2) k++;
                                                 if(j%7==3) f++;
                                                 if(j%7==5) ie++;
                }
                else
                {
                    if(j%7==0) det[w][i]=caracter;
                    if(j%7==1) reportes[q][i]=caracter;
                    if(j%7==2) remedy[k][i]=caracter;
                    if(j%7==3) ffecha[f][i]=caracter;
                    if(j%7==5) ifecha[ie][i]=caracter;
                    //vista[j][i]=caracter;
                    i++;
                }
            }
        }
        fclose(archivo);
        //for(j=0;j<=ayuda;j++) printf("%s\n",vista[j]);
        printf("\n");
        printf("Las determinantes son: \n");
        for(j=0;j<w;j++) printf("%s\n",det[j]);
        printf("\n");
        getch();
        printf("Los reportes de Remedy son \n");
        for(j=1;j<=k;j++) printf("%s\n",remedy[j]);
        printf("\n");
        getch();
        printf("Los reportes de RCMS son: \n");
        for(j=1;j<=q;j++) printf("%s\n",reportes[j]);
        printf("\n");
        getch();
        printf("Las fechas de cierre son \n");
        for(j=1;j<=f;j++) printf("%s\n",ffecha[j]);
        printf("\n");
        getch();
        printf("Las fechas de inicio son \n");
        for(j=1;j<=ie;j++) printf("%s\n",ifecha[j]);
        printf("\n");
        printf("El numero de reportes totales son de %d\n",ie);
        printf("Escribiendo en el archivo ...\n");
        
        archivos = fopen("Archivo_de_Salida.csv","w");
        
        fprintf(archivos,"%s","Las determinantes son: \n");
        for(j=1;j<=w;j++) fprintf(archivos,"%s,%s,%s\n",remedy[j],reportes[j],det[j-1]);
        
        fclose ( archivos );
        
        /* Ordenar las cosas
        for(j=0;j<=q;j++)
        for(i=j+1;i<=q;i++)
        {
                          if(strcmp(reportes[j],reportes[i])>0)
                          {
                                                             strcpy(nombre_aux,reportes[j]);
                                                             strcpy(reportes[j],reportes[i]);
                                                             strcpy(reportes[i],nombre_aux);
                          }
        }
        for(j=0;j<=q;j++)
        for(i=j+1;i<=q;i++)
        {
                          if(strcmp(remedy[j],remedy[i])>0)
                          {
                                                             strcpy(nombre_aux,remedy[j]);
                                                             strcpy(remedy[j],remedy[i]);
                                                             strcpy(remedy[i],nombre_aux);
                          }
        }
        for(i=j+1;i<=q;i++)
        {
                          if(strcmp(det[j],det[i])>0)
                          {
                                                             strcpy(nombre_aux,det[j]);
                                                             strcpy(det[j],det[i]);
                                                             strcpy(det[i],nombre_aux);
                          }
        }
        printf("\n");
        printf("Los reportes de RCMS ordenados son: \n");
        for(j=1;j<=q;j++) printf("%s\n",reportes[j]);
        printf("\n");
        printf("Los reportes de Remedy ordenados son: \n");
        for(j=1;j<=k;j++) printf("%s\n",remedy[j]);
        printf("\n");
        printf("Las determinantes ordenadas son: \n");
        for(j=0;j<=w;j++) printf("%s\n",det[j]);*/
        
        return 0;
}
