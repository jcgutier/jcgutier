#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
  char texto[1000];
  char vsrt[]="AEIOU";
  char con[]="BCDFGHJKLMN�PQRSTVWXYZ";
  static int vocales;
  static int consonantes;
  static int vcount[5];
  static int ccount[23];
  int j;
  int i=0;
  int k;
  char *p;
  printf("Ingresa algun texto con menos de 1000 caracteres\n===>");
  gets(texto);
  p=texto;
  for(j=0;j<strlen(texto);j++)
  {
     if(isalpha(texto[j])!=0)
           texto[j]=texto[j]&0xdf;
     if(strchr(vsrt,texto[j])!='\0')
           vocales++;
     if(strchr(con,texto[j])!='\0')
           consonantes++;
     switch(texto[j])
     {
          case 'A' : vcount[0]++; break;
          case 'E' : vcount[1]++; break;
          case 'I' : vcount[2]++; break;
          case 'O' : vcount[3]++; break;
          case 'U' : vcount[4]++; break;
     }
  }
  while(texto[i]!=NULL)
  {
         k=0;
         do
         {
             if(*(p+i)==con[k]) 
                   ccount[k]++;
             k++;
         }while(k<strlen(con));
         i++;
  }
  printf("-----------------------------------------\n");
  printf("El texto de entrada contiene %d vocales\n",vocales);
  printf("El texto de entrada contiene %d consonantes\n",consonantes);
  printf("-----------------------------------------\n");
  printf("De las vocales:\n");
  /*printf("[A] ------> %d\n",vcount[0]);
  printf("[E] ------> %d\n",vcount[1]);
  printf("[I] ------> %d\n",vcount[2]);
  printf("[O] ------> %d\n",vcount[3]);
  printf("[U] ------> %d\n",vcount[4]);*/
  p=vsrt;
  for(i=0;i<strlen(vsrt);i++)
  {
                            if(vcount[i]!=0)
                                 printf("[%c] ------> %d\n",*(p+i),vcount[i]);
  }
  printf("-----------------------------------------\n");
  printf("De las consonantes:\n");
  p=con;
  for(i=0;i<strlen(con);i++)
  {
                            if(ccount[i]!=0)
                                 printf("[%c] ------> %d\n",*(p+i),ccount[i]);
  }
  system("PAUSE");
  return 0;
}
