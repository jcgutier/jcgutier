#include <allegro.h>       

void init();
void deinit();

int main() {
    int i,j=0;
    char *p;
    int pos_x11 =300;
    int pos_y11 =100;
    int pos_x12 =310;
    int pos_y12 =110;
    
    int pos_x21 =100;
    int pos_y21 =100;
    int pos_x22 =110;
    int pos_y22 =110;
    
    BITMAP* buffer;
    
	init();
	
	buffer = create_bitmap(SCREEN_W,SCREEN_H);	
	void clear_bitmap(BITMAP* bitmap);
	
	while (!key[KEY_ESC]) {
		clear_bitmap(buffer);
		textprintf(buffer,font,0,0,makecol(100,255,100),"El tama�o dela pantalla es: %d x %d",SCREEN_W,SCREEN_H);
		textprintf(buffer,font,0,10,makecol(100,255,100),"El tipo de procesador es: ");
		j=0;
		p=cpu_vendor;
		while(*p!=NULL)
		{
                        textprintf(buffer,font,210+j,10,makecol(100,255,100),"%c",*p);
                        j+=8;
                        *p++;
        }
		//textprintf(buffer,font,0,10,makecol(255,255,255),"Posicion del disco:( %d , %d )",circle_pos_x,circle_pos_y);
		//textprintf(buffer,font,0,20,makecol(255,255,255),"Posicion del disco1:( %d , %d )",circle_pos_x1,circle_pos_y1);
		rectfill(buffer,pos_x11,pos_y11,pos_x12,pos_y12,makecol(0,0,255) );
		rectfill(buffer,pos_x21,pos_y21,pos_x22,pos_y22,makecol(0,255,255) );
		
		
		if(pos_x11 == SCREEN_W){pos_x11=-10;pos_x12=0;}
		if(pos_y11 == SCREEN_H){pos_y11=-10;pos_y12=0;}
		//if(pos_x+10<= 0)circle_pos_x=SCREEN_W;
		//if(pos_y+10<= 0)circle_pos_y=SCREEN_H;
		/*
		if(circle_pos_x1-50>= SCREEN_W)circle_pos_x1=0;
		if(circle_pos_y1-50>= SCREEN_H)circle_pos_y1=0;
		if(circle_pos_x1+50<= 0)circle_pos_x1=SCREEN_W;
		if(circle_pos_y1+50<= 0)circle_pos_y1=SCREEN_H;*/
		
		/*Controles
		if(key[KEY_D])circle_pos_x1++;
		if(key[KEY_A])circle_pos_x1--;
		if(key[KEY_S])circle_pos_y1++;
		if(key[KEY_W])circle_pos_y1--;*/
		
        if(key[KEY_RIGHT]){pos_x11++;pos_x12++;}
        if(key[KEY_LEFT]){pos_x11--;pos_x12--;}
        if(key[KEY_DOWN]){pos_y11++;pos_y12++;}
        if(key[KEY_UP]){pos_y11--;pos_y12--;}
        
        /*Puntos
        if(circle_pos_x1==circle_pos_x&&circle_pos_y1==circle_pos_y)break;
        if(circle_pos_y1-50==circle_pos_y+50||circle_pos_y1+50==circle_pos_y-50)
        {
             //if(circle_pos_x1-50==circle_pos_x+50||circle_pos_x1+50==circle_pos_x-50)break;
             break;
        }       */ 
        
		blit(buffer, screen, 0,0,0,0,SCREEN_W,SCREEN_H);
	}

	deinit();
	return 0;
}
END_OF_MAIN()

void init() {
	int depth, res;
	allegro_init();
	depth = desktop_color_depth();
	if (depth == 0) depth = 32;
	set_color_depth(depth);
	res = set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
	if (res != 0) {
		allegro_message(allegro_error);
		exit(-1);
	}

	install_timer();
	install_keyboard();
	install_mouse();
	/* add other initializations here */
}

void deinit() {
	clear_keybuf();
	/* add other deinitializations here */
}
