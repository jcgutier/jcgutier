
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

using namespace std;

int main()
{
    int x;
    int a[15];
    int *p;
    cout<<endl<<"Este programa busca el numero de repeticiones de 199 cualquier numero de 15 digitos que introduzcas"<<endl<<endl;
    system("pause");
    system("cls");
    for(int i=0;i<15;i++)
    {
        cout<<"introduce el valor"<<i<<"=";
        cin>>a[i];
    }
    p=&a[0];
    cout<<endl<<"Direccion inicial:"<<p<<endl;
    cout<<endl<<"El numero introducido es:";
    for(int j=0;j<15;j++)
    {
        p=&a[j];
        cout<<*p;
    }
    cout<<endl;
    cout<<endl<<"La direccion final es:"<<p<<endl;
    cout<<endl;
    x=0;
    for(int z=0;z<15;z++)
    {
        p=&a[z];
        if(*p==1)
        {
            p=&a[z+1];
            if(*p==9)
            {
                p=&a[z+2];
                if(*p==9)
                {
                    x++;
                }
            }
        }
    }
    cout<<"El numero de repeticiones de 199 es:"<<x<<endl;
    system("pause");
}
