#!/usr/bin/perl

use strict;
use warnings;
use Switch;
use DBI;

#Para saber si es numero o no
use Scalar::Util qw(looks_like_number);

#Variables
my $id=1;
my $fecha1;
my $sec;
my $min;
my $hour;
my $mday;
my $mon;
my $year;
my $wday;
my $yday;
my $isdst;

#Bienvenida con hora local
$fecha1 = localtime();
print "\n\n\tBienvenido la fecha local es $fecha1\n";


#Ciclo del menu del programa
foo: {
	while ( $id != 0 ){
		print "\n";
		print "**************************************************************************************\n";
		print "**************************************************************************************\n";
		print "**************************************************************************************\n";
		print "**************************************************************************************\n";
		print "**************************************************************************************\n";
		print "**************************************************************************************\n";
		print "\nMenu principal de opciones\n";
		print "\t1.- Registro nuevo\n";
		print "\t2.- Mostrar registros\n";
		print "\n0.- Salir";
		print "\nElige una opcion => ";
		$id=<STDIN>;
		if ( !(looks_like_number( $id ) ) ){
			print "\n\t+++++Opcion invalida prueba de nuevo+++++\n";
			$id = 1;
		}
		else{
			$id+=1; $id-=1;
			if ( $id > 2 ){
				print "\n\t+++++Opcion invalida prueba de nuevo+++++\n";
			}
			switch( $id ){
				case 1	{NuevoRegistro(); last foo;} 
				case 2  {LeerRegistros(); last foo;}
			}
		}
	}
}

sub NuevoRegistro{
	#Fecha
	my $fecha;
	my $lastfecha;
	my $dia;
	my $continua;
	my $lastpay;

#######################################################################################################
###Inicia conexion a Base de Datos
#######################################################################################################

        my $dbname = 'registros';
        my $dbhost = '127.0.0.1';
        my $dbuser = 'DBA';
        my $dbpwd = 'Mexico10';
 
        my $dbh;
        my $stmt;
        my $sth;
        my $row;

        print "\nConectando con la base de datos ...\n";

         ## Conectarse a la base de datos
         $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpwd) or die "Error de conexion: $DBI::errstr";

        print "Conexión OK.\n\n";

	$sth = $dbh->prepare("SELECT fecha, pagoquincena, tcredito, gastos, restante FROM quincena order by fecha asc");
        $sth->execute();
        while ($row = $sth->fetchrow_hashref) {
                #$row = $sth->fetchrow_hashref;
                $lastfecha="$row->{fecha}";
		$lastpay="$row->{tcredito}";
        }
        print "+----------------------+---------------+\n";
        print "+----------------------+---------------+\n";
        print "\nLa ultima fecha registrada es $lastfecha\n\n";
	my $lastmonth = substr $lastfecha , 5, 2;
	$lastmonth++;
	$lastmonth--;

	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
	$year += 1900;
	$mon +=1;
	#print "Hoy es $mday de $mon";
	if($mday >= 15 && $mday < 30){
		$dia="15";
	}
	else{
		$dia="30";
		$mon--;
	}
	$fecha="$year-$mon-$dia";
	print "\nFecha en el registro sera: ";
	print " $fecha\n";
	print "La fecha de registro es la correcta? (y/n) ";
	$continua=<STDIN>;
        chop( $continua );
	if ( $continua eq "n" ){
		print "Ingresa la fecha de registro con el formato: YYYY-MM-DD => ";
		$fecha=<STDIN>;
		chop( $fecha );
		print "La fecha de registro sera: $fecha \n";
		print "\t¿Quieres continuar? (y/n) ";
		$continua=<STDIN>;
        	chop( $continua );
		if ( $continua eq "n" ){
			last;
		}
	}


	#Variables
	my $quincena;
	my $quincena1;
	my $tcredito;
	my $luz=0;
	my $gas=0;
	my $restante;
	my $depa=0;
	my $ahorro=0;
	my $comidas=0;
	my $transporte=0;
	my $internet=0;
	my $cel=0;
	my $cosasdepa=0;
	my $otros=0;
	my $otrosgastos=0;

	print "\n\nIntroduce el pago de tu quincena ==> ";
	$quincena=<STDIN>;
	print "Introduce los gastos adicionales o previos ==> ";
	$otrosgastos=<STDIN>;
	print "Introduce el pago de luz ==> ";
        $luz=<STDIN>;
	print "Introduce el pago de gas ==> ";
        $gas=<STDIN>;

	if($lastmonth != $mon){
                $tcredito = $lastpay;
	}
	else{
		print "Introduce el pago para no generar intereses de la tarjeta de credito ==> ";
        	$tcredito=<STDIN>;
        	$tcredito/=2;
	}

	$restante = $otros - $otrosgastos - $tcredito - $gas - $luz;
	$quincena1 = $quincena - $otros - $cel - $transporte - $comidas - $internet - $ahorro - $cosasdepa - $depa;	
	$restante = $restante + $quincena1;
	
	#Ejecucion en pantalla
	print "\n\n**IMPORTANTE: El pago de la tarjeta para esta quincena es de: $tcredito\n";
	print "*El restante para gastos es de: $restante\n\n\n";

	print "\n****************************\n";
	print "\n****************************\n";
	print "\n****************************\n";
	print "Los datos son correctos y deseas continuar? (s/n) => ";
	$continua=<STDIN>;
        chop( $continua );
        if ( $continua eq "n" ){
		print "Cerrando conexión ...\n";
	        ## Desconectarse de la base de datos
        	if (! $dbh->disconnect) { warn "Error al desconectarse de la base de datos: $DBI::errstr"; }
		last;
	}

	print "Insertando datos ...\n";
	
 	## Preparar la sentencia de insercion en la base de datos
 	$sth = $dbh->prepare("insert into quincena ( fecha, pagoquincena, tcredito, gastos, renta, cosasdept, gas, luz, cel, ahorro, 
			restante, Internet, comidas, transporte) values ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? )");
 
	## Insertar un registro en la tabla
	$sth->execute( $fecha, $quincena, $tcredito, $otrosgastos, $depa, $cosasdepa, $gas, $luz, $cel, $ahorro, $restante, 
			$internet, $comidas, $transporte );
 
	print "Datos insertados en la base de datos.\n";
 

	print "Cerrando conexión ...\n";
	## Desconectarse de la base de datos
	if (! $dbh->disconnect) { warn "Error al desconectarse de la base de datos: $DBI::errstr"; }
	print "Conexión cerrada. ¡Hasta pronto!\n\n";
}

sub LeerRegistros(){

	my $dbname = 'registros';
        my $dbhost = '127.0.0.1';
        my $dbuser = 'DBA';
        my $dbpwd = 'Mexico10';

        my $dbh;
        my $stmt;
        my $sth;
        my $row;
	my $fecha;

	print "Conectando con la base de datos ...\n";
        ## Conectarse a la base de datos
        $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpwd) or die "Error de conexion: $DBI::errstr";
        print "Conexión OK.\n";
	
	#print "\nIntroduce la fecha que quieres ver con el formato: aaaa-mm-dd ==> ";
	#$fecha=<STDIN>;
        #chop( $fecha );
	#print "*************************  $fecha ";	
	print "+----------------------+---------------+\n";
	## Leer los registros de la tabla
	$sth = $dbh->prepare("SELECT fecha, pagoquincena, tcredito, gastos, restante FROM quincena order by fecha asc");
	$sth->execute();
	while ($row = $sth->fetchrow_hashref) {
		#$row = $sth->fetchrow_hashref;
		print "Fecha:\t\t\t" . $row->{fecha} . "\n";
		print "Quincena:\t\t" . $row->{pagoquincena} . "\n";
		print "Pago Tarjeta Credito:\t" . $row->{tcredito} . "\n";
		print "Gastos:\t\t\t" . $row->{gastos} . "\n";
		print "Restante:\t\t" . $row->{restante} . "\n";
		print "+----------------------+---------------+\n";
		print "+----------------------+---------------+\n";
		$fecha="$row->{fecha}";
	}
	print "+----------------------+---------------+\n";
	print "+----------------------+---------------+\n";
	print "\nLa ultima fecha es $fecha\n";
	print "Cerrando conexión ...\n";
        ## Desconectarse de la base de datos
        if (! $dbh->disconnect) { warn "Error al desconectarse de la base de datos: $DBI::errstr"; }
        print "Conexión cerrada. ¡Hasta pronto!\n\n";

}
