#!/usr/bin/perl

use strict;
use warnings;

use DBI;

my $quincena = 0;


print "Introduce tu quincena\n";
$quincena=<STDIN>;
print "Tu quincena es $quincena \n";

my $dbname = 'test';
my $dbhost = '127.0.0.1';
my $dbuser = 'DBA';
my $dbpwd = 'Mexico10';
 
my $dbh;
my $stmt;
my $sth;
my $row;

my $partid;
my $partname;
my $catid;

print "Introduce el ID de la parte\n";
$partid=<STDIN>;
print "Introduce el nombre de la parte\n";
$partname=<STDIN>;
chop( $partname );
print "Introduce el cat id de la parte\n";
$catid=<STDIN>;
 
 ## Conectarse a la base de datos
 ##
 $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpwd)
                     or die "Error de conexion: $DBI::errstr";
 
 ## Preparar la sentencia de insercion en la base de datos
 $sth = $dbh->prepare(
         "insert into Parts ( PartID, PartName, CatID) values ( ? , ? , ? )"
			);
 
 ## Insertar un registro en la tabla
 ##
 $sth->execute( $partid, $partname, $catid );
 
 ## Leer los registros de la tabla
 ##
# $sth = $dbh->prepare("select nombre, edad, fecha from tabla");
# $sth->execute();
# while ($row = $sth->fetchrow_hashref) {
 #    print "nombre: " . $row->{nombre} . "\n";
  #   print "edad: " . $row->{edad} . "\n";
   #  print "fecha de alta: " . $row->{fecha_alta} . "\n";
# }
 ## Desconectarse de la base de datos
 ##
 if (! $dbh->disconnect) {
     warn "Error al desconectarse de la base de datos: $DBI::errstr";
 }
#####################################################################
