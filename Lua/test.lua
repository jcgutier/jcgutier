-- Comments on lua look lik these
print (foo)

foo = true
print ("The value of foo is:")
print (foo)


result = 5 > 3
print ("Is 5 > 3?")
print (result)


hello = "hello, world"
-- Assign length to variables
count_hash = #hello;
count_func = string.len(hello)
print ("The string:")
print (hello)
-- Print the variables assigned at the top
print ("Has a length of:")
print (count_hash)
print(count_func)
-- Use string literals, in place
print (#"hello, world")
print (string.len("hello, world"))

name = "Mike"
color = "Blue"
-- Concatenate three strings
print ("Jill " .. "likes" .. " Red")
-- Concatenate a variable and a strings
print ("Jack dislikes " .. color)
-- Concatenate two variables and a string
print (name .. " likes " .. color)
-- Concatenate only variables
print (name .. color)
-- Assign result to variable
message = name .. " likes " .. color
print (message)

pi = 3.14
message = "The rounded value of pi is: " .. pi
print(message)
print("Nine: " .. 9)

print ("Please enter your name:")
name = io.read()
print ("Hello " .. name)
