export DISTRO_CODENAME="focal"
export CURRENT_USER=$USER
export CURRENT_USER_GROUP=$(id -gn $USER)

sudo -E env "PATH=$PATH" ansible-playbook personal_config.yml -i hosts.ini