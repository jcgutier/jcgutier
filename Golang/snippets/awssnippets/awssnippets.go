package awssnippets

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// AwsSession get the session
func AwsSession() *session.Session {
	fmt.Println("Aws Sessions")
	awsSession, awsSessionError := session.NewSession()
	if awsSessionError != nil {
		fmt.Println(awsSessionError)
		os.Exit(1)
	}
	return awsSession
}

// func CloudWatchMetrics() bool {
// 	svc := cloudwatch.New(awsSession)
// 	_, err := svc.PutMetricData(&cloudwatch.PutMetricDataInput{
// 		Namespace: aws.String("Site/Traffic"),
// 		MetricData: []*cloudwatch.MetricDatum{
// 			&cloudwatch.MetricDatum{
// 				MetricName: aws.String("PageViews"),
// 				Unit:       aws.String("Count"),
// 				Value:      aws.Float64(18057.0),
// 				Dimensions: []*cloudwatch.Dimension{
// 					&cloudwatch.Dimension{
// 						Name:  aws.String("PageURL"),
// 						Value: aws.String("my-page.html"),
// 					},
// 				},
// 			},
// 		},
// 	})
// 	if err != nil {
// 		fmt.Println("Error adding metrics:", err.Error())
// 		os.Exit(1)
// 	}

// 	// Get information about metrics
// 	result, err := svc.ListMetrics(&cloudwatch.ListMetricsInput{
// 		Namespace: aws.String("Site/Traffic"),
// 	})
// 	if err != nil {
// 		fmt.Println("Error getting metrics:", err.Error())
// 		os.Exit(1)
// 	}

// 	for _, metric := range result.Metrics {
// 		fmt.Println(*metric.MetricName)

// 		for _, dim := range metric.Dimensions {
// 			fmt.Println(*dim.Name+":", *dim.Value)
// 			fmt.Println()
// 		}
// 	}
// 	// Test GRaps
// 	return true
// }

func ListDynamoDB(awsSession *session.Session) {
	svc := dynamodb.New(awsSession)
	input := &dynamodb.ListTablesInput{}

	fmt.Printf("Tables:\n")

	for {
		// Get the list of tables
		result, err := svc.ListTables(input)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodb.ErrCodeInternalServerError:
					fmt.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
				default:
					fmt.Println(aerr.Error())
				}
			} else {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				fmt.Println(err.Error())
			}
			return
		}

		for _, n := range result.TableNames {
			fmt.Println(*n)
		}

		// assign the last read tablename as the start for our next call to the ListTables function
		// the maximum number of table names returned in a call is 100 (default), which requires us to make
		// multiple calls to the ListTables function to retrieve all table names
		input.ExclusiveStartTableName = result.LastEvaluatedTableName

		if result.LastEvaluatedTableName == nil {
			break
		}
	}
}
