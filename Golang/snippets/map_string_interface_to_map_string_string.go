func MsiToMss(msi map[string]interface{}) map[string]string {
	// Local variables
	returnMap := make(map[string]string, len(msi))
	var strvalue string
	for key, value := range msi {
		if _, ok := value.(string); ok {
			strvalue, _ = value.(string)
		} else if _, ok = value.(float64); ok {
			strvalue = strconv.Itoa(int(value.(float64)))
		} else if _, ok = value.(bool); ok {
			strvalue = strconv.FormatBool(value.(bool))
		}
		returnMap[key] = strvalue
	}
	return returnMap
}
