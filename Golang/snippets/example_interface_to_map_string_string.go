func InterfacetoMss(data interface{}) []map[string]string {
	// Define local variables
	var results []map[string]string
	var tdata []interface{}
	var ok bool

	// Test if data is an array
	tdata, ok = data.([]interface{})
	if !ok {
		// logger.Warning("data is not an array of interface")
		tdata = append(tdata, data)
	}

	// Transform data type
	for _, row := range tdata {
		mssRow := MsiToMss(row.(map[string]interface{}))
		results = append(results, mssRow)
	}
	return results
}
