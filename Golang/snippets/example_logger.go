package example_logger

import (
	"log"
	"os"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// TODO add execution ID on the logs

func LogInit() *zap.SugaredLogger {
	// Setting zap encoder config for log fields
	pe := zapcore.EncoderConfig{
		LevelKey:         "level",
		TimeKey:          "time",
		NameKey:          "logger",
		CallerKey:        "caller",
		EncodeCaller:     zapcore.ShortCallerEncoder,
		FunctionKey:      zapcore.OmitKey,
		MessageKey:       "msg",
		StacktraceKey:    "stacktrace",
		LineEnding:       zapcore.DefaultLineEnding,
		EncodeLevel:      zapcore.CapitalLevelEncoder,
		EncodeTime:       zapcore.RFC3339TimeEncoder,
		EncodeDuration:   zapcore.SecondsDurationEncoder,
		ConsoleSeparator: "   ",
	}

	// Setting file and console encoder to log to a file an to stderr
	fileEncoder := zapcore.NewJSONEncoder(pe)
	pe.EncodeLevel = zapcore.CapitalColorLevelEncoder
	consoleEncoder := zapcore.NewConsoleEncoder(pe)

	// Setting log level from LOGLEVEL environmental variable
	level := zap.InfoLevel
	if loglevel := os.Getenv("LOGLEVEL"); strings.ToUpper(loglevel) == "DEBUG" {
		level = zap.DebugLevel
	}

	// Setting log file location
	boctlLogFile, err := os.OpenFile("log/boctl.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Println(err)
	}

	core := zapcore.NewTee(
		zapcore.NewCore(fileEncoder, zapcore.AddSync(boctlLogFile), level),
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stderr), level),
	)
	// Suppressing output log if OUTOUTLOG is DISABLED
	if outputLog := os.Getenv("OUTPUTLOG"); strings.ToUpper(outputLog) == "DISABLED" {
		core = zapcore.NewTee(
			zapcore.NewCore(fileEncoder, zapcore.AddSync(boctlLogFile), level),
		)
	}

	l := zap.New(core, zap.AddCaller())

	return l.Sugar()
}

