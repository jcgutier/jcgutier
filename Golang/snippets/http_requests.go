package main

import (
    "fmt"
    "net/http"
    "crypto/tls"
)


/*##############################################################################################
## Peticion HTTP con el chequeo de seguridad deshabilitado para conexiones con certificados   ## 
## no confiables, o "self-sign".                                                              ##
##############################################################################################*/
func http_req() {
    http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
    _, err := http.Get("https://golang.org/")
    if err != nil {
        fmt.Println(err)
    }
}

/*##############################################################################################
## Peticiones HTTP con el chequeo de seguridad deshabilitado, usando un "client"              ##
##############################################################################################*/
func client_http_req() {
    tr := &http.Transport{
        TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
    }
    client := &http.Client{Transport: tr}
    _, err := client.Get("https://golang.org/")
    if err != nil {
        fmt.Println(err)
    }
}