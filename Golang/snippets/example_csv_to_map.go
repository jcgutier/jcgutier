func CsvToMap(csvFileName string) []map[string]string {
	csvFile, err := os.Open(csvFileName)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.FieldsPerRecord = -1

	csvData, err := reader.ReadAll()
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	var newMap []map[string]string

	var headers []string

	for index, each := range csvData {
		tmpMap := make(map[string]string, len(each))
		if index == 0 {
			lowerEach := []string{}
			for _, element := range each {
				lowerEach = append(lowerEach, strings.ToLower(element))
			}
			headers = lowerEach
			for _, header := range each {
				tmpMap[header] = ""
			}
		} else {
			for indx, value := range each {
				if len(headers) != len(each) {
					log.Fatal("Row ", each, " does not match headers len of ", len(headers))
				} else {
					tmpMap[headers[indx]] = value
				}
			}
			newMap = append(newMap, tmpMap)
		}
	}
	return newMap
}

