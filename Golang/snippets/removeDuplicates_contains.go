func Contains(s []string, str string) bool {
	for _, v := range s {
		if strings.ToLower(v) == strings.ToLower(str) {
			return true
		}
	}
	return false
}

func RemoveDuplicates(inputList []string) []string {
	newList := []string{inputList[0]}
	for _, value := range inputList {
		if !Contains(newList, value) {
			newList = append(newList, value)
		}
	}
	return newList
}
