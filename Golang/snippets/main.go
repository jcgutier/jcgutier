package main

import (
	"fmt"
	"snippets/awssnippets"
	"snippets/general"
)

func main() {
	fmt.Println("Snippets")
	awsSession := awssnippets.AwsSession()
	awssnippets.ListDynamoDB(awsSession)
	general.StartNestedArray()
}
