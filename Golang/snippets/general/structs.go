package general

import "fmt"

type AlertResume struct {
	AllActive int
	EnvActive []EnvActive
}

type EnvActive struct {
	Environment string
	Alerts      int
	Tagged      int
}

func StartNestedArray() {
	var alertsResume AlertResume
	alertsResume.AllActive = 1
	envs := []string{"prod", "int", "cert"}
	for index, env := range envs {
		alertsResume.EnvActive = append(alertsResume.EnvActive, EnvActive{})
		alertsResume.EnvActive[index].Environment = env
		alertsResume.EnvActive[index].Alerts = index + 10
		alertsResume.EnvActive[index].Tagged = index + 20
	}
	fmt.Printf("%+v\n", alertsResume)
}
