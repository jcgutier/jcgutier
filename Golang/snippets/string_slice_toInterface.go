func StringSliceToInterface(inputSlice []string) []interface{} {
	tmpSlice := make([]interface{}, len(inputSlice))
	for i, v := range inputSlice {
		tmpSlice[i] = v
	}
	return tmpSlice
}
