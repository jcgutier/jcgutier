package main

import "fmt"

func main(){
	loop_map()
}

/*##################################################################################
## Itera sobre un array, cada elemento tendra el valor de cada elemento del array ##
##################################################################################*/
func loop_slice_array(){
	a := []string{"Foo", "Bar"}
    for i, s := range a {
		fmt.Println(i, s)
	}
}

/*#####################################################
## Iteracion sobre "bytes" el index mostrara el byte ##
#####################################################*/
func loop_runes_bytes(){
	for i, ch := range "日本語" {
		fmt.Printf("%#U starts at byte position %d\n", ch, i)
	}
}

/*###########################################################
## Iteracion sobre un "map", obteniendo el par "key/value" ##
###########################################################*/
func loop_map(){
	m := map[string]int{
		"one":   1,
		"two":   2,
		"three": 3,
	}
	for k, v := range m {
		fmt.Println(k, v)
	}
}

func loop_channels(){
	ch := make(chan int)
go func() {
    ch <- 1
    ch <- 2
    ch <- 3
    close(ch)
}()
for n := range ch {
    fmt.Println(n)
}
}
