package main

import "fmt"

func main() {
  ages := map[string]int{}
  ages["Keith"] = 18

  //Ejemplos de if/sle
  if ages["Keith"] < 18 {
    fmt.Println("Keith cant vote")
  } else if ages["Keith"] < 67 {
    fmt.Println("Keith is not of retiremen age")
  }else {
    fmt.Println("Keith is of retirement age")
  }

  //Ejemplos de swicth
  switch {
  case ages["Keith"] < 18:
    fmt.Println("Keith cant vote")
  case ages["Keith"] < 67:
    fmt.Println("Keith is not of retiremen age")
  default:
    fmt.Println("Keith is of retirement age")
  }

  //Ejemlo de otro switch
  switch ages["Keith"] {
  case 1, 2, 3, 5, 7, 11, 13, 17, 19:
    fmt.Println("Keith has a prime number age")
  case 16:
    fmt.Println("Keith cant vote")
  case 18:
    fmt.Println("Keith can vote")
  case 67:
    fmt.Println("Keith can retire")
  default:
    fmt.Println("Keith age is nothing especial")
  }
}
