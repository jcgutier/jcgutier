package message

import "fmt"

//Funcion con la primer letra en mayusculas para ser exportada
func Greeting(name, message string) (salutation string) {
  salutation = fmt.Sprintf("%s, %s", message, name)
  return
}
