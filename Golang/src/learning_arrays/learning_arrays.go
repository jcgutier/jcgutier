package main

import "fmt"

func main() {
  //names := [3]string{"Keith", "Kevin", "Kayla"}
  var names [3]string

  names[0] = "Keith"
  names[1] = "Kevin"
  //names[2] = "Kayla"

  fmt.Println(names)
  fmt.Println("names[2] is empty: ", names[2] == "")

  //Ejemplos de Slice
  names2 :=[]string{}
  names2 = append(names2, "Keith")
  names2 = append(names2, "Kevin")
  names2 = append(names2, "Kayla", "Kyle")

  fmt.Println(names2)

  //Ejemplo de slice con make
  names3  := make([]string, 4)
  names3[0] = "Kaith"
  names3[1] = "Kaith"
  names3[2] = "Kaith"
  names3[3] = "Kaith"
  names3 = append(names3, "Karl")
  fmt.Println(names3)
}
