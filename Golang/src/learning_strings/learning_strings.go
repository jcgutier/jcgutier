package main

import "fmt"

/*
Este es un comentario
multilinea
*/

//Comentario descriptivo de la funcion
func main() {
	fmt.Println("Hello World!") //Comentario al final de un comando

	fmt.Println("Simple string")
	fmt.Println(`
This is multi-line \n no incluye ese salto de linea
String
`)
	fmt.Println("\u2272")

	fmt.Println('L') //caracter o runes, la comilla simple no puede ser usada como cadena
}

