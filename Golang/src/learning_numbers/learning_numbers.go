package main

import (
	"fmt"
	"math") //Paquete para operaciones matematicas avanzadas

func main(){
	fmt.Println("Addition:", 1+3)
	fmt.Println("Subs:", 1-3)
	fmt.Println("Mult:", 2*3)
	fmt.Println("Division:", 20/3)
	fmt.Println("Division:", 20.0/3) //20.0 representa un numero del tipo float
	fmt.Println("Exponents:", math.Pow(20.0, 3))
}
