package main

import "fmt"

func main() {
	fmt.Println("Greater than", 1 > 2)
	fmt.Println("Less than", 1 < 2)
	fmt.Println("Less tha or equal to", 1 <= 2)
	fmt.Println("Grater than or equal to", 1 >= 2)
	fmt.Println("Equivalent", 4.0 == 4)
	fmt.Println("No equivalent", 4.0 != 4.1)
}
