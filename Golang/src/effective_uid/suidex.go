package main

import (
        "fmt"
        "syscall"
        "time"
)

func main() {
    printdelay()
}

func printdelay() {
    fmt.Printf("Real UID: %d\n", syscall.Getuid())
    fmt.Printf("Effective UID: %d\n", syscall.Geteuid())
    time.Sleep(7 * time.Second)
}
