package main

import "fmt"

func main() {
  birthdays := map[string]string{
    "Keith": "02/06/2019",
    "Kevin": "06/12/1994",
    "Kayla": "06/12/1989",
  }

  fmt.Println(birthdays)

  delete(birthdays, "Keith")
  fmt.Println(birthdays)


  ages := map[string]int{}
  ages["Keith"] = 28
  ages["Kevin"] = 71
  ages["Kayla"] = 73

  fmt.Println(ages)
}
