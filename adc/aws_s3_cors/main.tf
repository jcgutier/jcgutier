// Define the provider AWS
provider "aws" {
  region = "us-east-1" # Specify your AWS region
}

// Create AWS S3 bucket, with option to delete all files on destroy
resource "aws_s3_bucket" "bucket" {
  bucket        = "jcgutier-testing-jcgutierrez" # Replace with your desired bucket name
  force_destroy = true
}


// Adding website config
resource "aws_s3_bucket_website_configuration" "blog" {
  bucket = aws_s3_bucket.bucket.id
  index_document {
    suffix = "index.html"
  }
}

// Allowing public access to the website
resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket                  = aws_s3_bucket.bucket.id
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

// Upload all the files present under HTML folder to the S3 bucket #####
resource "aws_s3_object" "upload_object" {
  for_each     = fileset("html/", "*")
  bucket       = aws_s3_bucket.bucket.id
  key          = each.value
  source       = "html/${each.value}"
  etag         = filemd5("html/${each.value}")
  content_type = "text/html"
}

// Getting policy from file and replacing the BUCKET_ARN
locals {
  bucket_policy_json = replace(file("${path.module}/resource_based_policy.json"), "BUCKET_ARN", aws_s3_bucket.bucket.arn)
}

// Create AWS resource (bucket) policy for public allow
resource "aws_s3_bucket_policy" "allow_public_access" {
  bucket = aws_s3_bucket.bucket.id
  policy = local.bucket_policy_json
}


// Adding second bucket to test CORS error
resource "aws_s3_bucket" "bucket2" {
  bucket        = "jcgutier-testing-jcgutierrez-media" # Replace with your desired bucket name
  force_destroy = true
}
resource "aws_s3_bucket_website_configuration" "blog2" {
  bucket = aws_s3_bucket.bucket2.id
  index_document {
    suffix = "index.html"
  }
}
resource "aws_s3_bucket_public_access_block" "public_access_block2" {
  bucket                  = aws_s3_bucket.bucket2.id
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}
resource "aws_s3_object" "upload_object2" {
  for_each     = fileset("html/", "*")
  bucket       = aws_s3_bucket.bucket2.id
  key          = each.value
  source       = "html/${each.value}"
  etag         = filemd5("html/${each.value}")
  content_type = "text/html"
}
locals {
  bucket_policy_json2 = replace(file("${path.module}/resource_based_policy.json"), "BUCKET_ARN", aws_s3_bucket.bucket2.arn)
}
resource "aws_s3_bucket_policy" "allow_public_access2" {
  bucket = aws_s3_bucket.bucket2.id
  policy = local.bucket_policy_json2
}



// Some examples taken from: https://dev.to/aws-builders/how-to-create-a-simple-static-amazon-s3-website-using-terraform-43hc
// AWS Tutorial to configure static website on Amazon S3: https://docs.aws.amazon.com/AmazonS3/latest/userguide/HostingWebsiteOnS3Setup.html#step4-add-bucket-policy-make-content-public


