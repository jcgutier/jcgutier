#!/bin/bash

#set -e

skipvideo='false'
skipdatasync='false'

while getopts 'vs' flag; do
  case "${flag}" in
    #a) aflag='true' ;;
    #b) bflag='true' ;;
    #f) files="${OPTARG}" ;;
    v) skipvideo='true' ;;
    s) skipdatasync='true' ;;
    *) echo "Unexpected option ${flag}" ;;
  esac
done

RED=`tput setaf 1`
GREEN=`tput setaf 10`
BLUE=`tput setaf 4`
RESET=`tput sgr0`
CHECK_MARK="[\xE2\x9C\x94]"

resultado_mensaje(){
    if [ $1 -eq 0 ]; then
        echo -e "${GREEN}$CHECK_MARK $2 ${RESET}"
    elif [ $1 -eq 4 ]; then
        echo -e "${BLUE}$CHECK_MARK $2 ${RESET}"
    else
        echo -e "${RED}[x] $2 ${RESET}"
    fi
}

################################
## Actualizar la cache de APT ##
################################
echo -ne "Actualizando la cache de APT ... \033[0K\r"
sudo -i apt update > /dev/null 2>&1
resultado_mensaje $? "Actualizacion de cache APT     "
################################

###############################
## Instalando pre-requisitos ##
###############################
echo -ne "Instalando los paquetes pre-requeridos ... \033[0K\r"
sudo apt install -y openssh-server openssh-client git curl gnupg2 apt-transport-https > /dev/null 2>&1
resultado_mensaje $? "Instalacion de paquetes pre-requeridos  "
###############################

##########################################
## Creacion de llaves SSH si no existen ##
##########################################
echo -ne "Creando llave SSH ... \033[0K\r"
if [ ! -f ~/.ssh/id_rsa ]
then
    ssh-keygen -t rsa -b 4096
    resultado_mensaje $? "Creacion de llave SSH"

    if [ -z "$GITLAB_TOKEN" ]; then
        read -p "Ingresa el token de gitlab: " GITLAB_TOKEN
    fi
    curl -X POST -F "private_token=$GITLAB_TOKEN" -F "title=$(hostname)" -F "key=$(cat ~/.ssh/id_rsa.pub)" "https://gitlab.com/api/v4/user/keys"
    resultado_mensaje $? "Agregar llave a gitlab"
else
  resultado_mensaje 4 "Llave no creada, se encontro una llave previa"
fi
##########################################

#################################################
## Se copia llave al server si esta alcanzable ##
#################################################
SERVIDOR_DEFAULT=192.168.0.10
if [ -z "$SERVIDOR" ]; then
    read -p "Ingresa la IP del servidor, deja en blanco para usar el default [$SERVIDOR_DEFAULT]: " SERVIDOR
fi
if [ -z "$SERVIDOR" ];then
  SERVIDOR=$SERVIDOR_DEFAULT
fi
echo -ne "Copiando la llave al servidor: $SERVIDOR ... \033[0K\r"
ssh-copy-id $USER@$SERVIDOR > /dev/null 2>&1
resultado_mensaje $? "Copia de llave SSH al servidor                           "
#################################################

###########################################
## Agregando las llaves de ssh al agente ##
###########################################
echo -ne "Agregando la llave al agente de SSH ... \033[0K\r"
SSHAGENTFILE=/tmp/sshagent
added_keys=`ssh-add -l`
if [ $? -ne 0 ]; then
	if [ -f $SSHAGENTFILE ]; then
		eval `cat /tmp/sshagent`
		export SSH_AGENT_PID
		export SSH_AUTH_SOCK
	else
		ssh-agent -s > /tmp/sshagent
		eval `cat /tmp/sshagent`
		export SSH_AGENT_PID
		export SSH_AUTH_SOCK
	fi
fi
unset SSHAGENTFILE
added_keys=`ssh-add -l`
my_key=`ssh-keygen -lf ~/.ssh/id_rsa | awk '{print $2}'`
if [ ! $(echo $added_keys | grep -o -e $my_key) ]; then
    ssh-add "$HOME/.ssh/id_rsa"
fi
resultado_mensaje $? "Llave de SSH agregada al agente          "
###########################################

##########################################################
## Clonar/actualizar los repositorios remotos de GitLab ##
##########################################################
if [ ! -d  ~/Documentos/Programas/ ]; then
  mkdir -p ~/Documentos/Programas/; resultado_mensaje $? "Creacion del directorio ~/Documentos/Programas/"
fi
if [ ! -d  ~/Documentos/Programas/jcgutier/ ]; then
  cd ~/Documentos/Programas/ && git clone git@gitlab.com:jcgutier/jcgutier.git
  resultado_mensaje $? "Clonar repositorio de jcgutier"
  cd ~/Documentos/Programas/jcgutier/ && git config user.email jcgutierrezo28@gmail.com && git config user.name "Juan Carlos"
  resultado_mensaje $? "Configuracion del repositorio jcgutier"
fi
cd ~/Documentos/Programas/jcgutier/ && git pull origin master > /dev/null 2>&1
resultado_mensaje $? "Actualizacion del repositorio jcgutier"
##########################################################

######################################
## Obtener el backup del homeserver ##
######################################
if [ "$skipdatasync" == "false" ]; then
    ~/Documentos/Programas/jcgutier/Bash/BackUpScripts/dataget.sh -as
elif [ "$skipvideo" == "true" ]; then
    ~/Documentos/Programas/jcgutier/Bash/BackUpScripts/dataget.sh -asv
else
    resultado_mensaje 4 "Se salto la obtencion de archivos backup del servidor: $SERVIDOR"
fi
######################################

####################################################
## Agregar la configuracion personal al ~/.bashrc ##
####################################################
cat <<EOF > ~/.bashrc
# Mi configuracion personal de bash
if [ -f "$HOME/Documentos/Personal/personalbashconfig" ]; then
    . "$HOME/Documentos/Personal/personalbashconfig"
fi
EOF
resultado_mensaje $? "Agregar la configuracion personal a bashrc"
source ~/.bashrc
####################################################

##############################
## Clonando mis repositrios ##
##############################
$jcgutier/Bash/gitpull.sh
##############################

#############################################################
## Creando grupo de Docker y agregando el grupo al usuario ##
#############################################################
grep docker /etc/group > /dev/null 2>&1
if [ $? -eq 0 ]; then
    resultado_mensaje 4 "El grupo de docker ya existe"
else
    sudo groupadd docker; resultado_mensaje $? "Creacion del grupo docker"
    resultado_mensaje 4 "El grupo de docker ya existe"
fi
sudo usermod -a -G docker $USER
resultado_mensaje $? "Se agrega usuario al grupo docker"
#############################################################

#########################
## Instalando paquetes ##
#########################
PAQUETES="vim
supervisor
python3-pip
python3-dev
libevent-dev"
echo -ne "Instalando los paquetes requeridos ... \033[0K\r"
sudo apt install -y $PAQUETES > /dev/null 2>&1
resultado_mensaje $? "Instalacion de paquetes requeridos  "
#########################

################################
## Instalando paquetes de pip ##
################################
PIP_PACKAGES=(\
    "virtualenvwrapper" \
    "scapy" \
    "awscli" \
    "youtube-dl" \
)
#    "docker-compose" \
sudo -H pip3 install pip --upgrade > /dev/null
sudo -H pip install setuptools --upgrade > /dev/null
for PACKAGE in ${PIP_PACKAGES[@]};do
    sudo -H pip freeze | grep $PACKAGE > /dev/null 2>&1 && \
    sudo -H pip install $PACKAGE --upgrade > /tmp/pipout_$(date +"%Y%m%d_%H%M%S").txt 2>&1 || \
    sudo -H pip install $PACKAGE >> /tmp/pipout_$(date +"%Y%m%d_%H%M%S").txt 2>&1
    resultado_mensaje $? "Instalacion/Actualizacion de $PACKAGE    "
done
resultado_mensaje $? "Instalacion de paquetes pip             "
################################

#################################################################
## Inicializacion de los ambientes virtuales de Python basicos ##
#################################################################
export WORKON_HOME=$HOME/.virtualenvs
wpfile=$(sudo find / -name "virtualenvwrapper.sh" 2>/dev/null | tail -1)
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source "$wpfile"
cat <<\EOF >> ~/.bashrc

#Configuracion de virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
EOF
echo "source $wpfile" >> ~/.bashrc
resultado_mensaje $? "Agregar configuracion del virtual env wrapper al bashrc"
lsvirtualenv | grep home > /dev/null; if [ "$?" -ne "0" ]; then
    mkvirtualenv -p /usr/bin/python2.7 home;ret=$? && \
        pip install pip --upgrade && \
        pip install setuptools --upgrade && \
        pip install -r $jcgutier/Python/Home/requirements27.txt
    resultado_mensaje $ret "Creacion de ambiente virtual de python: home"
fi
lsvirtualenv | grep Homep3 > /dev/null; if [ "$?" -ne "0" ]; then
    mkvirtualenv -p /usr/bin/python3 Homep3;ret=$? && \
        pip install pip --upgrade && \
        pip install setuptools --upgrade
    resultado_mensaje $ret "Creacion de ambiente virtual de python: homep3"
    sudo -E $jcgutier/Python/Home/Addsystemdservices/startupnotificationservice.py
    resultado_mensaje $? "Instalacion del startup notification"
fi
lsvirtualenv | grep notas > /dev/null; if [ "$?" -ne "0" ]; then
    mkvirtualenv -p /usr/bin/python3 notas;ret=$? && \
        pip install pip --upgrade && \
        pip install setuptools --upgrade
    	pip install -r $notas/app/requirements.txt
    resultado_mensaje $ret "Creacion de ambiente virtual de python: notas"
fi
#################################################################

# ##############################
# ## Instalacion de go 1.13.3 ##
# ##############################
# go version > /dev/null; if [ "$?" -ne "0" ]; then
#     cd /tmp; curl -O https://dl.google.com/go/go1.13.3.linux-amd64.tar.gz && \
#         sudo tar -C /usr/local -xzf go1.13.3.linux-amd64.tar.gz
#     resultado_mensaje $? "Instalacion de golang 1.13.3"
# else
#     resultado_mensaje 4 "$(go version)"
# fi
# ##############################

##################################
## Importando las llaves de gpg ##
##################################
gpg --list-keys | grep JuanGutierrez > /dev/null; if [ "$?" -ne "0" ]; then
  sudo chmod 600 ~/.gnupg/*.*
  sudo chmod 700 ~/.gnupg/*/
  sudo chown $USER:"$(id -gn)" ~/.gnupg/*
  gpg --import $prog../Personal/private_key_juangutierrez.asc
  resultado_mensaje $? "Importacion de llave gpg de JuanGutierrez"
else
  resultado_mensaje 4 "La llave gpg de JuanGutierrez ya existia"
fi
gpg --list-keys | grep CarlosGutierrez > /dev/null; if [ "$?" -ne "0" ]; then
  sudo chmod 600 ~/.gnupg/*.*
  sudo chmod 700 ~/.gnupg/*/
  sudo chown $USER:"$(id -gn)" ~/.gnupg/*
  gpg --import $prog../Personal/private_key_carlosgutierrez.asc
  resultado_mensaje $? "Importacion de llave gpg de CarlosGutierrez"
else
  resultado_mensaje 4 "La llave gpg de CarlosGutierrez ya existia"
fi
##################################

###########################################################
## Configuracion de herramientas en repositorios remotos ##
###########################################################
if [ ! -d $HOME/github.com/pyenv/ ]; then
  mkdir -p $HOME/github.com/ && cd $HOME/github.com/
  git clone https://github.com/pyenv/pyenv.git
  resultado_mensaje $? "Obtener el repositorio remoto de pyenv"
else
  cd $HOME/github.com/pyenv/;git pull > /dev/null 2>&1
  resultado_mensaje $? "Actualizacion de pyenv"
fi
if [ ! -d $HOME/github.com/tfenv/ ]; then
  mkdir -p $HOME/github.com/ && cd $HOME/github.com/
  git clone https://github.com/tfutils/tfenv.git
  resultado_mensaje $? "Obtener el repositorio remoto de tfenv"
else
  cd $HOME/github.com/pyenv/;git pull > /dev/null 2>&1
  resultado_mensaje $? "Actualizacion de tfenv"
fi
###########################################################

## Configurando UFW ##
# echo "=== Configurando UFW ==="
# sudo ufw allow ssh
# sudo ufw allow http
# sudo ufw allow 443/tcp
# sudo ufw allow 5000/tcp
# sudo ufw --force enable
# sudo ufw status

# #########################
# ## Instalando homebrew ##
# #########################
# echo -ne "Instalando homebrew ... \033[0K\r"
# brew info > /dev/null 2>&1 || /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
# test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
# test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
# cat <<\EOF >> ~/.bashrc

# # Exportando las variables de brew
# EOF
# echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bashrc
# brew info > /dev/null 2>&1
# resultado_mensaje $? "Instalacion de homebrew"
# #########################

# ####################################
# ## Actualizacion de paquetes brew ##
# ####################################
# echo -ne "Actualizando paquetes de brew ... \033[0K\r"
# brew update > /dev/null 2>&1 && brew upgrade > /dev/null 2>&1
# resultado_mensaje $? "Actualizacion de paquetes brew               "
# ####################################

# #####################################
# ## Instalacion de paquetes de brew ##
# #####################################
# PAQUETES_BREW=(\
#     "minikube" \
#     "kubectx" \
#     "derailed/k9s/k9s" \
# )
# for PAQUETE in ${PAQUETES_BREW[@]};do
#     brew install $PAQUETE > /dev/null 2>&1
#     resultado_mensaje $? "Instalacion de $PAQUETE"
# done
# #####################################

################################################
## Configuracion de supervisor para misnotass ##
################################################
sudo bash -c 'cat <<EOF > /etc/supervisor/conf.d/misnotas.conf
[program:misnotas]
command=$HOME/Documentos/Programas/misnotas/run_no_gevent.sh
directory=$HOME/Documentos/Programas/misnotas/
user=carlos
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
EOF'
resultado_mensaje $? "Agregar configuracion de misnotas a supervidor"
sudo supervisorctl reload > /dev/null
resultado_mensaje $? "Reload de supervisor"
################################################
